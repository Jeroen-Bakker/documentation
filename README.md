# documentation

This repository contains documentation done during projects, research and other development activities.

This is used to keep track of changes and some workflow tools I personally use.

Eventually the documentation should be published on other platforms.