eval "$(/opt/homebrew/bin/brew shellenv)"
alias b="/Users/jeroen/blender-git/build_darwin/bin/Blender.app/Contents/MacOS/Blender --open-last"
export EDITOR="vim"
source "$HOME/.cargo/env"

# Git auto completion
autoload -Uz compinit && compinit

bindkey -v

m() {
    echo $( clear)
    pushd
    echo "************************************************************"
    echo "*** Build $(date)"
    echo "************************************************************"
    cd /Users/jeroen/blender-git/build_darwin/
    time ninja install
    popd
}