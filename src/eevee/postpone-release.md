# Decision

Yesterday the decision was made to post-pone EEVEE-Next to Blender 4.2 LTS. This decision was made with the consensus of all involved developers in EEVEE-Next development.

# Why is EEVEE-Next post-poned?

EEVEE-Next has some performance issues in the new setup of how Shadows and horizon scans are evaluated. Solving the shadow performance is more involved than first expected and there isn't a clear idea how to mitigate it. For horizon scan the solution is more clear, but requires additional development work. Next to that there is a performance drop when used on low powered GPUs.

Blender uses a release cycle based on several stages we call Bcon phases. New features can land during the first two phases (BCon1 and BCon2) at the end of BCon2 there is a moment where projects are evaluated, before we go into BCon3. BCon3 we should only do bug fixes and stabilizations. More information about our release cycle can be found at [https://developer.blender.org/docs/handbook/release_process/release_cycle/].

# What will happen next?

In the main development branch of Blender the engine names will be changed. `EEVEE` will be renamed into `EEVEE-Next` and `EEVEE (Legacy)` will be renamed to `EEVEE`.
When Blender 4.1 enters BCon3, EEVEE-Next will be made unavailable in blender-v4.1-release branch. EEVEE will still be available in alpha-builds of Blender 4.2.
Although EEVEE-Next will not be released in Blender 4.1 anymore, the developers don't want to loose their focus and agreed that we should still continue the development as if we would be releasing it for Blender 4.1.

# Why don't we release EEVEE-Next as an experimental option in Blender 4.1?

Although releasing EEVEE-Next as experimental option would reach out to more users, we believe that the current out-reach is already good, taken the inflow of issues and the development effort that we can spent. Besides that there are also some other aspects we discussed that an experimental option impacts:
* Experimental options that are released should be part of the manual, making the manual confusing to users. The changes to the manual would also be temporarily.
* Community tutorials would be confusing as the name of the engine would not be consistent across the releases.
EEVEE-Next will still be available in alpha-builds of Blender 4.2 which you can download from builder.blender.org or via steam.

# Blender 4.2 is an LTS release.

Blender 4.2 is an LTS release and we provide support for 2 years after its release. We discussed the impact of making this release the first release of EEVEE-Next. There is a higher chance of code divergence, making backporting changes more difficult, or sometimes not possible.
Looking back at our experiences with LTS releases most changes happens in the first few months of an LTS release. When Blender 4.3 is released many users switch. At that time LTS releases would mainly be used by professionals and studios and only high priority bugs are fixed (crashes, file corruption and workflow regressions). Making changes that effects the resulting render pixels are mostly not accepted as that is a risk for long run projects.
Our conclusion is that most support would happen when the code hasn't been able to deviate that much and backporting can be done.

# What about the shading improvements of EEVEE Legacy

Some changes that we done also improved the quality of EEVEE-Legacy. These changes were more related to better BSDF compatibility with Cycles. These changes will not be reverted and would be available in Blender 4.1 release.

# Feedback

We would like to thank everyone actively testing and providing feedback in the past months and keep up the valuable feedback. Feedback is essential in order to stabilize and find issues in EEVEE-Next.
