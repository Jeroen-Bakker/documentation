# Short term

* [x] Ensure that failing tests won't fail on the buildbot, except when crashed (change exit code) Crashing is part of the reporting (perhaps no rendered image) so should be detectable. This should be done for all GPU render engines for now.
* [ ] Fix eevee_next_grease_pencil crash
* [ ] Enable render tests on buildbot (apple only)
* [ ] Recheck motion blur tests on AMD/Mesa
* [ ] Fix opengl draw tests 
* [ ] Fix metal draw tests 
* [ ] Fix vulkan draw tests 