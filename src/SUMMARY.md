# Introduction

# EEVEE/Viewport Module

- [Module meetings](eevee-viewport/meetings.md)

# Projects 2023

- [Vulkan](gpu-vulkan/index.md)
  - [TODO](gpu-vulkan/todo.md)
  - [Roadmap](gpu-vulkan/roadmap.md)
  - [Vulkanized 2024](gpu-vulkan/vulkanized2024.md)
- [EEVEE](eevee/index.md)

# Projects 2024

- [3D Brush](3d-brush/index.md)
  - [Project State 2023](3d-brush/project-state-2023.md)

# Reports

- [2023](reports/2023.md)
- [2024](reports/2024.md)

# Development

- [Development setup](development/development-setup.md)
- [Renderdoc](development/renderdoc.md)
- [Issues](development/issues.md)
