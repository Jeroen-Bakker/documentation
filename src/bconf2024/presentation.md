---
marp: true
headingDivider: 2
theme: gaia
class: invert
paginate: true
footer: Blender Conference 2024 - Why does Blender Crash?
---
<!-- _footer: "" -->
# Why does Blender crash?
GPU platform support
Jeroen Bakker (jeroen@blender.org)

<!--
Hi welcome everyone to this presentation about a topic everyone has to deal with. Crashes!

Well crashes can happen due to many reasons and today we go in more detail on platform specific crashes. 

Crashes that happen on one platform, but not on another one. Most likely these issues are related to GPUs.
I will not cover Cycles related issues as that is a topic on its own and not my expertise.

The goal of this presentation is to share knowledge what we have collected working on GPU related projects and maintaining them.

-->

# Typically reports

- Unable to start Blender.
- Switching to material preview/render preview.
- Rendering EEVEE
- Enabling specific EEVEE features.

## Possible causes

- Limits are not respected
- Shader compilation issues
- Resource lifetimes
- Driver

# Limits

- GPU has physical limits
- Driver provides limits to application
  - Physical limits
  - Best practice
  - Good idea at the time

## Limits - Example

- #128407: Blender crashes with a large Light Probe Volume Pool on Intel Arc.
- Pool size of 512 MB / 1024 MB.
- EEVEE uses a 3D texture, allocated during initialization.

## Limits - Requested size (<= 4.2.3)

| Pool Size | X    | Y    | Z    | 
| --------- | ---- | ---- | ---- |
| 16 MB     | 1024 | 124  | 17   |
| 32 MB     | 1024 | 244  | 17   |
| 64 MB     | 1024 | 484  | 17   |
| 128 MB    | 1024 | 964  | 17   |
| 256 MB    | 1024 | 1928 | 17   |
| 512 MB    | 1024 | 3856 | 17   |
| 1 GB      | 1024 | 7712 | 17   |

<!--
A possible cause for a crash is memory allocations 
-->

## Limits - Specification

- 3D texture has limits. (GL_MAX_3D_TEXTURE_SIZE)
```
glGet (OpenGL4 reference manual):

A rough estimate of the largest 3D texture that the GL can handle. The value must be at least 64.

glGetTexLevelParameter

It has to report the largest square texture image that can be accommodated with mipmaps but a long
skinny texture, or a texture without mipmaps may easily fit in texture memory.
```
- GL_MAX_3D_TEXTURE_SIZE for Intel Arc is 2048.

## Limits - Requested size (>= 4.2.4)

| Pool Size | OpenGL | X    | Y    | Z    |
| --------- | ------ | ---- | ---- | ---- |
| 16 MB     |        | 64   | 1928 | 17   |
| 32 MB     |        | 128  | 1928 | 17   |
| 64 MB     |        | 256  | 1928 | 17   |
| 128 MB    |        | 512  | 1928 | 17   |
| 256 MB    |        | 1024 | 1928 | 17   |
| 512 MB    |        | 2048 | 1928 | 17   |
| 1 GB(*)   | 512 MB | 2048 | 1928 | 17   |

## Limits - Requested size (>= 4.3)

| Pool Size | OpenGL | X    | Y    | Z    | Vulkan |X     | Y      | Z  | 
| --------- | ------ | ---- | ---- | ---- | ------ | ---- | ------ | -- |
| 16 MB     |        | 64   | 1928 | 17   |        | 64   | 1928   | 17 |
| 32 MB     |        | 128  | 1928 | 17   |        | 64   | 3856   | 17 |
| 64 MB     |        | 256  | 1928 | 17   |        | 64   | 7712   | 17 |
| 128 MB    |        | 512  | 1928 | 17   |        | 64   | 15424  | 17 |
| 256 MB    |        | 1024 | 1928 | 17   |        | 64   | 30844  | 17 |
| 512 MB    |        | 2048 | 1928 | 17   |        | 64   | 61684  | 17 |
| 1 GB      | 512 MB | 2048 | 1928 | 17   |        | 64   | 1217974| 17 |

<!--
In vulkan the limit is still 2048, but it depends on other factors as well.
- How is the texture used
- How many mipmaps are you requesting
- Which data type is used

2048 is more a in the most complex situation you're still able to allocate a 2048 cubic 3D image.
-->
##

![bg 80%](./resources/gpuinfo-gl-max-3d-texture-size.png)

https://opengl.gpuinfo.org/
https://vulkan.gpuinfo.org/


# Shader Compilation

![bg 90%](./resources/compiler.png)

## GLSL

- Blender shaders are GLSL

```glsl
void main()
{
  vec4 sampled_color = texture(image, texCoord_interp);
  fragColor = vec4(sampled_color.r * shuffle.r + sampled_color.g * shuffle.g +
                   sampled_color.b * shuffle.b + sampled_color.a * shuffle.a) *
              color;
}
```

## Compilation - Frontend

- Syntax validation
- Source code parsing
- Macros
- Loop unrolling
- Function inlining
- Compiles into an 'intermediate' representation
- Optimizes the intermediate representation

## Intermediate

![bg 50%](./resources/spirv.png)

## Compilation - Backend

- Hardware limits are applied
  - Register
  - Local Memory
- Device specific optimizations
  - Branching
  - Instruction mapping

  <!--
  The intermediate representation needs to be translated to the capabilities and instructions of the GPU it will run on.
  -->

## Example #123071

```cpp
/* Silence macros when compiling for shaders. */
#  define BLI_STATIC_ASSERT(cond, msg)

BLI_STATIC_ASSERT(sizeof(SeqStripDrawData) * GPU_SEQ_STRIP_DRAW_DATA_LEN <= 16384,
                  "SeqStripDrawData UBO must not exceed minspec UBO size (16K)")
```

## Example #123071

```cpp
/* Silence macros when compiling for shaders. */
#  define BLI_STATIC_ASSERT(cond, msg)

BLI_STATIC_ASSERT(sizeof(SeqStripDrawData) * GPU_SEQ_STRIP_DRAW_DATA_LEN <= 16384,
                  "SeqStripDrawData UBO must not exceed minspec UBO size (16K)")
```

```txt
ERROR (gpu.shader): gpu_shader_icon_multi VertShader:
      | Error: C0159: invalid char 'K' in integer constant suffix
```

## Example #123071 - Fix

```cpp
/* Silence macros when compiling for shaders. */
#  define BLI_STATIC_ASSERT(cond, msg)

BLI_STATIC_ASSERT(sizeof(SeqStripDrawData) * GPU_SEQ_STRIP_DRAW_DATA_LEN <= 16384,
                  "SeqStripDrawData UBO must not exceed minspec UBO size (16384)")
```

## Challenges OpenGL

- Compilers are different
  - Initial shaders were small, but grew due to its success
  - Specs became more strict
  - Vendors: Market drives strictness.
- Drivers don't warn us
  - #128561 (Qualifier GLSL difference qualifier)
  - #128578 (Geometry shader emit vertex difference)
- Blender pushes limits

## Challenges Metal

- GPU drivers are part of the OS.
- Strategic platform choices.
- Platforms are long lived.
- Users might can't/won't update OS.


# Resource lifetime

```cpp
GPUTexture *atlas = GPU_texture_create_2d(
      "thumb_atlas", tex_width, tex_height, 1, GPU_RGBA8, GPU_TEXTURE_USAGE_SHADER_READ, nullptr);
  GPU_texture_update(atlas, GPU_DATA_UBYTE, tex_data.data());
  GPU_texture_filter_mode(atlas, true);
  GPU_texture_extend_mode(atlas, GPU_SAMPLER_EXTEND_MODE_CLAMP_TO_BORDER);

  /* Draw all thumbnails. */
  GPU_matrix_push_projection();
  wmOrtho2_region_pixelspace(ctx->region);

  ThumbsDrawBatch batch(strips_batch, atlas);
  for (int64_t i = 0; i < rects.size(); i++) {
    const rcti &rect = rects[i];
    const SeqThumbInfo &info = thumbs[i];
    batch.add_thumb(info, info.cropx_max - info.cropx_min + 1, rect, tex_width, tex_height);
  }
  batch.flush_batch();

  GPU_matrix_pop_projection();

  GPU_texture_unbind(atlas);
  GPU_texture_free(atlas);
```

## Swap chain

![bg 90%](./resources/resource-life-time.png)

# The weight of legacy

## The success of shader pipelines

<!--
Shaders provided that developers could actually program GPUs. First the shaders where small (<100 LOC). But due to its success and the increase of GPU power more and more complex shaders where created. The compilers used in the early times needed to handle more complexity, but still you didn't want to break all
the applications/games that were out there.

Some platforms decided to stick to a less strict interpretation of the GLSL spec for compatibility or convenience reasons. Resulting in an eco system where a developer isn't warned when deviating from the spec.
-->

## Hardware development

![bg 75%](./resources/hardware-life-cycle.png)
<!--
Hardware development cycle has limited flexibility. Although GPUs are released in batches and some batches can vary from others. It is still risky to change the internals and should be done very carefully.
-->

## End of life

- Bugs not solved
- Security/Critical updates
- Changing applications
  - New render techniques
  - Rewrites
  - Fixes

<!--
Eventually hardware gets EOL, which means that there will be no driver updates anymore.

 - Issues inside driver might not be solved anymore
 - New engine techniques might not be supported as it triggers internals that have never been tested or used.
 - Applications might introduce more workarounds to by-pass these issues
 - Application cannot rely on driver extensions maturity
 
We have seen in the past that the last driver update introduces new bugs that will not be solved.
If we detect that we try to document the last working driver and black-list the unsupported drivers.

 GPUs when released are build with a after being manufactured cannot be changed. 
-->

## Workarounds

- Use a different data type (HQ normals)
- Data packing/encoding
- Allocations from the main thread
- Synchronization issues

## About drivers

- Contains multiple drivers
  - GPU Architecture
  - OpenGL/Vulkan/DirectX/.. 

##

![bg](./resources/vulkan-driver-versions.png)

## OpenGL driver version

```
Graphics card: AMD Radeon(TM) R4 Graphics ATI Technologies Inc. 4.6.13558 Core Profile Context 26.20.11016.1
```

- 13558 -> Driver build number

<!--
In OpenGL there is no public table available. But understanding the versioning helps.
For me the OpenGL version (4.6.13558) provides often more information than the marketed driver version number.
-->


# How can you help?

- Test early
- Report always

## Also check Alpha, Beta releases and developments

- Users are more eager to test release candidates
- Fixing issues may take weeks/months
- Receive feedback during development of features on compatibility
  - Improves stability
  - Steers development choices
  - Future developments can rely on earlier made choices


## Don't be

- "Developers are still working on it"
- "It is a alpha build, things are expected to be broken"
- "Someone else will report it"
- "I will just continue with previous version"
- "Perhaps it is my computer"

## Be

- Try to reproduce on other machine
- Report via `Help -> Report a Bug`

```
Graphics card: Intel(R) Arc(tm) A750 Graphics (DG2) Intel Corporation Intel open-source Mesa driver Mesa 24.1.3 - kisak-mesa PPA
```

# ?
![bg 100%](./resources/wallpaper.jpg)

<!-- _footer: "" -->


##

![bg 50%](./resources/diagram-compatibility-solutions.png)

##

![bg 50%](./resources/diagram-driver-issue.png)


##
![bg 75%](./resources/gpu-architecture-immediate-mode-rendering.png)

##
![bg 60%](./resources/gpu-architecture-tile-based-rendering.png)

##

![bg 50%](./resources/texture-layouts.gif)

# Let me be clear!

- Most causes are located in Blender
  - Unclear specs
  - Drivers to flexible
  - Tooling not sufficient
- Driver bugs are uncommon, but do exist

## Blender GLSL improvements

- Improving GLSL development
- Use SPIR-V for validation
- Changes to buildbot
  - Draw/render tests
  - Different platforms
