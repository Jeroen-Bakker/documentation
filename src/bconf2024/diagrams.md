```mermaid
quadrantChart
    title Compatibility solutions
    x-axis Unknown issue --> Known issue
    y-axis Unknown solution --> Known solution
    quadrant-1 Plan/Develop
    quadrant-2 Waiting for evidence
    quadrant-3 Availability/Lack of experience
    quadrant-4 Requires engineering
```

```mermaid
quadrantChart
    x-axis Driver Issue --> Blender Issue
    y-axis Short fix-time --> Long fix-time
    quadrant-1 Requires engineering
    quadrant-2 Workaround/Blacklist
    quadrant-3 Wait for new driver
    quadrant-4 Fix bug
```

```mermaid
flowchart LR

    GLSL["Blender GLSL"]
    OpenGLSL["OpenGL GLSL"]
    VulkanGLSL["Vulkan GLSL"]
    frontend["Frontend compiler"]
    intermediate["Intermediate representation"]
    backend["Backend compiler" ]
    machine["Machine code"]

    GLSL --> MSL --> frontend
    GLSL --> OpenGLSL --> frontend
    GLSL --> VulkanGLSL --> frontend
    frontend --> intermediate --> backend --> machine

```

```mermaid
flowchart LR
    subgraph CPU
        recording
        optimizing
        submitting
    end
    subgraph GPU
        drawing
        swapchain
        display
    end

    recording --> optimizing --> submitting --> drawing --> swapchain -->display
```