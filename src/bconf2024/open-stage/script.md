Camera showing a desk.
Person comes in:

- "Morning Oscar"; In a **teasing** voice: *"Did you sleep in the office?"*
- "Ow yeah right, the boss send a video I had to watch"
- In a slightly irretating voice "No no no no no"
- In a relaxed professional telephone voice 
  - "Hi boss, yeah I saw the video."
  - "I tried, but I cannot use Vulkan."
  - "No I don't need a new system. The video is obviously fake.
  - "Think about it... these huge speedups are not realistic."
  - "Yeah speak you soon, Bye"

- Speaking to himselve: *"Nobody gets between me an my cookies!"*
- Speaking loud through the room *"Hey Oscar, when you go to the shop, can you buy some extra cookies, I feel a bit frustrated today!"*

Artefacts:
- Post-it with: "Watch the video!" on the screen
- Bowl containing cookies on the desk

Video:
- Shows Blneder 4.3 with OpenGL backend loading "barbershop", go to material preview
- Go to system preferences and switch to Vulkan backend
- Restart Blender
- Open barbershop and go to material preview
- Go to system preferences and select Different device
- Restart Blender
- Open barbershop and go to material preview

Text:

- "Do you want to be more productive"
- "Are your always waiting for material compilation"
- "Are you filling the waiting time with unhealthy habits"
- "Like grabbing and eating a lot of cookies"
- "Blender will make you more productive and healthier with the new Vulkan backend!"
- "Faster material compilations"
- "Try out Blender 4.3 experimental Vulkan backend and give feedback!"
