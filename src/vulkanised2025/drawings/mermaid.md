```mermaid
block-beta
    columns 5
    block
    Cuda
    OptiX
    OneAPI
    HIP
    end
    space 
    space
    space
    space

    Cycles
    EEVEE
    Core["Blender Core"]
    PythonUI
    Addon


space space space space space
    space
    space
    space
    PythonAPI["Python API"]:2

    block:GPU:5
    columns 6
    GPUImmediate["GPU Immediate emulation"]
    GPUShader["GPU Shader"]
    end
    block:Backend:5
    MetalBackend["Metal backend"]
    OpenGLBackend["OpenGL backend"]
    VulkanBackend["Vulkan backend"]
    end
    space
    space

    space space space

    block:gl:5
    Metal["Metal 3.1"]
    OpenGL["OpenGL 4.3"]
    Vulkan["Vulkan 1.2"]
    OpenXR("OpenXR")
    end


    Core --> GPU
    Cycles --> GPU
    EEVEE --> GPU

    PythonUI --> PythonAPI
    Addon --> PythonAPI
    PythonAPI --> GPU
    OpenGLBackend --> OpenGL
    MetalBackend --> Metal
    VulkanBackend --> Vulkan
    OpenGLBackend --> OpenXR
    VulkanBackend --> OpenXR
```

```mermaid
timeline
    Section Alpha
    Blender 4.0 (2023-10): Available in alpha builds
    
    Section Experimental
    Blender 4.3 (2024-11): First public release
                         : Experimental under development option
    Blender 4.4 (2025-03): Maturity
                         : Performance
                         : Platform
                         : Experimental
    
    Section Production ready
    Blender 4.5 LTS (2025-07) : OpenXR
                              : Hydra
                              : OpenSubdiv
                              : HDR
                              : Not experimental 
                              : OpenGL still default
    Blender 5.0 (2025-11): Default backend
                         : OpenGL still available
```

```mermaid
stateDiagram
    direction LR

    ApplicationThread: Application thread

    state ApplicationThread {
        Assigned: Request render graph
        Filled: Adds nodes
    }

    DeviceSubmitted: Submission queue
    Reuse: Reuse queue

    SubmissionThread: Submission worker

    
    state SubmissionThread {
        Reorder: Reorder nodes
        Extraction: Extract pipeline barriers
        Recording: Record to command buffer
        QueueSubmission: Submit to device queue
    }

    [*] --> Assigned
    Assigned --> Filled
    Filled --> DeviceSubmitted
    DeviceSubmitted --> Reorder
    Reorder --> Extraction
    Extraction --> Recording
    Recording --> QueueSubmission
    Recording --> Reuse
    Reuse --> Assigned


```