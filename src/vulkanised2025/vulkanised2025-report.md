# Vulkanised 2025 - report out

Vulkanised is a conference where specification writers, driver developers and application developers meet to discuss the latest about Vulkan. This includes future directions and what points to address in the near term.

This was my second time at the conference. Last year Blender was a new-comer/outsider. But this year, also due to the work I do for Khronos/Vulkan community, Blender was welcomed as an insider. This allowed me to make more connections as people were interested in and have a better understanding of our development/solution. Of course everyone knows Blender, but sharing the challenges that we face is eye-opening for many.

My main take-away from the conference is:
- Having confirmed the solution for improving the performance of uploading the depsgraph result to the GPU.
- Confirmed a solution for handling HDR/non-HDR swapchains.
- Some concrete areas of investigation to improve shader performance. 
- Better understanding about debugging driver related issues.

Also did a presentation about [Blender transition towards Vulkan](https://www.youtube.com/watch?v=GPxa1nqYmO4)

## Vulkan: Roadmap

Vulkan is already out for 8 years and some core designs have already been replaced by new designs. Sometimes even multiple new designs, which fit different use-cases. Due to compatibility the old designs are still part of the driver. The working group wants to deprecate parts of the specification. This means that in the documentation it will be stated that a feature is deprecated and be a sign to developers to strongly check for other solutions. It is not expected that features will be removed as driver developers will always want to support as many applications as possible.

Some discussions were about API usage.

- The API is huge and better guidance is needed to point an application developer to make the right choices.
- Future work of the working group is about debugging, but unclear how that will look like.
- Possibility that the image layouts are going to be reviewed. Do we really need 20 image layouts.
- Industry is moving towards a compute only workload. Graphic workload are more often done by compute shaders.

Slides of all presentations are [available](https://www.vulkan.org/events/vulkanised-2025#agenda). Videos will be released at a later moment.

## Vulkan: SDK

Most work last year was related to better error messages, faster loading of specification and tooling overall. Blender extensively uses these tooling, but there are some areas the usage can be improve. For example GFXReconstruct allows recording and playback of an application where you are able to playback on other devices/systems. Can be interesting for triaging.

Vulkan validation layers include tools for out of bound checks. Extensions like `VK_KHR_device_fault` allows debugging when a driver/GPU crashes inside the driver. Debug markers are used to try to narrow down to the actual command that is failing. For Blender this can give some insights why certain drivers don't work as expected.

## Performance: Device Generated Commands

[`VK_EXT_device_generated_commands`](https://docs.vulkan.org/spec/latest/chapters/device_generated_commands/generatedcommands.html) is an extension that can replace indirect drawing/compute to support full GPU based workflows. With indirect drawing/compute the CPU is still in control over the commands being executed. With device generated commands a compute shader can write those commands into a buffer and the CPU only needs to record that the GPU will need to dispatch the compute shader and "execute" the commands inside the buffer.


## Performance: Maximum reconvergence

A reoccurring topic during the conference was [maximum reconvergence](https://docs.vulkan.org/features/latest/features/proposals/VK_KHR_shader_maximal_reconvergence.html). The goal is to have better control over branching that happens when executing shaders. From a high level perspective this is similar to wide instructions on the CPU (AVX/SSE). In shaders this is often left to the compiler to figure out.

With `VK_KHR_shader_maximal_reconvergence` an application can control the branching using new shader functions. These functions give detailed control over thread masks. On the GPU masks are used to identify which thread takes which branch of a conditional by masking out the other threads.

## Tool: Lavapipe

- Lavapipe is supported on "all" major platforms including mac. An application is able to distribute its own lavapipe and use as a fallback for unsupported/broken platforms.

## Tool: libGPULayer

A library for developers to quickly implement a vulkan layers. Layers are optional software that can be placed between an application and the driver using hooks and part of the Vulkan loader. libGPULayer is developed by ARM and used during their triaging to identify driver bugs and prototype potential workarounds, without changing the driver or application.

This can also be used to validate resource lifetimes specific to the application implementation. However as only a few patterns exist, this should be more valuable when implemented inside the Vulkan validation layer.

## Organizing open source development

Vulkan ecosystem include many open source projects. The developmnent of those open source development are organized/financed differently
- **Lunar-G**: Although the company does consultancy, the development of the Vulkan SDK seems to be sponsored from Google and Valve. 
- **Collabora** & **Igalia**: Almost all development are done via consultancy contracts. The reason that mesa is in the state that it currently is is based on that GPU vendors/ISPs want to keep their devices running as long as possible. I did get the impression (and acknowledgement) that some development are done from the personal interest of the developers. Igalia team is structured as a collection of partners. People are expected to grow to partner level within a few years. 

## Random face to face discussion

- Architecture, Engineering and scientific visualization use-cases show similar issues compared to Blender. Mainly because a lot of data is computed by the CPU and needs to be uploaded. They are interested in how Blender handle this issue.
- Had several discussions with driver developers and how to handle issues.
- Many open source projects facing a similar issue. People are hesitant in reporting issues.
- Would Blender be open to support an Anari integration? [Anari](https://www.khronos.org/anari/) is an open source render engine API, similar to USD/Hydra, but targetting scientific visualization, engineering and architecture workflows. 

## Other topics
### Vulkan: Raytracing

New extensions appear to be used for execution reordering. The extension allows better control over when certain rays are executed. Similar to maximum reconvergence, but then for tracing rays.

### Machine learning

AI is a hot topic at the conference. There were several talks about it:

- SLang: Ability to generate a differentials of any function during compile time. This allows developers to work on the function and maintain implementation in a single place. Differentials are often used in AI workloads to optimize what choices an a machine learning algorithm needs to take.
- [Random-Access Neural Compression of Material Textures](https://research.nvidia.com/labs/rtr/neural_texture_compression/): Machine learning algorithm for texture compression. Using machine learning multiple channels (Albedo, normals, metal etc) can be compressed at in a single network. Shown example showed a production scene containing 6GB of blocked compressed data to become less than 1 GB. The big challenge is that compression it self took 20 seconds per material/texture set.
- Other machine learning algorithms are also being worked on, including depth intersection and gaussian splatting.
- There is a lot of research to use NxM matrices inside shaders. Some research already realized shader/compiler extensions as most can be solved during shader compilation. 

### Video

- Intra referential frames allow to use partial of a frame as a I-frame. This is mainly useful for streaming and video conferences.
- Lavapipe now supports vulkan video. This is project is executed by University of Vienna. Practical uses are not done yet. 

### Mobile

There were several mobile related topics. These topics covered:
- Bindless mobile HW-raytracing using lightweight-vk. 
- How does the swap chain work on rotated devices and what should an application do to be most power efficient when devices are rotated.
  - Something that was covered is that an application can have multiple swap-chains in play and select based on requirements. For Blender this could solve the HDR swapchain challenge where we don't know which swapchain should be used without interpreting the scene-data. In this situation we can create the swapchains memory-less.
- Temporal upscaling: A modified implementation of FSR2 for low powered devices. Uses custom shaders to reduce bandwidth and memory needs.
- Best practices when working on low powered devices and how to optimize for tiled based architectures. Gave a lot of insights of what and how a tiled based GPU works internally.
- A new GPU profiling tool is in development. It primary target is mobile, but can also run on Linux. It combines Perfetto and GFX-reconstruct with a custom UI to give more insight what is actually going on. Seems an interesting tool and would want to add it to our tool-set as well.
