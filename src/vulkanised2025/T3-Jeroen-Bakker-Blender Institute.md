---
marp: true
headingDivider: 3
paginate: false
footer: Vulkanised 2025 - Blender Transition towards Vulkan - jeroen@blender.org
class: invert
---

<style>
h1, h2, h3, footer {
  color: orange;
}

</style>


#
<!-- _footer: "" -->

![bg](images/first-slide.png)

## Content
<!-- paginate: true -->

- What is Blender?
- Why Vulkan?
- Render graphs
- Shader compilation
- Platform support
- Tooling


## Blender is ..

### .. we all know about the default cube
<!-- paginate: false -->
<!-- footer: "" -->
![bg](images/blender-default-cube.png)
### .. a modeller ..
![bg](images/blender-modelling.png)
### .. sculpt ..
![bg](images/blender-sculpt.png)
### .. and paint tool
![bg](images/blender-painting.png)
### .. an animation tool
![bg](images/blender-animation.png)
### .. a rasterizer ..
![bg](images/blender-sponza-eevee.png)
### .. and a path tracer
![bg](images/blender-sponza-cycles.png)
### .. a 2D animation tool
![bg](images/blender-2d-animation.png)
### .. a video player
![bg](images/blender-media-viewer.png)
### .. a video editor
![bg](images/blender-video-editor.png)
### .. a feature tracker
![bg](images/blender-tracker.png)
### .. flexible workflows
![bg](images/blender-compositor.png)
### .. a development environment
![bg](images/blender-development-environment.png)
### .. a python module
![bg](images/blender-as-python-module.png)
### .. aged well
![bg width:678px](images/blender-birth.png)
### .. and is still growing
![bg](images/blender-activity.png)

## Bender is ..
<!-- paginate: true -->
<!-- footer: "Vulkanised 2025 - Blender Transition towards Vulkan - jeroen@blender.org" -->

- Community driven open source 3D content creation suite.

## Why Vulkan?

- OpenGL is loosing traction

  - Blender pushes boundaries
  - Increase potential bugs
  - New features not available
  - Stability & support

- Perhaps leverage better GPU utilization.

## 
<!-- _class: lead -->
![bg width:1200](drawings/frameworks.svg)


### Vulkan backend - high level

- Data management
  - Resource tracking
  - Conversion
- Render graph
- Command building
- Shader compilation


## Getting something drawn

- Approach is a render graph
- Nodes with resource dependencies
- Resource tracking per image layer
- Existing API pushed to a node per command.

### Render graph nodes
<style scoped>
  table {
    font-size: 18px;
  }
</style>
<center>

| **Node** | **Vulkan API** |
| -------- | -------------- |
| **Data transfer** |
| `VKBlitImageNode` | `vkCmdBlitImage` |
| `VKClearColorImageNode` | `vkCmdClearColorImage` |
| `VKClearDepthStencilImageNode` | `vkCmdClearDepthStencilImageNode` |
| `VKCopyBufferNode` | `vkCmdCopyBuffer` |
| `VKCopyBufferToImageNode` | `vkCmdCopyBufferToImage` |
| `VKCopyImageNode` | `vkCmdCopyImage` |
| `VKCopyImageToBufferNode` | `vkCmdCopyImageToBufferNode` |
| `VKFillBufferNode` | `vkCmdFillBuffer` |
| `VKSynchonizationNode` | `vkCmdPipelineBarrier` (only used for swapchain barriers) |
| `VKUpdateBufferNode` | `vkCmdUpdateBuffer` |
| `VKUpdateMipmapsNode` | `vkCmdPipelineBarrier`/`vkCmdBlitImage` |

</center>

### Render graph nodes
<style scoped>
  table {
    font-size: 12px;
  }
</style>
<center>

| **Node** | **Vulkan API** |
| -------- | -------------- |
| **Draw** |
| `VKBeginRenderingNode` | `vkCmdBeginRendering`/`vkCmdBeginRenderPass`
| `VKClearAttachmentsNode` | `vkCmdClearAttachments`|
| `VKDrawIndexedIndirectNode` | `vkCmdDrawIndexedIndirect` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants` |
| | `vkCmdBindVertexBuffers`/`vkCmdBindIndexBuffer`] |
| `VKDrawIndexedNode` | `vkCmdDrawIndexed` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants` |
| | `vkCmdBindVertexBuffers`/`vkCmdBindIndexBuffer`] |
| `VKDrawIndirectNode` | `vkCmdDrawIndirect` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants` |
| | `vkCmdBindVertexBuffers`] |
| `VKDrawNode` | `vkCmdDraw` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants` |
| | `vkCmdBindVertexBuffers`] |
| `VKEndRenderingNode` | `vkCmdEndRendering`/`vkCmdEndRenderPass` |

</center>

### Render graph nodes

<style scoped>
  table {
    font-size: 20px;
  }
</style>

<center>

| **Node** | **Vulkan API** |
| -------- | -------------- |
| **Compute** |
| `VKDispatchIndirectNode` | `vkCmdDispatchIndirect` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants`] |
| `VKDispatchNode` | `vkCmdDispatch` |
| | optional: [`vkCmdBindPipeline`/`vkCmdBindDescriptorSets`/`vkCmdPushConstants`] |
| **GPU selection** |
| `VKBeginQueryNode` | `vkCmdBeginQuery` |
| `VKEndQueryNode` | `vkCmdEndQuery` |
| `VKResetQueryPoolNode` | `vkCmdResetQueryPool` |

</center>

### Node
```cpp
template<VKNodeType NodeType,
         typename NodeCreateInfo,
         typename NodeData,
         VkPipelineStageFlags PipelineStage,
         VKResourceType ResourceUsages>
class VKNodeInfo : public NonCopyable {

 public:
  using CreateInfo = NodeCreateInfo;
  using Data = NodeData;

  /**
   * Node type of this class.
   *
   * The node type used to link VKRenderGraphNode instance to a VKNodeInfo.
   */
  static constexpr VKNodeType node_type = NodeType;

  /**
   * Which pipeline stage does this command belongs to. The pipeline stage is used when generating
   * pipeline barriers.
   */
  static constexpr VkPipelineStageFlags pipeline_stage = PipelineStage;

  ...
```
### Node
```cpp
  ... 
  /**
   * Which resource types are relevant. Some code can be skipped when a node can only depend on
   * resources of a single type.
   */
  static constexpr VKResourceType resource_usages = ResourceUsages;

  /**
   * Update the node data with the data inside create_info.
   *
   * Has been implemented as a template to ensure all node specific data
   * (`Data`/`CreateInfo`) types can be included in the same header file as the logic. The
   * actual node data (`VKRenderGraphNode` includes all header files.)
   *
   * This function must be implemented by all node classes. But due to cyclic inclusion of header
   * files it is implemented as a template function.
   */
  template<typename Node, typename Storage>
  static void set_node_data(Node &node, Storage &storage, const CreateInfo &create_info);

  /**
   * Extract read/write resource dependencies from `create_info` and add them to `node_links`.
   */
  virtual void build_links(VKResourceStateTracker &resources,
                           VKRenderGraphNodeLinks &node_links,
                           const CreateInfo &create_info) = 0;

  /**
   * Build the commands and add them to the command_buffer.
   *
   * The command buffer is passed as an interface as this is replaced by a logger when running test
   * cases. The test cases will validate the log to find out if the correct commands where added.
   */
  virtual void build_commands(VKCommandBufferInterface &command_buffer,
                              Data &data,
                              VKBoundPipelines &r_bound_pipelines) = 0;
}
```

### Node implementation

```cpp
struct VKCopyBufferToImageData {
  VkBuffer src_buffer;
  VkImage dst_image;
  VkBufferImageCopy region;
};
struct VKCopyBufferToImageCreateInfo {
  VKCopyBufferToImageData node_data;
  VkImageAspectFlags vk_image_aspects;
};

class VKCopyBufferToImageNode : public VKNodeInfo<VKNodeType::COPY_BUFFER_TO_IMAGE,
                                                  VKCopyBufferToImageCreateInfo,
                                                  VKCopyBufferToImageData,
                                                  VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                  VKResourceType::IMAGE | VKResourceType::BUFFER> {
 public:
  template<typename Node, typename Storage> static void set_node_data(Node &node, Storage &storage, const CreateInfo &create_info)
  {
    node.storage_index = storage.copy_buffer_to_image.append_and_get_index(create_info.node_data);
  }

  void build_links(VKResourceStateTracker &resources, VKRenderGraphNodeLinks &node_links, const CreateInfo &create_info) override
  {
    ResourceWithStamp src_resource = resources.get_buffer(create_info.node_data.src_buffer);
    ResourceWithStamp dst_resource = resources.get_image_and_increase_stamp(create_info.node_data.dst_image);
    node_links.inputs.append({src_resource, VK_ACCESS_TRANSFER_READ_BIT});
    node_links.outputs.append({
      dst_resource, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, create_info.vk_image_aspects});
  }

  void build_commands(VKCommandBufferInterface &command_buffer, Data &data, VKBoundPipelines & /*r_bound_pipelines*/) override
  {
    command_buffer.copy_buffer_to_image(
        data.src_buffer, data.dst_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &data.region);
  }
};
```
### Node resource dependencies

```json
groups_build_commands:
  node_group=969,
  node_group_range=1019-1019,
  node_handle=1019,
  node_type="DISPATCH_INDIRECT", 
  debug_group="/SPACE_VIEW3D/Viewport/EEVEE/negZ_view/Deferred.Opaque/Shadow/TilemapUpdate/RenderClear"

NODE:
  type="DISPATCH_INDIRECT"

 inputs:
  handle=377, type="BUFFER", vk_handle=140734952600192, vk_access="VK_ACCESS_SHADER_READ_BIT"
  handle=355, type="BUFFER", vk_handle=140734934300544, vk_access="VK_ACCESS_SHADER_READ_BIT"
  handle=351, type="BUFFER", vk_handle=140734934300032, vk_access="VK_ACCESS_INDIRECT_COMMAND_READ_BIT"

 outputs:
  handle=865, type="IMAGE", vk_handle=140734829073920, name="shadow_atlas_tx_",
    vk_access=["VK_ACCESS_SHADER_READ_BIT", "VK_ACCESS_SHADER_WRITE_BIT"],
    vk_image_layout="VK_IMAGE_LAYOUT_GENERAL",
    vk_image_aspect="VK_IMAGE_ASPECT_COLOR_BIT",
    layer_base=0
```

### Render graph submission
<!-- _class: lead --->
![img](drawings/render-graph.svg)

- Many application threads
  - UI: 1-2 threads
  - EEVEE: 2-4 threads
  - Cycles: `n` threads
- Input order is UI code, not GPU order
- Command buffers are not kept: workload assumed to be always rendering
- Queue submission can be post-poned

## Shader compilation

- Compilation performance is essential
  - Latency between user action and the result
  - Partial update before caching before baking
  - Materials fast compilation vs fast execution
  - Material/engine recompilation
- ShaderC allows to compile shaders in parallel, however...

### 
![bg](images/spirv-tools-friendly-name-mapper.png)

### Power of open source

![img width:1150](images/spirv-tools-fix.png)

- Must do: Have an issue -> report!
  - Projects want to understand use-cases
  - We like bugs (to be known, so they can be solved)

### Benchmark
<!-- _footer: "" -->

<style scoped>
  table {
    font-size: 16px;
  }
</style>
*Test*: Start Blender, open scene, final viewport
3000 objects, 550 materials, 200 images
![bg right width:700](images/shader-barbershop.png)

| Backend | Test                      | Duration (s) |
| ------- | ------------------------- | -------- |
| OpenGL  | **Coldstart/No subprocesses** | 1:52 |
| OpenGL  | Coldstart/8 subprocesses  | 0:54     |
| OpenGL  | Warmstart/8 subprocesses  | 0:06     |
| Vulkan  | Coldstart single threaded | 0:59     |
| Vulkan  | Warmstart single threaded | 0:10     | 
| Vulkan  | **Coldstart multi threaded**  | 0:06 |
| Vulkan  | Warmstart multi threaded  | 0:03 |


## Blender - platforms support

- Development targets Vulkan 1.2.
- In the end all supported platforms can do 1.3.

### Extensions and features 

<style scoped>
table {
  font-size: 12px;
}
</style>

<center>

| **Features**                   | **Extensions**                              |
| ------------------------------ | ------------------------------------------- |
| **Required**                   | **Required**                                |
| Geometry shaders [1]           | VK_EXT_provoking_vertex_extensions          | 
| Logical operations             |                                             | 
| Dual source blending           | **Optional**                                |
| Image cube array               | VK_KHR_dynamic_rendering                    |
| Multi draw indirect            | VK_EXT_dynamic_rendering_unused_attachments |
| Multi viewport                 | VK_EXT_dynamic_rendering_local_read         |
| Shader clip distance           | VK_EXT_shader_stencil_export                |
| Draw indirect first instance   | VK_EXT_swapchain_colorspace                 |
| Fragment store and atomics     | VK_KHR_fragment_shader_barycentric          |
|                                | VK_KHR_maintenance4                         |
| **Optional**                   |                                             |
| Sampler anisotropy             |                                             |
| Shader draw parameters         |                                             |
| Shader output layer            |                                             |
| Shader output viewport index   |                                             |

</center>

## Supported platforms

<style scoped>
table {
  font-size: 12px;
}
</style>

<center>

| **Devices**                   | **Windows**           | **Linux**                  |
| ----------------------------- | --------------------- | -------------------------- |
| **NVIDIA**                    |                       |                            |
| GTX 700 - GTX 800             | NVIDIA 500+           | *Unsupported*              |
| GTX 900 - GTX 1000            | NVIDIA 500+           | NVIDIA 550+                |
| RTX 2000 - RTX 5000           | NVIDIA 500+           | NVIDIA 550+                |
| **AMD**                       |                       |                            |
| HD 7000 - HD 8000             | *Unsupported*         | Mesa                       |
| R 200, R 300                  | *Unsupported*         | Mesa                       |
| RX 400 - RX 600               | AMD Official/latest   | AMD Official/latest, Mesa  |
| RX Vega                       | AMD Official/latest   | AMD Official/latest, Mesa  |
| RX 5000 - RX 9000             | AMD Official/latest   | AMD Official/latest, Mesa  |
| **Intel**                     |                       |                            |
| Intel 6-10th gen iGPU         | *Unsupported*         | Mesa                       |
| Intel 11th gen iGPU and above | Intel Official/latest | Mesa                       |
| Intel Arc                     | Intel Official/latest | Mesa                       |
| **Qualcomm**                  |                       |                            |
| Adreno X1-85 GPU              | Qualcomm next release | *Not tested*               |

</center>

## Development tooling 

- Testing
  - `blender --gpu-backend {vulkan/opengl/metal}`
- Simulate other GPUs
  - `blender --debug-gpu-force-workarounds`
  - `blender --debug-gpu-vulkan-local-read`

### 
![bg width:768](images/tools-device-selector.png)

###
![bg](images/tools-validation-layers.png)

### 
![bg](images/tools-renderdoc.png)

### Custom capturing

```sh
blender --debug-gpu-renderdoc
```

```c
#include "GPU_debug.hh"

void MyRenderer::render() {
  GPU_debug_capture_begin(__func__);
  GPU_debug_group_begin("MyRenderer");

  /* Do stuff */

  GPU_debug_group_end();
  GPU_debug_capture_end();
}
```

## 
<!-- _class: lead -->
![bg width:1150](drawings/timeline.svg)

## Become part of the development community

- New to open source development: Google summer of code
- developer.blender.org
- chat.blender.org

**Kind of developments**

- `VK_EXT_descriptor_buffer`
- `VK_EXT_graphics_pipeline_library`
- Local read support for non tiled architectures.
- Code optimizations
- ...

## Time for questions

