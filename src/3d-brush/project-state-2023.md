# Project State 2023

In the current planning for 2024 the 3d brush will get focus again. The team involved is also
different than previously. So it is a good time to outline the current state and ideas for the
project in order to align with any new direction.

The project outline for the 3D Texturing Brush is to use the sculpt brush and PBVH data structure to
improve the functionality and performance of texture paint mode.

Project main development efforts so far where done in 2022. During this time most time was spent
in designing and implementing a acceleration structure that could handle brush stoke updates and
finding a workable solution to fix seem bleeding.

## Sculpt vs paint mode

During initial developments the 3D brush was implemented in Sculpt mode as part of the paint tool.
There are limitations of doing this in sculpt mode like only supporting the base mesh (no modifiers
applied) what is the input source for sculpting PBVH.

The brush is available in sculpt mode as at the beginning of the project the concept of a paint mode
didn't exist yet. When the paint mode was defined the focus went to other projects and paint mode
was never realized.

Paint mode is defined as a new object mode in the 3d viewport, allowing paint operations on objects.
This paint mode is designed to be used for attribute painting and texture painting.

See https://projects.blender.org/issues?q=%22paint%20mode%22&type=your_repositories&state=open&labels=297
for a list of designs. 

## 2D texturing

During the planning phase of project Gold there was a meeting looking at the current needs of
artist concerning painting. The results of that meeting is available at
[#109820](https://projects.blender.org/blender/blender/issues/109820).

Current implementation of 2D texturing is very slow and has a separate feature set. The reason that
it is slow is that it creates a texture containing what needs to be updated and then apply the changes.
The intermediate texture depended on the brush settings and used to be cached. The 2D texture brush
was extended to improve the texture quality (anti aliasing option), but when enabled it removed the
ability to cache the intermediate texture resulting in recreating it for every brush sample. Also it
doesn't scale well as the CPU internal caches are loaded with data that is only used once and can
overflow the cache when a brush covers a larger part of the image.

The brush also lacks features and some features might not be easy to implement using the current
approach.

My take on the topic is to replace the 2D texture brush with the 3D texture brush, ignoring the 3rd
dimension. This can be done as the internal 3D texture brush structures are already using 3D and UV
space. By replacing the 3d coordinates with an UV grid would allow using the 3D texture brush. 
There are benefits as we have less code to maintain and features implementing for the 2D texturing
or 3D texturing can be re-used.

This still needs more research if this is the right direction and how the details will influence
future requirements and development. We should also investigate if making `Paint mode` usable on
texture data blocks is a possibility here.

## Multi object UV maps

Currently not implemented. An idea discussed during the workshopts was to consider multiple objects
when generating the PBVH pixel data structure. After the edge bleeding  data structures have been
populated the data of objects not being painted on can be cleared.

## Fixing edge bleeding

A lot of effort has been done to fix edge bleeding in the 3d texturing brush.
Current solution solves edges of uv islands that are connected to other UV islands by extending the
UV mesh [D14970](https://archive.blender.org/developer/D14970). Edges of UV islands that don't
connect to other UV islands copy the closest pixel and thereby removing the edge bleeding.
[#105336](https://projects.blender.org/blender/blender/pulls/105336). 

One feature that has been requested is to fill in pixels that are not covered in the texture. This
could be done via an operator or as an option when the stoke is completed. Details not settled yet.
The idea how this could be done was to calculate the distance between each pixel that isn't covered
to the closest pixel that is covered by the PBVH pixels data structure. Using a mipmap blur where
where the missing pixels are filled in with an average pixel color would produce some results that
would still be fast to calculate, without using to much memory.

## Blur/smooth brush

This feature is still in the ideas phase. There is no clear design how this could be done and still
be performant.

The idea that was proposed by Brecht is that pixels should be able to locate their neighbor pixels
which would be used to search the input pixels for the blur/smooth. 

Another approach is to render with the viewpoint of the brush to an image. And use this as input of
the blur/smooth brush. This fit with an GPU accelerated approach where graphic pipeline can be used
for drawing, mipmapping and a compute shader to sample from the mipmap.

## Wet paint

Sculpt paint mode has a wet paint option when painting on vertices. This option hasn't been ported
to texturing yet. Would be a good approach to start documenting the wet paint algorithm or point
to a paper/reference implementation.

Project has some unknowns and we need to check how it fits in the current 3D Brush implementation.

## Color Filter

Texture painting has a color/filter tool. Which feels more like an operator than a tool. As this is
not really a brush specific operator it can be handled as its own sub-project. It can use the PBVH
pixels data structure in order to know the 3d space and UV space coordinates when taking current
object or UV islands into account.

Seems like a stand-alone and not that complicated task.

An idea that I had was to add a preview of the operator when dragging the mouse. Similar how
texture painting application show a preview.

## Performance

### Node splitting

When implementing the 3D Texturing Brush we used a lot of geometry and 8-16k textures. For smaller
geometry sizes a node splitting algorithm had been introduced that spliced pixels in PBVH Nodes into
smaller nodes to improve threading benefits. This part still needs improvements. Current
implementation is more functional (it works), than being designed for performance and optimized
for different models/texture sizes.

In December 2023 I detected that the dicing implementation crashed. It was recently migrated to CPP
but it wasn't tested and now frees uninitialized memory. For the short term I disabled the dicing.
[#115928](https://projects.blender.org/blender/blender/pulls/115928)

### Brush sampling order

In 2022 I did some experiments in order to improve the performance. The idea behind the first
experiment was to change the control flow during painting. 

In the current situation sculpt brush strokes are structured in

```mermaid
sequenceDiagram
    participant Brush Stroke
    participant Frame
    participant Brush Sample
    participant Pixel

    activate Brush Stroke
    loop until mouse released
        Brush Stroke --> Frame: start new frame
            activate Frame
            loop until time elapse
                Frame --> Brush Sample: apply
                    activate Brush Sample
                    Brush Sample --> Pixel: update pixels
                        activate Pixel
                        deactivate Pixel
                    deactivate Brush Sample
            end
            deactivate Frame
    end
    deactivate Brush Stroke
```

> **FRAME**: A frame is everything that happens between two redraw of the display.
> A concept that isn't reflected in the Sculpting/Brush API.

In this approach a pixel is read and written to multiple times per frame. This leads to cache
flushing and depends on the brush reevaluation if a pixel is covered by brush samples, when most
of the time the pixels are covered multiple times.

The thesis of the experiment was to switch the loop of the pixels with the loop of the brush samples
to improve data locality and reuse the coverage data inside the inner loop.

```mermaid
sequenceDiagram
    participant Brush Stroke
    participant Frame
    participant Pixel
    participant Brush Sample

    activate Brush Stroke
    loop until mouse released
        Brush Stroke --> Frame: start new frame
            activate Frame
            loop until ?
                Frame --> Brush Sample: create
                    activate Brush Sample
                    deactivate Brush Sample
            end
            loop for covered each pixel 
                Frame --> Pixel: update
                    activate Pixel
                    Pixel --> Pixel: read
                    loop for each created brush sample covering pixel
                    Pixel -> Brush Sample: apply
                        activate Brush Sample
                        deactivate Brush Sample
                    end
                    Pixel --> Pixel: store
                    deactivate Pixel
            end
            deactivate Frame
    end
    deactivate Brush Stroke
```

This structure has also benefits as marking and filling undo buffers and determining areas on the
texture can be collected on frame level and evaluated once. Mostly improving the readability of the
control flow.

Although this improved the performance it showed some limitations in the brush sampling API.

* The number of brush samples generated are based on how much time it takes to evaluate the
  brush sample. In case a brush sample is just created and applied in a later stage would generate
  a large number of brush samples and sometimes decreasing the end user performance. When compared
  the technical performance (number of brush samples applied per second) would increase.

### GPU Evaluation

The previous experiment has been done in preparation of evaluation of the texturing brush on the GPU.
There are some benefits in GPU evaluation, but there is more research needed to make this production
ready.

```mermaid
flowchart LR
    subgraph CPU
    ImageDataBlock
    end

    subgraph GPU

    subgraph GPU Painting
    TextureStreaming
    LoadPixelsComputeShader([Load Pixels])
    PaintComputeShader([Paint Compute Shader])
    ApplyPixelsComputeShader([Copy To GPU Texture Compute Shader])
    end

    subgraph EEVEE/Workbench Drawing
    GPUTexture
    DrawEngine([EEVEE / Workbench])
    end
    end

    GPUTexture -->|read| LoadPixelsComputeShader
    ImageDataBlock -->|transfer| LoadPixelsComputeShader
    LoadPixelsComputeShader -->|write| TextureStreaming
    GPUTexture -->|read| DrawEngine
    TextureStreaming -->|read| ApplyPixelsComputeShader
    ApplyPixelsComputeShader -->|write| GPUTexture
    TextureStreaming -->|transfer| ImageDataBlock
    TextureStreaming <-->|read/write| PaintComputeShader
```

Some benefits I found during the experiment:

* Texture streaming was used to load the section of the texture in GPU memory that was painted on.
  The source could be CPU, but we could also consider loading it from an available GPU texture.
* Undo can be postponed to the end of the frame, reducing the times undo buffers had to be marked.
* Areas of the undo buffer match the areas of the GPU texture that requires partial updating before drawing
* The sizes of the undo buffers, texture streaming buffers and GPU texture partial uses the same
  resolution (256x256) allowing to mark updates/undo buffer once. 
* Streaming buffers would only uploaded to the GPU when they are first covered by a brush sample. 
* A GPU -> CPU data transfer would be needed and preferable at the end of each frame. Those are
  typically slower, but as they happen when the GPU isn't drawing it the slowdown would be limited.
  The data transfer isn't needed for undo buffers. Only needed to update the image data block and
  we could also postpone it until the end of the stroke if not used as an depsgraph input.
* Evaluation per pixel removes the need of node splitting. 

Current branch is not working at this time due to changes to the GPU backend. 
[#115900](https://projects.blender.org/blender/blender/pulls/115900)
