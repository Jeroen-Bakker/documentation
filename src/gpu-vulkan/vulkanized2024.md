# Vulkanized 2024

https://vulkan.org/events/vulkanised-2024 is from 5 to 7th of February in Sunnyvale, CA in the US.
Venue is at [Google Sunnyvalue, Building MP1, Moffet Place](https://maps.app.goo.gl/7bcqJiNRxSNqSuU39)

I want to visit it arriving 4th of February Stay for 4 nights and come back.

Registration fee is 475

# Flight

Incoming KL0605 Leaves 10AM from amsterdam and arrives at 12PM in san francisco. 286 - 346
Outgoing KL0606 (8th of feb) 14:05 arrives at 9:15 AM

Currently cost around 1348,- when not paying for luggage.


# Hotels

* https://www.extendedstayamerica.com

# TODO

* [ ] Visa
* [ ] Book conference ticket
* [ ] Book hotel
* [ ] Book flight
* [ ] Get American dollars

