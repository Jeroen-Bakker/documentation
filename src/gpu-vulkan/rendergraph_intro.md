# Introduction to render graphs

Graphics API using OpenGL, Metal, DirectX or Vulkan have a mechanism to track resources and might reorder GPU commands to improve performance. In OpenGL, Metal and pre DX12 this mechanism is a driver responsibility. On DX12 and Vulkan this became an application responsibility.

## Render graphs

Many games and frameworks have all come implemented this mechanism somewhere. Since 2017 this mechanism was named render graph.
The different implementations can be grouped in some categories:

### Pipeline based

Pipeline render graphs - pre record pipelines and how they relate using named resources (or resource slots). When using (draw/compute) these pipelines the named resources are replaced with actual resources. During the prerecording the resource transition and pipeline barriers can already be determined and flattened into a list of commands. When using, the resources are bound and the list of commands are executed.

Pipeline render graphs are typically found in games. Where 1 thread is responsible for all drawing and a small amount of helper thread can do compute and data transfer. For correct working the whole application uses the render graph as the only API to UI/Render engine.

Examples of these are:

### Driver level

### Sequential state tracking

## Compared to our needs

Blender can have multi

## Future target

The pipeline based render graphs fits well with our draw manager approach and would also be a way to reduce CPU cycles by moving part of the draw manager responsibility to the render graph. Using prerecording and named resources would allow the Vulkan backend to don't re-analyze what the draw manager already prepared.

Editor code could use this to replace legacy IMM calls with pipelines.