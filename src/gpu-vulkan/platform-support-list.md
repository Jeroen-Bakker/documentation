# Platform support list

This page lists the platforms that we target to support support in Blender 4.4 using the Vulkan backend.

> [!IMPORTANT]
> This list is not final yet.
> * Platforms can still be removed when we detect issues we cannot work around or takes to much effort to do so.
> * Platforms can be added when driver has been released that can be supported.
> * Newer platforms are added when they are released and drivers become available.

| Vendor | Driver          | Os      | Devices                       | 
| ------ | --------------- | ------- | ------------------------------|
| NVIDIA | 500+            | Windows | GTX 700 - GTX 1000            |
|        |                 |         | RTX 2000 - RTX 4000           |
| NVIDIA | 550+            | Linux   | GTX 900 - GTX 1000            |
|        |                 |         | RTX 2000 - RTX 4000           |
| AMD    | Official/latest | Windows | RX 400 - RX 600               |
|        |                 |         | RX Vega                       |
|        |                 |         | RX 5000 - RX 8000             |
| AMD    | Official/latest | Linux   | RX 400 - RX 600               |
|        |                 |         | RX Vega                       |
|        |                 |         | RX 5000 - RX 8000             |
| AMD    | Mesa            | Linux   | HD 7000 - HD 8000             |
|        |                 |         | R 200, R 300                  |
|        |                 |         | RX 400 - RX 500               |
|        |                 |         | RX Vega                       |
|        |                 |         | RX 5000 - RX 8000             |
| Intel  | Official/latest | Windows | Intel 11th gen iGPU and above |
|        |                 |         | Intel Arc                     |
| Intel  | Mesa            | Linux   | Intel 6th gen iGPU and above  |
|        |                 |         | Intel Arc                     |

* Table shows a devices as series.
* Professional & mobile lines of GPU of the same generation should also be supported.
* Mesa NVK isn't tested and therefore not listed.
* MoltenVK is officially not supported.
* NVIDIA laptops with a hardware switch should work. But developers haven't been able to validate.


| **Devices**                   | **Windows**           | **Linux**                  |
| ----------------------------- | --------------------- | -------------------------- |
| **NVIDIA**                    |                       |                            |
| GTX 700 - GTX 800             | NVIDIA 500+           | *Unsupported*              |
| GTX 900 - GTX 1000            | NVIDIA 500+           | NVIDIA 550+                |
| RTX 2000 - RTX 5000           | NVIDIA 500+           | NVIDIA 550+                |
| **AMD**                       |                       |                            |
| HD 7000 - HD 8000             | *Unsupported*         | Mesa                       |
| R 200, R 300                  | *Unsupported*         | Mesa                       |
| RX 400 - RX 600               | AMD Official/latest   | AMD Official/latest, Mesa  |
| RX Vega                       | AMD Official/latest   | AMD Official/latest, Mesa  |
| RX 5000 - RX 9000             | AMD Official/latest   | AMD Official/latest, Mesa  |
| **Intel**                     |                       |                            |
| Intel 6-10th gen iGPU         | *Unsupported*         | Mesa                       |
| Intel 11th gen iGPU and above | Intel Official/latest | Mesa                       |
| Intel Arc                     | Intel Official/latest | Mesa                       |
| **Qualcomm**                  |                       |                            |
| Adreno X1-85 GPU              | Qualcomm Official     | *Not tested*               |
