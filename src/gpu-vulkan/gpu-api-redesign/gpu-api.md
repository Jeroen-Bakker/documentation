# GPU API Redesign

##

**GPU stack**
    Combination of application and driver GPU related code.

## Current state

Currently we have several APIs related to GPU drawing.

```mermaid
classDiagram
  namespace UI {
    class Editors
  }
  namespace Python {
    class PyGPU
  }
  namespace GPU {
    class GPUBatch
    class GPUImmediate
  }
  namespace Draw {
    class DrawManager
  }
  <<Interface>> GPUBatch
  <<Interface>> GPUImmediate
  <<Interface>> DrawManager

  Editors --> GPUBatch
  Editors --> GPUImmediate
  Editors --> DrawManager
  PyGPU --> GPUBatch
  DrawManager --> GPUBatch
```

### GPUImmediate

`GPUImmediate` is a compatibility layer to use OpenGL pre-core programming model. It was introduced during Blender 2.8 when we switched to the OpenGL core profile.

The API should be deprecated and it usages should be replaced by GPUBatch. However due to the understandability of this API to non GPU developers and not promoting that this API is deprecated it is actually the most commonly used API concerning UI/Editor drawing.

Draw backs of this API is that geometry/data aren't kept on the GPU and data needs to be resent each time.

### GPUBatch

`GPUBatch` uses a shader based approach. Where geometry batches are created and uploaded and when the geometry doesn't change it can be reused by other shaders or for the next frame.

Due to the ability to prepare geometry batches it is faster then GPUImmediate mode.

### DrawManager

`DrawManager` is an API on top of `GPUBatch` that adds a programming model for high performance rendering. It is typically used to draw the 3D viewport where performance matters.

The API uses some best practices on how to order draw commands to reduce context switches. 

## Limitations

Current APIs have a disconnect with how modern GL APIs are structured that leads to non optimal performance and complex translation code in our backends.

### Pipelines
Modern GL-APIs (Vulkan/Metal) provides accessibility to pipelines. A pipeline is a configuration of the GPU when performing drawing or compute commands.

Most change to the pipeline configuration can trigger a recompilation of the pipeline, including a recompilation of the shader. This can already happen by changing the blend mode using `GPU_blend` of using geometry with just a different layout.

### Texture layouts

Pixels of textures that are used in a pipeline must be in a specific layout. The layout depends on how the texture is used inside the pipeline.

There are different layouts for 
- `TRANSFER_READ/WRITE`
- `SHADER_READ/WRITE`
- `ATTACHMENT`
- `PRESENT`

When transferring a texture to a new layout, the previous layout needs to be provided as well. Changing a layout can be done by providing so called pipeline barriers. A pipeline can alter the layout of a whole texture, but also a single layer or LOD level. In this case a texture can have a mixed layout.

More information about pipeline barriers will be provided in the section about resource versions.

### Command reordering

Commands that are send to the GPU stack can be executed in a different order on the GPU. This is done to reduce pipeline recompilation and resource layout transitions.

Depending on the backend the responsibility can be somewhere different. In OpenGL this is a driver responsibility and the application isn't aware and cannot influence the reordering directly. In Metal this is also a driver responsibility, but the application can provide hints to influence the reordering. In Vulkan this is the sole responsibility of the Application.

### Resource versions

With the command reordering in mind it is important to track versions of resources. You don't want to reorder the commands in a way that it uses a different content of the resource.

Every time a pipeline (or CPU code) alters a resource a new version will be tracked. It is the same resource, only the commands before the change are scoped and must not use the content of new resource.

A pipeline barrier can be added before the commands to guard a resource between read and write actions. It also is used to transform the layout of a texture. These pipeline barriers also need to know how resources are going to be used until the next pipeline will be added so it only locks the GPU when it is needed.

### Backend implementation

Currently our APIs are limited to a single batch and logic is required to fulfill the requirements of modern GL-APIs.

Before sending the commands to the GL-API the actual commands are recorded in an intermediate buffer. When the intermediate buffer is send to the GPU (via a flush/finish, or other event) the intermediate buffer is analyzed to reorder commands and to generate the correct pipeline barriers.

It could be that the reordering and pipeline barriers will be the same when looking over frames, but due to the API granularity level the GPU backend doesn't know what it is actually drawing/computing.


## Other software

How do game engines and other GPU frameworks solve this?

### WebGPU/wgpu

WebGPU is a standard to provide low level access to GPU devices on the Web. wgpu is an widely used implementation of this standard. The API is designed in such way that the developer can create a flow between pipelines and point out how resources are used between them. These pipeline flows are called RenderPipelines and ComputePipelines. I used the term pipeline flow as to not confuse vulkan and metal developers with  with graphics pipeline and compute pipeline.

The implementation can extract and cache pipeline barriers with each pipeline flow. The next time a pipeline flow is used the resource handles are updated and the already extracted commands are submitted to the GL-API

> NOTE: the pipeline barriers created are not optimized for performance, but rather for clarity. It generates barriers that are stalling the GPU more than actually needed. Reason is that it only tries to optimize resource usages within a flow o it can reuse the prerecorded commands.


### Godot

Godot has a similar implementation as we do. They have their own GPU API which is also accessible to game developers. The API is shader based. After reaching out to them about what they think would be their target API they responded that they are also inspired by WebGPU. If that WebGPU was defined before they developed their API they might have used it.

### AAA game engines.

Since 2017 there is a lot of presentations done at GDC and other conferences about optimization. Most APIs I have seen are based on a similar approach as WebGPU, but with resource tracking across pipeline flows.

This approach has multiple names, but more often it is called a render graph. The render graph contains nodes. Each node has a list of relations with its resources. Depending on the implementation a single node can contain a single compute pipeline or a flow of compute pipelines. Similar to render nodes (graphics).

Some framework (nicebyte) try to do something smart so they don't need to add a render graph API. They use something similar as vulkan synchronization validation layer does. However there are some limitations to when this can be used. These limitations include multi threaded drawing.

Some differences between Blender and these framework is how drawing and threading is organized. Games typically use one main drawing thread and can have a small number of helper threads. The helper threads are often used for data transfers and compute passes to update textures or the scene (physics). Blender however can have multiple drawing threads for example when performing background rendering, baking or GPU compositing. Each thread has its own context, but eventually submit to the same GPU queue.

References:
- [Render Graph 101](https://blog.traverseresearch.nl/render-graph-101-f42646255636)
- [Godot](https://youtu.be/j1SH1gL7E6A?si=YR8MOltlmsCNI9Uq) / [slides](https://vulkan.org/user/pages/09.events/vulkanised-2024/vulkanised-2024-clay-john-godot.pdf)
- [Vulkan Synchronization Made Easy](https://www.youtube.com/watch?v=d15RXWp1Rqo)  / [slides](https://vulkan.org/user/pages/09.events/vulkanised-2024/vulkanised-2024-grigory-dzhavadyan.pdf)
- [WebGPU](https://youtu.be/SH0N4QmioUw?si=bMw4djgWzuJ9ukH-) / [slides](https://vulkan.org/user/pages/09.events/vulkanised-2024/vulkanised-2024-albin-bernhardsson-arm.pdf)
- Add GDC presentations as well.

## Target API

Our target should be to move the render graph (currently part of the draw manager) as its only API. Usages of other APIs should be migrated to the render graph API.

This API should give the API-user more clarity of what is actually needed. The GPU Backend also gets more context of what the API-user is doing and make better decisions. Also being able to cache decisions for the next time to reduce CPU cycles.

```mermaid
classDiagram
  namespace UI {
    class Editors
  }
  namespace Python {
    class PyGPU
  }
  namespace GPU {
    class GPURenderGraph
  }
  namespace Draw {
    class EEVEE
  }
  namespace GL {
    class Vulkan
    class Metal
  }
  <<Interface>> GPURenderGraph

  Editors --> GPURenderGraph
  PyGPU --> GPURenderGraph
  EEVEE --> GPURenderGraph
  GPURenderGraph --> Vulkan
  GPURenderGraph --> Metal
```

Although the details of the API isn't clear, it is clear that there are several stages when using the API. All code examples here don't represent the final API and should only be read as guide. For now I kept as close to the current Draw manager API.

All details are open for discussion.

### Defining a render graph node.

```cpp
void OutlinePass::init()
{
  if (render_node_info_.is_initialized()) {
    return;
  }

  Pass &pass = render_node_info_.new_pass();
  pass.state_set(DRW_STATE_WRITE_COLOR | DRW_STATE_BLEND_ALPHA_PREMUL);
  pass.shader_set(ShaderCache::get().outline.get());
  pass.draw_procedural(GPU_PRIM_TRIS, 1, 3);
}
```

Defines a template for a render graph node. A render graph nodes can have multiple passes and multiple draw commands per pass.

> NOTE: Some passes for example materials are too complex to cache as the drawing commands and resource bindings and even parameter change to often. I would assume these will be reset every time.


### Syncing a render graph node.

When syncing the resources the render_node_info_ can be used to initialize an instance where the render_node_ and resources are linked.

```cpp
void OutlinePass::sync(SceneResources &resources)
{
  render_node_.init(render_node_info_);
  render_node_.bind_ubo("world_data", resources.world_buf);
  render_node_.bind_texture("objectIdBuffer", &resources.object_id_tx);
}
```

When first used the `render_node_info_` can be analyzed by the GPU backend creating a list of commands that are needed to send to the GPU. These commands wouldn't contain any references to the actual resources. Names or ids could be used inside the list of commands.

After the `render_node_` is initialized it contains a prepared `render_node_info_`. Resources can be added; the added resources will be stored beside the `render_node_info_`. The resources and `render_node_info_` will be merged later on in the drawing process.

### Submitting a render graph node

When it is decided to draw a node the node is sent to the GPU backend.

```cpp
void OutlinePass::draw(Manager &manager)
{
  manager.add_node(render_node_);
}
```
The GPU backend adds the node to the render graph of the current context.
No GPU commands are sent during this phase hence the name `add_node`.

### Context render graph submission.

Eventually the GPU Backend will send the commands to the GPU. Just before this happens the nodes are reordered to reduce pipeline recompilations.
After the order is known the resources can be merged with the commands and the pipeline barriers (already part of the command list) can be updated to use the actual state of the resource.

As the submission happens later in the process we have more information how a specific resource version is used and that can lead to better pipeline barriers.

The commands can be recorded into a GL-API specific command buffer and submitted to the device queue.


## Project phasing

How do we get from the current state to the target state?

## Step 1: Vulkan application responsibility

OpenGL and Metal both have a full render graph or part of it as driver level responsibility. For Vulkan the application is fully responsible to provide the correct calls.

Our Vulkan backend doesn't have a render graph, and lacks performance and correct resource synchronization. Beginning 2024 research and experiments were performed how to solve this. Due to our threading model a low level render graph would be a good solution.

This render graph would not be a replacement for the draw manager but would address be able to translate GPUBatch and GPUImmediate mode APIs to a render graph to create the correct list of commands to send to the GPU.


```mermaid
classDiagram
  namespace GPUVulkan {
      class VKRenderGraph
      class VKBatch
      class VKImmediate
  }
  namespace GL {
    class Vulkan
  }

  VKBatch --> VKRenderGraph
  VKImmediate --> VKRenderGraph
  VKRenderGraph --> Vulkan
```

Prototype is available in https://projects.blender.org/blender/blender/pulls/118963

In stead of having a ComputeNode and a GraphicsNode it also contains many nodes that are specific to the GPUBatch/GPUImmediate APIs.

```mermaid
classDiagram
    class VKDevice
    class VKBackend
    class VKContext
    class VKRenderGraph {
        add_node()
        submit_for_presentation()
        submit_for_read_back()
    }
    class VKResources {
        add_image()
        add_buffer()
    }

    VKBackend *--> VKDevice
    VKDevice *--> VKResources
    VKContext *--> VKRenderGraph
    VKRenderGraph o..> VKResources
    VKRenderGraph *--> VKRenderGraphNode
```

The reason to prioritize the Vulkan specific render graph before the GPU render graph:
- Detailed API for the GPU render graph can be extracted better when a backend is implemented that has a render graph at its core. Current draw manager API was added based on best practices and improving performance on OpenGL backend. Currently it has some parts that doesn't map nicely to Vulkan (or Metal)
- Vulkan is needed for platform support. Some OpenGL platforms don't work, but will using Vulkan.
- Vulkan is needed to reduce shader compilation times in EEVEE-Next
- Vulkan is needed as some GPU features are not available in OpenGL at all.


## Step 2: GPU RenderGraph API

After phasing out OpenGL and based on the VKRenderGraph we can design a GPURenderGraph. Phasing out OpenGL is not a hard requirement, but would reduce the amount of work.

Using test cases we can validate correct working of the Vulkan and Metal render graph implementation.

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPUBatch
  SpaceDraw --> GPUImmediate
  PyGPU --> GPUBatch
  DrawManager --> GPUBatch
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

## Step 3a: API migration

Step 3 is a migration process where the usage of the GPUBatch and GPUImmediate mode APIs are migrated to the GPURenderGraph process. The order of this process can be discussed and depends on the needs the moment we start the migration.

The idea is to keep all APIs working until the whole code base is migrated to the render graph approach.

The order described here is to first migrate GPUBatch calls inside the editors as these are not that many as GPUImmediate or as complex as DrawManager. After getting some experience we can plan the other migrations better.

### Step 3a: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPURenderGraph
  DrawManager --> GPUBatch
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

### Step 3b: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPUBatch
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

### Step 3c: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPURenderGraph
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

### Step 3d: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  PyGPU --> GPURenderGraph
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

## Risks

- The development will spawn over multiple years. Priorities will change what would leave us in an unfinished state. We are currently in this state due to immediate mode drawing. 
- Impact is huge as it touches all existing drawing code in editors, draw manger and python Add-ons.









