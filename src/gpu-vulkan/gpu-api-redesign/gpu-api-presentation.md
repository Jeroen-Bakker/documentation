---
marp: false
paginate: true
size: 16:9
headingDivider: 2
theme: uncover
class: 
    - invert
---

# The future of GPU APIs in Blender

# Current state (April 2024)

- Immediate mode emulation
- Draw Batches
- Draw module

## Immediate mode (emulation layer)


```cpp
GPUVertFormat *format = immVertexFormat();
uint pos = GPU_vertformat_attr_add(format, "pos", GPU_COMP_F32, 2, GPU_FETCH_FLOAT);

immBindBuiltinProgram(GPU_SHADER_3D_UNIFORM_COLOR);

/* Draw a light green line to indicate current frame */
immUniformThemeColor(TH_CFRAME);

immBegin(GPU_PRIM_LINES, 2);
immVertex2f(pos, x, v2d->cur.ymin - 500.0f); /* XXX arbitrary... want it go to bottom */
immVertex2f(pos, x, v2d->cur.ymax);
immEnd();
immUnbindProgram();
```
Ref: `ANIM_draw_cfra`

## Draw Batches

```cpp
GPUBatch *batch = ui_batch_roundbox_shadow_get();
GPU_batch_program_set_builtin(batch, GPU_SHADER_2D_WIDGET_SHADOW);
GPU_batch_uniform_4fv_array(batch, "parameters", 4, (const float(*)[4]) & widget_params);
GPU_batch_uniform_1f(batch, "alpha", alpha);
GPU_batch_draw(batch);
```
Ref: `ui_draw_dropshadow`

## Draw Manager

```cpp
void OutlinePass::sync(SceneResources &resources)
{
  ps_.init();
  ps_.state_set(DRW_STATE_WRITE_COLOR | DRW_STATE_BLEND_ALPHA_PREMUL);
  ps_.shader_set(ShaderCache::get().outline.get());
  ps_.bind_ubo("world_data", resources.world_buf);
  ps_.bind_texture("objectIdBuffer", &resources.object_id_tx);
  ps_.draw_procedural(GPU_PRIM_TRIS, 1, 3);
}

void OutlinePass::draw(Manager &manager)
{
  manager.submit(ps_);
}

```

## API Usages

```mermaid
classDiagram
  namespace UI {
    class Editors
  }
  namespace Python {
    class PyGPU
  }
  namespace GPU {
    class GPUBatch
    class GPUImmediate
  }
  namespace Draw {
    class DrawManager
  }
  <<Interface>> GPUBatch
  <<Interface>> GPUImmediate
  <<Interface>> DrawManager

  Editors --> GPUBatch
  Editors --> GPUImmediate
  Editors --> DrawManager
  PyGPU --> GPUBatch
  DrawManager --> GPUBatch
```

## API GL Relationships

```mermaid
classDiagram
  namespace GPU {
    class GPUBatch
    class GPUImmediate
  }
  namespace GL {
    class OpenGL
    class Metal
    class Vulkan
  }
  <<Interface>> GPUBatch
  <<Interface>> GPUImmediate

  GPUBatch --> OpenGL
  GPUImmediate --> OpenGL
  GPUBatch --> Metal
  GPUImmediate --> Metal
  GPUBatch --> Vulkan
  GPUImmediate --> Vulkan
```

# Limitations

- Current APIs don't reflect modern GL APIs
- APIs don't tell the developer what is actually done on the GPU.
    - Pipelines
    - Resource state tracking
- Developers are not able to express their intent/need to the GPU backend.

## Pipelines

- Reconfigure and executes GPU tasks
- Changes to a pipeline recompiles the active shader and flushes GPU commands
    - `GPU_blend`
    - Change geometry layout of batches
    - ..

## Command reordering

## Limitations (intent/need)

- Caches a stream of commands.
  - Generate correct pipeline/resource barriers.
  - Reorder commands to reduce context switches.
- Driver responsibility for OpenGL
- Driver responsibility for Metal (application can give hints)
- Application responsibility for Vulkan


## Resource version tracking


## Resource state tracking (images)

- Different texture layout required based on usage inside the pipeline.
    - `TRANSFER_SOURCE/DESTINATION`
    - `SHADER_READ/WRITE`
    - `ATTACHMENT`
    - `PRESENT`
- Regions within a texture can be in a different layout.

# Target situation

## API inspired by WebGPU

- RenderGraph
  - Keeps track of resources access/layouts and pipeline transitions
  - API is able to cache previous made decisions and reapply them quickly

## RenderGraph: Initialization (concept)

```cpp
void OutlinePass::init()
{
  if (render_node_.is_initialized()) {
    return;
  }

  Pass &pass = render_node_.new_pass();
  pass.state_set(DRW_STATE_WRITE_COLOR | DRW_STATE_BLEND_ALPHA_PREMUL);
  pass.shader_set(ShaderCache::get().outline.get());
  pass.draw_procedural(GPU_PRIM_TRIS, 1, 3);
}
```
- Can describe multiple passes 
- Can describe resource relationship between passes.

## RenderGraph: Syncing resources (concept)

```cpp
void OutlinePass::sync(SceneResources &resources)
{
  render_node_exec_.init(render_node);
  render_node_exec_.bind_ubo("world_data", resources.world_buf);
  render_node_exec_.bind_texture("objectIdBuffer", &resources.object_id_tx);
}
```

## RenderGraph: Execution (concept)

```cpp
void OutlinePass::draw(Manager &manager)
{
  manager.add_node(render_node_exec_);
}
```

- 


## Target APIs
```mermaid
classDiagram
  namespace UI {
    class Editors
  }
  namespace Python {
    class PyGPU
  }
  namespace GPU {
    class GPURenderGraph
  }
  namespace Draw {
    class EEVEE
  }
  namespace GL {
    class Vulkan
    class Metal
  }
  <<Interface>> GPURenderGraph

  Editors --> GPURenderGraph
  PyGPU --> GPURenderGraph
  EEVEE --> GPURenderGraph
  GPURenderGraph --> Vulkan
  GPURenderGraph --> Metal
```

# Small steps

- First Vulkan only implementation

## Step 1: Vulkan application responsibility

```mermaid
classDiagram
  namespace GPUVulkan {
      class VKRenderGraph
      class VKBatch
      class VKImmediate
  }
  namespace GL {
    class Vulkan
  }

  VKBatch --> VKRenderGraph
  VKImmediate --> VKRenderGraph
  VKRenderGraph --> Vulkan
```

## Step 2: GPU API

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPUBatch
  SpaceDraw --> GPUImmediate
  PyGPU --> GPUBatch
  DrawManager --> GPUBatch
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

## Step 3a: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPURenderGraph
  DrawManager --> GPUBatch
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```
## Step 3b: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPUBatch
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

## Step 3c: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPUBatch
      class GPUImmediate
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  SpaceDraw --> GPUImmediate
  PyGPU --> GPURenderGraph
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```

## Step 3d: API migration

```mermaid
classDiagram
  namespace UI {
    class SpaceDraw
  }
  namespace Python {
    class PyGPU
  }
  namespace Draw {
    class DrawManager
  }

  namespace GPU {
      class GPURenderGraph
  }
  namespace GPUMetal {
    class MTLRenderGraph

  }
  namespace GPUVulkan {
    class VKRenderGraph
    
  }

  SpaceDraw --> GPURenderGraph
  PyGPU --> GPURenderGraph
  DrawManager --> GPURenderGraph
  GPURenderGraph <|-- VKRenderGraph
  GPURenderGraph <|-- MTLRenderGraph
```





# Research

- Godot
- AAA game engines
- WebGPU