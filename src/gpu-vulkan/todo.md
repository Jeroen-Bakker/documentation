# TODO

## General development

* [x] Texture Samplers aren't implemented and uses a single pre configured sampler.
  * (114827)[Implemented with https://projects.blender.org/blender/blender/pulls/114827]
* [x] Implement DrawList. Currently DrawList draws per item, but should render all items in a single command.
* [ ] Clear/Load/Store state in render passes. Michael is working on a new proposal that would solve the issue that render passes are cleared when dispatching a compute pass.

## Bugs

* [x] Multi material meshes only display the first material. Other parts of the mesh isn't drawn.
  * (115190)[https://projects.blender.org/blender/blender/pulls/115190]: Issue was that both the binding and the indirect buffer offsetted the starting index. I am unsure if we actually need offsetted index buffer binding.
* [ ] Validation errors with device only streaming buffers. (Workbench materials for example are device only/dynamic. We should go over our definitions of device only, streaming, dynamic, static and based on them find better usage flags for the different buffers. 

## Blender 4.2

* [ ] (117992)[https://projects.blender.org/blender/blender/pulls/117992] check should be enabled again

## Missing texture formats

- [ ] Miguel mentioned failing tests
```
[  FAILED  ] GPUOpenGLTest.texture_1d_array
[  FAILED  ] GPUOpenGLTest.texture_1d_array_upload
[  FAILED  ] GPUOpenGLTest.texture_roundtrip__GPU_DATA_UINT__GPU_DEPTH32F_STENCIL8
[  FAILED  ] GPUOpenGLTest.texture_roundtrip__GPU_DATA_UINT__GPU_DEPTH24_STENCIL8
[  FAILED  ] GPUOpenGLTest.texture_roundtrip__GPU_DATA_UINT__GPU_DEPTH_COMPONENT32F
[  FAILED  ] GPUOpenGLTest.texture_roundtrip__GPU_DATA_UINT__GPU_DEPTH_COMPONENT24
[  FAILED  ] GPUOpenGLTest.texture_roundtrip__GPU_DATA_2_10_10_10_REV__GPU_RGB10_A2UI
```

## Overlay

* [x] Depth write is turned off and renders grid on top of extra objects. `p_extra_ps`. In OpenGL depth write is on.
  * Cause: `VKPipelineState::state` wasn't initialized properly. (https://projects.blender.org/blender/blender/pulls/114792)

## EEVEE

* [ ] Subpasses - New design needs to be implemented where the required transitions are pre-recorded.
* [ ] NVIDIA renders black (Also seen on Intel UHD)

## Cycles

* [ ] Pixel buffers
* [ ] Cuda/HIP/OneAPI support - might require a different workflow 

## OpenXR

* [ ] OpenXR is currently OpenGL only.

## Color management

* [ ] Background and Backdrop is rendered whit-ish. 

## Performance

* [ ] Track resource usages and report on missing barriers (debug mode). Check WebGPU, nice.graphics and synchronisation_valid.
* [ ] Use synchronisation2 with layer for unsupported devices.

## Device Support

* NVIDIA 1080TI stalls using official 535 drivers during submission.
* MoltenVK Seems like DPI settings are incorrectly determined and failed blitting.

## UI/UX

These are typically nice to have features and will be added at the end of the project, or when
there is a need by the project itself. List is prioritized.

* [x] [https://projects.blender.org/blender/blender/pulls/115184] Display memory statistics. Was needed to inspect potential memory issues.
* [ ] Select rendering device
* [ ] Switch between OpenGL/Vulkan


