# Vulkan Project planning (2024Q2)

It seems like the vulkan project will be continued, which would be amazing. I was asked to see if
and when more people could work on the vulkan project. Let's quickly start with an overview of what
needs to be done.

## Overview

```mermaid
gantt
    title Vulkan overview
    dateFormat YYYYMMDD
    axisFormat %
    
    section GPUBackend/EEVEE
        Render graph          : active, rg,        20240401, 10w
        Framebuffer clear ops :         clear_ops, after rg, 3w
        Subpasses             :         subpasses, after rg, 1w

    section Interoperability
        Cycles                :         cycles,    after rg, 6w
        OpenSubdiv            :         subdiv,    after rg, 6w
        OpenXR                :         openxr,    after rg, 1M
```

From the module point of view `Render Graph`, `Framebuffer clear ops` and `subpasses` are the most
important as that would enable us to release a Blender version with a back-end switch. The interoperabilty
tasks are required before the Vulkan Backend becomes default. Between the initial release and becoming
default Cycles (and other external render engines) will not be supported. Of course this will
be clearly communicated to the user when switching the GPU backend.


## Break-down: Render graph

**GOAL**
    Optimize the commands that are send to the GPU to ensure optimal performance. This is done by
    programmatically adding barriers, change image layouts and reordering/combining commands to
    minimize GPU stalls.

**Context**
    Render graph is part of any Graphics/Compute stack. In OpenGL and Metal this is part of the
    driver stack. For modern graphics APIs like Vulkan/DirectX 12 this is an application responsibility.
    The reason for that is that applications can tailor the implementation to their specific needs.

**Industry validation**
    The implementation of the render graph is key for any vulkan implementation. Industry experts
    (like VulkanWG, Godot, WGPU, Google android) were involved during the design process. We also
    looked in reusing existing render graphs (WGPU, Google Angle) to speed up the implementation.
    The project conclusion was that reusing an existing render graph didn't suit the requirements
    we need.

**Design task**
   [blender/blender#118330]

**Development PR**
    [blender/blender#118963]

The next graph shows the break down of the render graph. Light-grey tasks have already been
completed, red tasks are tasks that are on the critical path. the tasks `ComputeDispatchIndirect` and
`Mipmap generation` can be done when needed as they are not part of the critical path. Technically
this could be done by a second developer.

```mermaid
gantt
    title Render graph
    dateFormat YYYYMMDD
    axisFormat % 

    section Transfer
        Resource tracking Buffer: done, res_buffer,            20240401,                1w
        Resource tracking Image : done, res_image,             20240401,                1w
        VKStorageBuffer         : done, vk_storage_buffer,     after res_buffer,        1w
        VKVertexBuffer          : done, vk_vertex_buffer,      after vk_storage_buffer, 1w
        VKIndexBuffer           : done, vk_index_buffer,       after vk_storage_buffer, 1w

    section Compute
        ComputePipeline         : done, vk_compute,            20240401,                1w
        ComputeDispatch         : done, dispatch,              after vk_storage_buffer, 1w
        ComputeDispatchIndirect : done, dispatch_indirect,     after vk_storage_buffer, 1w
        VKImage                 : done, vk_image,              after res_image,         1w

    section Graphics
        Mipmap generation       :       mipmap,                after vk_image,          1w
        VKImageView             : crit, vk_image_view,         after vk_image,          1w
        VKFramebuffer           : crit, vk_framebuffer,        after vk_image_view,     1w
        VKRenderPass            : crit, vk_render_pass,        after vk_framebuffer,    1w
        GraphicsPipeline        : crit, graphics,              after vk_render_pass,    2w
        Draw                    : crit, draw,                  after graphics,          1w
        DrawIndexed             : crit, draw_indexed,          after draw,              1w
        DrawIndirect            : crit, draw_indirect,         after graphics,          1w
        DrawIndexedIndirect     : crit, draw_indexed_indirect, after draw_indirect,     1w
        Start Blender           : crit, start_blender,         after draw_indexed,      2w
```

> NOTE: all tasks until the `Start Blender` can only be validated using existing test-cases or via GPU debuggers.

## Resource need from project point of view

The vulkan project is setup as a quality oriented project. When the Vulkan backend has an issue, this
will reflect other projects. 

For 2024Q2 the project would mostly have full-time work for 1 developer. The work must be code-reviewed
by at least one other developer. Clément is the first who comes to mind, but would rather also have
other developers (Miguel, but also Brecht) to have a look at it. The render graph will not
land as a single commit and review of the data transfers/compute can already start. My personal goal
would be to have the Vulkan backend finished and hand over to the GPU module at the end of this quarter.

What would help the project is to continue with the [GPU Module roadmap](https://projects.blender.org/blender/blender/wiki/Module:%20EEVEE%20&%20Viewport). 
Overlay-next/selection-next are required before the Vulkan backend can be enabled by default.
- Overlay-Next [blender/blender#102179]
- Selection-Next [blender/blender#102177]

For 2024Q3 the project will be in a different phase and performing the remaining tasks could be assigned
to multiple developers.

> NOTE: When discussing Interoperability with the VulkanWG they explicitly tell me that interoperability
> with Vulkan is complex. IMO the main reason is that resource synchronization needs to happen between
> external queues and ownership of resources needs to be explicitly transferred.