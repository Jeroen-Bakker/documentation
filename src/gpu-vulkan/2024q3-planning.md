# Vulkan Project planning (2024Q3)

## Current state

- **The goals of [2024Q2](2024q2-planning.md) have been met!**
- Vulkan backend has been reimplemented using a render graph. The render graph keeps track of resource states and vulkan commands in order to reorder the commands, insert appropriate synchronization barriers and perform resource layout transitions.
- Performance is comparable with OpenGL. Note that the current backend still has many head room for optimization. **mr_elephant.blend**: *OpenGL 11.02ms*, *Vulkan 10.50ms*
- EEVEE works with the exception of volume probes. All features have been implemented.
- There are several known platforms that have support issues. These include:
  - ~~[NVIDIA](blender/blender#114385) (Platforms doesn't seem to meet our minimum requirements).~~
  - Steam deck viewport is flickering and text rendering shows a layout transition/synchronization issue. (this is not about the steam deck, but its platform is also use on other devices).
  - ...
- Animation playback or complex scenes can lead to missing device errors.
- Background rendering exits in a faulty render graph. Fix is easy, just need to be sure why it gets in this state.

![Wanderer](20240620_vulkan-wanderer.png "Wanderer scene rendered in Vulkan")

## Goal for Q3

**Stabilize & improve to make Vulkan available in an official release.**

> NOTE: 4.3 is possible, but might slip to 4.4 due to summer holidays.

### In scope

- Stabilize the vulkan backend and fix issues mentioned above.
- Improve (startup) performance
  - Threaded shader compilation
  - Persistent pipeline cache
  - Use shader library extension to increase cache hits
- Improve render performance by adding support for
  - [VK_KHR_dynamic_rendering_local_read](https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_dynamic_rendering_local_read.html)
  - [VK_EXT_descriptor_buffer](https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_EXT_descriptor_buffer.html)
  - [VK_KHR_fragment_shader_barycentric](https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_fragment_shader_barycentric.html)
  - Bindless rendering.
  - Test and implement more command reordering/pruning strategics.
  - Improve cache prefetching of the render graph
  - Improve GPU occupancy.
- Improve device support and reporting.
- Make cycles display work with vulkan backend.
- Add preference to switch between OpenGL and Vulkan. Including a device selector.

> NOTE: this might look as many tasks, but most tasks are fairly small (or have been prepared to fit inside the render graph architecture). Would expect that most task have a development time of 1-2 days and should in average fit in a lead time of 1 week.

> NOTE: People interested in GPU development are welcome to help out. There is  interest from the Vulkan community to help out (advice/actual development). But I am not pressing people there.

> NOTE: Besides this there is an interest from the Viewport/EEVEE module to experiment with HWRT in the viewport. This effort should be handled prototyping and could lead to development for 2024Q4.

### Out of scope

- GPU based subdivision; no investigation done on the vulkan support in OSD. 2 years ago it wasn't existing, but Pixar developers where looking into it. Doing a [quick search](https://graphics.pixar.com/opensubdiv/docs/release_36.html) it should be supported. We might want to join the effort to enable OSD for Vulkan and Metal but would still require some detailed analysis about the feasibility and complexity.
- OpenXR support; Should be added, seems like a medium project. Current ecosystem is unclear to me ATM.
