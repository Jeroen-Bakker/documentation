# Vulkan Configurator

Vulkan configurator is a powerful tool when developing the Vulkan backend. 

## Validation layers

Vulkan runtime doesn't perform validation. The runtime expects that all
parameters are correct. To identify correctness validation can be activated.

Validating correct usage of vulkan is complex and the validation layers need
to balance between performance and correctness. Sometimes there are known issues
in the validation layers. Gladly vulkan configurator allows to disable some
checks that are known to not work for us.

- **VUID-VkBufferCreateInfo-size-06409**: fault reports that the buffer size
  must be less than 0. This has been fixed upstream, but isn't part of the
  preferred SDK. [[validationlayers#7189](https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/7189)]
- **VUID-VkGraphicsPipelineCreateInfo-pMultisampleState-09026**: faulty reports
  that `pMultisampleState` is `nullptr` if it is filled, and correct. This is
  caused when adding support for dynamic rendering to the validation layers
  [[validationlayers#8015](https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/8015)]


