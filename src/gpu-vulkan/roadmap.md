# Vulkan Roadmap

After discussing projects for 2024 we detected a misperception of what Vulkan can deliver to Blender.
This document will try to clarify how the current project and future project depends. For clarity I
will refer to the current project as Vulkan GPU Backend.

As this was collected in limited time this should not be considered an official roadmap, as validation
steps in the process were skipped. The goal is to align decision making in the project planning for
2024. We can consider this a draft.

For a more detailed overview about the current project I would recommend to watch the first part of
the Blender conference [presentation](https://video.blender.org/w/bnrF73ZFiXpLMUXN2E5Diq). It covers
reasoning why this project is executed, project timeline and sketch some outlines that is used to
when constructing this roadmap.

The timeline has shifted as after the conference I was asked to help out on other projects.

Other information resources are the weekly meetings with other members of the EEVEE/Viewport module,
the discussions I had with Clement about multiple topics and information received from partners. I 
also included some questions/expectations that Ton raised.

## Overview

> **NOTE**: I used limited time to describe the projects so more projects can be defined with user benefits, but I just didn't had the time to do so. I didn't had the time to add projects such as asynchronous compute and the impact that could have on Blender. Also projects like supporting the script node, GLSL->HSL conversion
>
> Eventually tickets should be created for the projects that are validated by the module and admins 

```mermaid
flowchart LR
    Vulkan([Vulkan GPU Backend])
    OptimizingUI([Optimizing Editor Drawing])
    OpenGLDeprecation([Deprecating OpenGL])
    OpenXRVulkan([Vulkan OpenXR Support])
    MetalVulkanGPUAPI([Alignment Metal/Vulkan])
    ThreadedDrawing([Threaded Drawing])
    NonBlockingRendering([None Blocking Rendering])
    DeviceSelection([Device Selection])
    HWRaytracing([EEVEE Hardware Raytracing])
    MeshShading([Mesh Shaders])
    GeomStreaming([Geometry Streaming])
    EEVEEShadowPerformance([EEVEE Shadow Performance])
    HSL([Replace GLSL with HSL])
    ScriptNode([Add support for HSL in Script node])
    OSLLite([Support for limited OSL])
    ParallelCompilation([Parallel Material Shader Compilation])


    Vulkan --> OptimizingUI
    Vulkan --> OpenXRVulkan --> OpenGLDeprecation
    OpenGLDeprecation -->  MetalVulkanGPUAPI
    OpenGLDeprecation --> NonBlockingRendering
    MetalVulkanGPUAPI --> ThreadedDrawing
    Vulkan --> DeviceSelection
    Vulkan --> HWRaytracing
    Vulkan --> MeshShading
    MeshShading --> GeomStreaming
    MeshShading --> EEVEEShadowPerformance
    OpenGLDeprecation --> HSL
    HSL --> ScriptNode
    ScriptNode --> OSLLite
    Vulkan --> ParallelCompilation
```

In a nutshell the user benefits can be summed up as:

* Improve overall performance in Blender
* Handling of large scenes and large geometries.
* Improve user experience by removing blocking interfaces
* Improve quality in AO, GI, Shadows, Reflections and Refractions.
* Script Node support
* Parallel material shader compilation

If more is required, the module should reserve more time in updating the roadmap.

## Projects

### Vulkan GPU Backend

|          |               |
|--------:|:--------------|
| **Goal** | Implementation of the Vulkan GPU Backend, feature parity with OpenGL performance close to OpenGL. |
| **Status** | Paused. |
| **Timeline** | 1 quarter fulltime for feature parity, 1 quarter fulltime for performance. I expect an initial maintenance period to take 1 day a week for 1 release period. |
| **User Benefits** | Blender would run more stable and more consistent between GPU generations. |
| **Developer Benefits**| Improve the development eco system by the tools that Vulkan delivers. This has already been valuable for OpenGL EEVEE development. |

There are still 2 milestones that needs to be achieved. 

#### Feature Parity

Currently the feature parity is to a level that editor drawing and workbench is considered done.
EEVEE is in Progress, Cycles hasn't started but we know it requires some changes due to API
differences with Cuda/HIP which needs to be validated with the Cycles team.

#### Performance

The approach of the project is to first have feature parity, before starting on optimizing. The reason
is a lack of information sources and personal experience of Vulkan API at the start of the project.
Vulkan API is gives closer access to how a GPU is internally structured and would perform best. The
API was also driven from game engines. This resulted in that the API requires recipes of how an
application will access the GPU. These recipes and called pipelines. Multiple pipelines and dependencies
between pipelines also needs to be pre-compiled for better performance (renderpasses). Games typically
have a limited set of pipelines and renderpasses that are know in advanced and precompiled when
installing a game.

For Blender this isn't currently feasible. When starting Blender 1000 of these pipelines are needed and
based on the actual content and what the user is doing. New pipelines are needed all the time.
Due to the current API of the GPU module renderpasses cannot be used optimal.

### Optimize Editor Drawing

|          |               |
|--------:|:--------------|
| **Goal** | Improve performance by optimize editor drawing code | 
| **Status** | Started - Community Effort |
| **Timeline** | Expectation that this could be 1 month of work when not done as community effort. It could be timeboxed. |
| **User Benefits** | Blender UI would become more fluent when areas requires redrawing for bigger scenes. |
| **Developer Benefits** | Code will be more centralized. |

Project isn't Vulkan related, but during Vulkan GPU Backend development it came clear that several
editors code slowed down drawing because the way how commands where send to the GPU module. Leading
to sending multiple commands that could have been combined in a single command. During development
of those editors it wasn't that obvious, but when using bigger scenes this will make a difference.

This project has been started as a community effort. The video sequencer has already been improved
* [Sequencer](https://projects.blender.org/blender/blender/pulls/115311)
* [Outliner](https://projects.blender.org/blender/blender/issues/114265)

There might be more editors that require a look at, but the approach would be similar compared to
the mentioned tickets.

### Vulkan OpenXR Support

|          |               |
|--------:|:--------------|
| **Goal** | Ensure OpenXR can run on Vulkan. | 
| **Status** | Proposal |
| **Timeline** | Expect 1 month of work |
| **User Benefits** |  |
| **Developer Benefits** |  |

Currently OpenXR is only supported via OpenGL or by using a dual context with DirectX.
The Before OpenGL can be deprecated we need to ensure
all code paths are supported and use Vulkan.


### OpenGL Backend Deprecation

|          |               |
|--------:|:--------------|
| **Goal** | Replace the default backend on Windows/Linux to Vulkan, remove OpenGL | 
| **Status** | Defined |
| **Timeline** | Is more a milestone, so expect 3 weeks of work removing code + support |
| **User Benefits** |  |
| **Developer Benefits** | Less code to maintain, less platforms to maintain, raising availability of GPU features |

OpenGL Backend Deprecation is an important milestone on the roadmap. OpenGL platform support can
be disruptive for active project development. OpenGL platform support is driven by 2 factors:
Blender features and driver updates.

Blender features that uses GPU are hard to validated on the different platforms as we don't have
access to all the platforms we support. It is also not realistic. Mostly when a mayor release of
Blender (3.5, 3.6) is imminent it takes between 2 days and 3 weeks of support to ensure that recent 
developments are compatible with all the different platforms out there.

Driver updates especially for older platforms can break Blender. OpenGL drivers contains a compiler.
The compilers are often tailor made for a driver and driver validation in OpenGL doesn't cover
most cases. Some drivers are also locked and cannot be updated, removing flexibility to work around
issues. GLSL should also not be considered a single language. It has several dialects and each of
them their own quirks.

To provide an example of recent release where a feature and driver didn't work and took me quite a
long (leap) time to solve due to availability, feature change and driver compiler issues:

* [#113235](https://projects.blender.org/blender/blender/issues/113235) Voronoi texture
* [#113124](https://projects.blender.org/blender/blender/issues/113124) Driver Crash

In Vulkan this isn't an issue as Blender is responsible for the compiler and can fix to a single
GLSL dialect.

The activities that are done for this project are
* Removing the OpenGL backend from GPU, GHOST, Python Module, Build configuration, test suites etc.
* Updating development documentation, end user manual, and minimum requirements.
* Maintenance in case stuff breaks.

### Metal Vulkan API
|          |               |
|--------:|:--------------|
| **Goal** | Align GPU module API closer to Metal And Vulkan | 
| **Status** | Proposal |
| **Timeline** | TBD, Medium complexity |
| **User Benefits** | Improve performance |
| **Developer Benefits** | Reduce code duplication |

After OpenGL is removed we can change the GPU API to be more aligned to what modern GL APIs require.
How this will look like is still unclear, In the current API there are already improvements to
utilize some of this for the Metal backend (subpass rendering, tiled base GPU support).

### Non Blocking Rendering

|          |               |
|--------:|:--------------|
| **Goal** | Remove the limitation that EEVEE rendering blocks the Blender UI | 
| **Status** | Defined |
| **Timeline** | TBD, Medium complexity |
| **User Benefits** | Improve performance, better user experience |
| **Developer Benefits** |  |

The GPU contexts are currently globally defined. Moving context to thread local storage (or similar
solution) would allow users to do EEVEE/Workbench renders without blocking the UI.


### Threaded Drawing

|          |               |
|--------:|:--------------|
| **Goal** | Use multi-threading to reduce idle time of GPUs | 
| **Status** | Proposal |
| **Timeline** | TBD, High complexity |
| **User Benefits** | Improve performance |
| **Developer Benefits** |  |

Vulkan promises that drawing commands can be given to the GPU from different threads, reducing the
idle time of GPUs. There are two approaches, and both of them can solve a part of the solution.

NVIDIA GPUs have multiple command queues in their hardware architecture. Each queue can facilitate
all drawing commands that originate from the same CPU thread. AMD doesn't have multiple command queues
and are collected in a single command queue on their hardware.

Vulkan has the concept of a command buffer which records commands that can be send to the GPU. The
command queue can be filled from multiple thread, but only when several requirements are met.

Commands filled from a thread needs to be recorded to an secondary command buffer. The secondary
command buffer is then recorded to a primary command buffer the primary command buffer is sent to the
GPU. When using secondary command buffers it is not allowed to make changes to render passes and
frame buffers. This in the current design limits us to be not that useful.

### Device Selection

|          |               |
|--------:|:--------------|
| **Goal** | Let user decide which GPU device will be used for drawing, rendering and baking | 
| **Status** | Proposal |
| **Timeline** | TBD, Low-Medium complexity |
| **User Benefits** | More flexibility in platform, Improved performance when rendering |
| **Developer Benefits** |  |

Current Vulkan backend already supports GPU device selection, but it is hidden for the user. I use
it to test Vulkan Backend changes against multiple GPUs.

This project would allow the user to select any GPU device in their system. This would improve the
performance as the users can select a different device for background render instances and foreground
instances of Blender.

Depending on the need, but would increase the complexity of the project. A different GPU device could
be selected based on the task being performed (baking, rendering, UI).

### EEVEE Hardware Raytracing

|          |               |
|--------:|:--------------|
| **Goal** | Use GPU Hardware Raytracing features in the raytracing pipeline of EEVEE.| 
| **Status** | Proposal |
| **Timeline** | TBD, hardware raytracing=Medium complexity, software raytracing=High Complexity |
| **User Benefits** | Increase quality GI, reflections, refractions, shadowing, AO|
| **Developer Benefits** | Hardware raytracing is easier to maintain, compared to software raytracing |

EEVEE supports different raytrace methods. In the current implementation there is a method that
skips raytracing and one to use screen spaced raytracing. The idea behind this is that raytracing
methods can be added and selected by the user to support their usecase/workflow. Hardware raytr

In the planning is to add a Scene/World based raytracing methods. We need one for GPUs that don't
have hardware raytracing support (aka software based raytracing) and we could optionally add one using
the hardware raytracing features of the GPU. This is similar to CUDA vs OPTIX, HIP vs HIPRT in Cycles.

The benefit of using hardware raytracing method is that the vendor can optimize the method based on
the platform. The API for this is standardized, but requires more GPU memory and the implementation
is a black box. For the software based raytracing we have more control and can tailor it to a our
workflow, but the complexity is high.

Developments are already discussed to add hardware raytracing support to the Metal Backend. It is
not desired to keep this a Metal only feature.

### Mesh Shading

|          |               |
|--------:|:--------------|
| **Goal** | Add Mesh Shading API to GPU module.| 
| **Status** | Defined |
| **Timeline** | TBD, Medium complexity |
| **User Benefits** | |
| **Developer Benefits** |  |

Mesh Shading is going to be used in the Metal backend to improve the vertex shading stage of
shadow rendering of EEVEE. This doesn't add a reusable API to the GPU module yet. For better
alignment with Vulkan and future features we should define an API.

### Geometry Streaming

|          |               |
|--------:|:--------------|
| **Goal** | Use geometry streaming to improve large scene rendering performance | 
| **Status** | Proposed |
| **Timeline** | TBD, Medium complexity |
| **User Benefits** | Auto LOD, Large Scenes, Lower rendering times |
| **Developer Benefits** |  |

Geometry Streaming (Unreal Nanite) would allow rendering/drawing of large scenes and detailed
models with less impact on performance then we currently have.

Note that Mesh Shaders was added as dependency, but Unreal itself uses compute shaders. Optimizing
mesh shaders often requires vendor specific tweaks.



### EEVEE Shadow Optimizations

|          |               |
|--------:|:--------------|
| **Goal** | Improve performance of EEVEE Shadow Optimizations | 
| **Status** | Defined |
| **Timeline** | TBD, Low-Medium Complexity |
| **User Benefits** | Improved performance |
| **Developer Benefits** |  |

Using mesh shaders it is possible to reduce the selection what needs to be updated. This is
already in the making for Metal, but hidden in the backend.
