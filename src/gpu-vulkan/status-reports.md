# Vulkan - project status reports

Projects are asked to do a status report on a regular basis. To keep track of the consistency of these status reports the Vulkan project collect them in a single file.

## 2025-02-21

Vulkanised 2025 has happened. A presentation about [transitioning to Vulkan](../vulkanised2025/T3-Jeroen-Bakker-Blender%20Institute.pdf) was presented. Check [here](../vulkanised2025/vulkanised2025-report.md) for the report. 

Main development is done on [blender/blender!133716]. OpenGL specific shaders are ported to use shader create info. This allows the shaders to be cross compiled to Metal and Vulkan. Most shaders are easy to migrate with the exception of patch evaluation [blender/blender!134917]. These shaders includes external GLSL. After all shaders are migrated we will use the new OSD API for OpenGL, before tackling Metal and Vulkan. 

**Short term goals**

- Port all SubDiv shaders to use shader create info.
- Use new OSD API for OpenGL
- Enable Metal + OSD and fix remaining issues
- Enable Vulkan + OSD and fix remaining issues

## 2025-02-07

Prepared for the Vulkanized 2025 conference next week. I will be given a presentation about how Blender is transitioning towards Vulkan. Final presentation will be distributed via the conference channels. Last year was amazing I learned a lot and hope to do the same this year. My goal would be to know more about how vulkan driver conformance testing is organized. We had several issues with drivers in the past related to the conformance testing.

- [blender/blender!133981, blender/blender!133877] Rest of the time mostly worked on fixing EEVEE and compositor tests. The Vulkan backend can successfully pass those tests. Workbench render tests are failing due to texture filtering issues hence we still hide it behind an CMAKE option.
- [blender/blender!134020] Compiling shaders could stall. The reason was that shaders where marked completed, before the final results were stored in the correct location. Leading to incorrect linking of those shaders.
- [blender/blender!134032] Removed some resource locking that doesn't guard anything anymore.

**Short term goals**

- [https://www.vulkan.org/events/vulkanised-2025] Vulkanized 2025
- [blender/blender!133716] OpenSubDiv: Cleaning up current GPU based open subdiv implementation to prepare to be enabled for Metal and Vulkan.

## 2025-01-24

Didn't work much on the Vulkan project due to several high priority issues in the Viewport & EEVEE module.

[blender/blender!133535] The threaded shader compiler use a BKE/GHOST function that was not thread safe, leading to reading and writing spirv files in different places. This has been solved by reading the cache folder when Blender starts and use this folder when accessing spirv files. 

[blender/blender!133485] Using Cycles had a big performance regression. The reason was that Cycles uploaded the result to the GPU, but the Vulkan backend used a CPU roundtrip when updating the viewport. Resulting in a huge performance degradation. 

[blender/blender!133528] Using scenes on non-rebar capable systems could lead to crashes as device only buffers where allocated with mapping capability. This combination is very limited on those systems and would crash when this memory area didn't had any space left for allocation. This was solved by giving the calling code more control over the type of memory allocation it should do. This was already the plan, but now it was required to fix an actual issue.

[blender/blender!133528] The design of the new device command builder is documented at https://developer.blender.org/docs/features/gpu/vulkan/render_graph/ 

**Short term goals**

- Fix test cases for [blender/blender#132682]
- Prepare presentation for Vulkanised 2025


## 2025-01-10

Vulkan backend has two main performance issues. Command building and uploading data to the GPU. Some tests have been conducted to see how using threading during command building will effect performance. [blender/blender!131965] The result was that using multiple threads would block inside the driver. On NVIDIA it was is a mutex, and on AMD/Mesa it seems to suffocate the PCI lanes. End result was similar performance as a single thread.

[blender/blender#132682] With this knowledge a new approach was drafted where a single background thread is continuously building the command buffers of render graphs and send them to the GPU. This makes command pool/buffers easier and allows drawing threads to prepare the next render graph when the previous is build and submitted to the GPU. Current state of is that the basics are implemented and slowly the training wheels are removed. Nothing concrete to report on the performance gain yet.

**Short term goals**

- Continue developing on [blender/blender#132682]
- Prepare presentation for Vulkanised 2025
- Update developers documentation 

## 2024-12-06

Last weeks mainly worked on fixing reported issues and improve platform support. Community is helping out a lot already with reporting issues and also providing patches. Upcoming weeks more time will be allocated to get these patches landed.

[blender/blender!131053] Optimize EEVEE by using VK_EXT_dynamic_rendering_local_read. This extension allows us to read from an attachment that has been written to during the same rendering scope. EEVEE uses this during stencil classification and shadow rendering. The PR is coming along fine, but requires more testing and validation so it works on all platforms.

[blender/blender!131261] Use GPU synchronization. Was always the goal of the Vulkan backend, but didn't become a high priority. It is good to see that some experienced Vulkan developers are taking on the task. GPU synchronization will remove CPU synchronizations when it is not needed, making the GPU continue the work without interference of the CPU. This PR makes use of Semaphores for the swapchain and timeline semaphores for sequential submission.

**Short term goals**

- Improve stability of Vulkan backend by fixing bugs.
- Create devtalk feedback thread.
- Improve quality of the Vulkan backend by documenting and fixing internal issues.
- ShaderC [blender/blender!129971] and Vulkan Headers [blender/blender!128995] have been updated. Libs still need to be updated for some platforms. Poking platform maintainers


## 2024-11-21

Blender 4.3 is officially released and the first reports related to the Vulkan backend are created. Inspecting the first reports, most were expected due to the current state of the backend
- Crashes when using OpenGL GLSL directly in add-ons; however we improved this situation
- Memory leak when using the backend for a long time
- Graphics glitches that were already reported during Alpha/Beta
- Selection bugs due to temporarily disablement of GPU based depth picking

[blender/blender!129062] For 4.4 a workaround for devices that don't support dynamic rendering was added. This would allow us to support more devices (mostly AMD/Intel legacy platforms). However we still having issues with legacy Intel Windows drivers. Workbench is working, but the driver isn't able to handle the complexity of some EEVEE shaders. When that happens `vkCreateGraphicsPipelines` command return `VK_SUCCESS`, but with a `VK_NULL_HANDLE`. This isn't expected and not something we can easily work around.
Next step here is to report out findings to Intel in order to get some insight and as a step of remove support of these devices when we remove OpenGL backend. It is still undecided which Blender version this is.

[blender/blender#127768] One reason why the Vulkan backend was hidden behind developers extra option was that selection was not reliable on NVIDIA/AMD official drivers. Blender 4.3 was released and some reports where coming in for other related selection issues. The root cause of these issues was found. The cause was that reading back the depth from a depth stencil buffer was not implemented. Adding support for these data types and small code cleanups also fixed the original selection bug.

**Short term goals**

- Improve stability of Vulkan backend by fixing bugs.
  - [blender/blender#129678] Synchronization
  - [blender/blender#127225] Memory leak
- Report findings to Intel.
- Update ShaderC [blender/blender!129971] and Vulkan Headers [blender/blender!128995] for Blender 4.4. Poking platforms maintainers for validation/approval.


## 2024-11-08

Last period not much of development did happen due to preparation, execution and aftermath of the conference.
This week was decided to move the Vulkan backend behind "Developer extras" option in the preferences [blender/blender#129799]. This to communicate better to the user that it is still in experimental stage. The rest of the week was improving the stability for the Blender 4.3 release.
- [blender/blender#129863] Fix selection issues for NVIDIA/AMD official drivers
- [blender/blender#129775], [blender/blender#130016] Fix several issues for ARM based devices.
- [blender/blender#129957] Fix several test cases and enabled test cases that were supported
- [blender/blender#129708] Fix crash when drivers don't perform any limit checks.
- [blender/blender#129265] Fix render artifacts when using AMD official driver

**Short term goals**

- Improve stability for Blender 4.3 release
- Update ShaderC and Vulkan Headers for Blender 4.4

**Longer term goals**

- In November the goal is to add support for more platforms. These include legacy platforms or platforms where the driver are known to have bugs and will not receive any updates anymore.
- If all goes well December/January will be on performance.


## 2024-10-17

Mostly focused on platform support and fixing threading issues.

- Blender 4.3 will not be supported on Windows/Intel iGPUs 10th gen or earlier due to a driver issue when using dynamic rendering. It seems like the driver expect to be in a state that cannot be met and crashes. When starting a new rendering scope it validates the last bound graphics pipeline to be compatible, which can not be the case. Vulkan specs mentions this check to be valid when executing a draw command, which later one and at that time the condition is met.
- For Blender 4.4 a workaround will be added which can also be used by "other" legacy platforms. This will increase the platform compatibility beyond what we now support for OpenGL.
- There was a threading issue that crashed Cycles rendering in the viewport. When looking into the issue several other issues were detected and fixed. These fixes include that some buffers where allocated in memory space that couldn't support features that were requested, leading to crashes.

**Short term goals**

- Update ShaderC library for Blender 4.4
- Add legacy platform support

## 2024-10-04

Mostly focussed on some performance improvements when using production grease pencil files. And stabilizing the vulkan backend for its initial release.

- [blender/blender#127860]: GPU that blender should use for drawing can be selected. The auto detection should work as expected but there are some configurations like (multi-GPU workstations/server setups) where the auto detection doesn't allow you to select a specific GPU without tweaking driver/OS specific settings. This PR simplifies this by letting the user select the GPU to use.
- [blender/blender#128068]: Performance of descriptor sets. Grease pencil drawing will create a lot of similar descriptor sets, which could be a performance bottleneck on selected platforms (Intel Arc). We removed code indirection in the way how the state manager, shader interface and descriptor sets worked together, allowing better detection if the descriptor set was actually different compared to the previous one.
- [blender/blender#128232]: Blender reported the internal driver version of the vulkan backend. This adds confusion when triaging as this driver is often not advertised to the user. By reporting on the marketed version of a driver this should easy the process.
- [blender/blender#128188]: Immediate mode used tracked resources. With the new swap chain tracked resources this isn't needed and tracked resources should be phased out. This change refactors immediate mode to use swap chain tracked resources

**Community developments**

Several community developments are starting to take off. These includes active developments and people wanting to work on Blender/Vulkan.

- Support is being added for `VK_KHR_dynamic_rendering_local_read`. This is a fairly new extension, but would benefit EEVEE shadow rendering and deferred rendering. Their approach is in line with my expectation and hopefully we will seen some code soon.
- [blender/blender#127995] Support has been added for `VK_KHR_fragment_shader_barycentric`. This is mainly used in the wireframe shader node.
- In contact with a student who can participate in open source projects as part of his education for 6-8 months.

**Short term goals**

- Stabilizing the backend for its initial release. Some validation warnings have appeared that needs fixing.
- Add support for render passes to check if that increases the support for older platforms (including faulty drivers).
- Improve performance by tweaking the command reordering.
- Blender conference presentation

## 2024-09-19

[blender/blender#127418] Did an experiment to see how we could reduce the startup times when using Vulkan. Most time was spent on shader compilation. For OpenGL we introduced a parallel compilation feature. When implementing this in Vulkan we saw several issues in the SPIR-V compiler (especially the optimizer) that did blocked most parallel execution. However where it was blocking didn't need to be blocked at all. This was reported and fixed up-stream. [blender/blender#127564] is created to backport the change to our shaderc library. More changes to shaderc has been done to support our case better. But would want to wait until the next release of shaderc. These changes will reduce compilation times as well, but are less important than the global block.

[blender/blender#127698] Contains the parallel compilation part of the experiment including comparisons with OpenGL and cold/warm starts.

Still the startup time was still to large (8 seconds) where most time was still spent on front-end compiling step of shaders. Shaders that could already be cached in the pipeline and can skip the front-end compilation step at all. For vulkan has an extension for it `VK_EXT_shader_module_identifier`. However this is quite complex to introduce in the current setup and it still needed a cache to store the identifiers. For now we added a disk cache for SPIR-V binaries. These binaries can work on any platform, but still needs to ibe compiled to specific GPUs. The SPIR-V disk cache still needs some development as validating correctness of a SPIR-V binary is hard to do without compiling, adding a sidecar in the cache would make this step easier. The sidecar can also be used to store the identifiers.

On AMD these improvements resulted in starting Blender in 1.05 seconds with a warm cache. Starting Blender, loading barbershop_interior waiting for the viewport to settle takes 3 seconds. NVIDIA shows similar results.

This work is currently spliced into multiple PRs and will be reviewed one-by-one. The SPIR-V disk cache still needs some work as validating SPIR-V is hard to do, adding a sidecar in the cache would make it easy. The sidecar might also be useful when adding support for `VK_EXT_shader_module_identifier`.

**Next steps**

- Land PRs for improving shader compilation times and Blender startup times.
- Fix render graph threading issue when tweaking materials.
- Improve support for Intel GPUs (Windows/Iris+ crashes on startup).


## 2024-08-16

During my holiday I got some request from a developer working for Intel who wants to contribute to the Vulkan project as a volunteer developer. I prepared some tasks that he could be working on.
- [blender/blender#126228]: Add support for `VK_KHR_fragment_shader_barycentric`.
- [blender/blender#126229]: Separate static and dynamic pipeline caches and make the static pipeline cache persistent
- [blender/blender#126230]: Skip frontend and backend compilation when shader module is already cached.

An ARM based company is willing to help out as well. Which and how still needs to be discussed. It makes sense for them to add features to the Vulkan backend related to tiled based GPU architectures. The GPU API has already an API which is utilized by EEVEE to optimize Metal running on tiled based GPU architectures. In the Vulkan backend we don't have an optimized code-path for this architecture. Also not sure about how it would match the dynamic rendering approach that we are currently following.

Implemented a prototype ([blender/blender#126353]) for better resource discarding/destruction to find out the important aspects that the final solution needs to take into consideration. The prototype is able to playback animation without crashing. Wrote up the [design](https://projects.blender.org/Jeroen-Bakker/documentation/src/branch/main/src/gpu-vulkan/vulkan-resources.md). 

**Next steps**

At this moment I am prioritizing stabilization before performance. When the backend is stable we can add the user preference setting to activate Vulkan as an experimental option. 

- Finalize & validate design for resource discarding/destruction. 
- Implement the design.

There are some platforms (mostly legacy) that currently not able to run with Vulkan due to the extensions we require. By having the option in the user preferences might allow more users to test the backend and report issues.

At that moment I do accept any report in the Vulkan backend, but as it is still experimental their priority will be normal. Development on performance can then continue.



## 2024-07-17

Had some back and forth with Mesa developers to fix issues occurring both Blender and Mesa. These issues were not related to Vulkan but caused that I couldn't do that much as expected. The time I had I worked on fixing the synchronization issues by implementing a sub resource tracking for arrayed textures. We chose to implement it as part of the command builder as that would reduce the amount of resources that needed to be tracked globally. The solution was straight forward, but needed the render graph to know which layers are bound to a shader and what layouts was expected. [blender/blender#124407]

I didn't had time to determine the impact for the support for `VK_EXT_descriptor_buffer`.

**Smaller improvements & fixes**

 - Added support for particle hair ([blender/blender#124854])
  
Continued stabilizing the Vulkan backend by opening large scenes. I was able to open scenes like junkshop, bistro, monster & victor and used EEVEE without raising any validation or synchronization error.


**Next steps**

Only showing the top 3 in order of importance.

 - [blender/blender#124863] Resources are discarded globally, but should consider the swap chain. This isn't the case and resources can be destroyed when still in flight.
 - [blender/blender#124864] Crash due to invalid render graph state during playing back.
 - [blender/blender#124215] Add support for `VK_EXT_descriptor_buffer`


## 2024-07-05

Last two weeks, the feature parity and stability of the Vulkan backend improved a lot. We are now able to remove 'all' side wheels and check the performance bottleneck shifts. The bottleneck shifted to `Descriptor sets` [[explanation](https://vkguide.dev/docs/chapter-4/descriptors/)].

Although the descriptor set is the only solution available on all platforms, it is known to be a faulty design and many extensions have been created to fix this. Sadly there is no extension that support all our devices. To work around this we will select the best extension for Blender and use it when the system supports it. Most likely the chosen extension will be `VK_EXT_descriptor_buffer` [[vulkan proposal](https://docs.vulkan.org/features/latest/features/proposals/VK_EXT_descriptor_buffer.html)]

EEVEE runs very well on [Vulkan](https://mstdn.social/@atmind/112718658077021096). There are some synchronization issues left to handle. However in the original design of the render graph we decided to not support state tracking of sub-resources, but EEVEE currently requires it. This can be implemented in two places: inside the resource tracker as a global solution, or as part of the command builder where a rendering scope can track this.

**Next steps (Short term)**

- [blender/blender#124214] Vulkan sub-resource state tracking
- [blender/blender#124215] Add support for `VK_EXT_descriptor_buffer`

**Impression**

It is not clear if Vulkan will be available in 4.3. Mainly reason is that we are heading into the summer holiday season and it is unclear which issues will be postponed for me to look at when I get back.

