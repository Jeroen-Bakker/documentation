---
marp: true
theme: uncover
class: invert
size: 16:9
paginate: true
headingDivider: 1
---

# Vulkan 2024Q2
Canteen session

# Why Vulkan?

- OpenGL is deprecated
  - Support levels dropping
  - New features not available
- Check bconf23 presentation

# Current state

October 2023
- Most of the UI is working, except:
  - Cycles display driver
  - EEVEE-Next (used to work, but didn't catch up on the new features)
  - OpenXR
  - OpenSubdiv
- Focussed on stability, not performance.

# Goal for 2024Q2

* Get the performance (at least) on par with OpenGL across all supporting platforms.
* Implement missing features to support EEVEE-Next.

# __Sync__ memory model with __Async__ execution model

```cpp
void my_draw(GPUTexture *other_tx) {
  GPUTexture *texture = GPU_texture_create_2d(...);
  GPU_texture_copy(other_tx, texture);
  GPU_texture_free(texture);
}
```

- On GPU `texture` is allocated
- Copy is recorded
- Freeing of texture is postponed

---
![height:640](images/programming-model.svg)

# Command execution

* Commands start in order, but can end out of order


# Layout transitions

![height:512px](images/texture-layouts.gif)

# Resource versioning

# Pipelines

# Multi threading



