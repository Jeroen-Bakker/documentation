# Vulkan Samples

Vulkan samples has some requests for examples that align with developments of the upcoming year for
Blender.

- OpenXR
- Font rendering.
- Interop.

Font rendering is already working and can be an interesting example to understand the vulkan samples
workflow.