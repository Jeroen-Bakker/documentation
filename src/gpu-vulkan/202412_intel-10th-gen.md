# Intel iGPU 10th Gen and earlier support

Past year we have been looking at adding support to Intel iGPU 10th gen and previous generations that use proprietary Windows driver. However after many efforts and engineering hours we come to the conclusion that the Vulkan part of the driver isn't able to work with Blender. As the driver is out of support it might not receive any updates anymore to fix the issues we detected.

Below we list our observations and how we come to our conclusion to not support the effected platforms. We also mention when the support will end and how users can mitigate the issues.

## Dynamic rendering

The driver promotes that it supports VK_EXT_dynamic_rendering. However when enabled Blender would crash when calling in certain condition `vkCmdBeginRendering`. Intel driver team reported back that the driver was crashing as the current bound pipeline wasn't compatible with the rendering info. When reading the Vulkan specs this should be validated when performing any `vkCmdDraw*` command later on. Due to the dynamic nature of Blender it is hard for us move pipeline binding before starting rendering. There are also cases where you only want to clear attachments in that case there are no pipelines involved.

According to the driver it has been validated against conformance version 1.3.1.1, which doesn't include any dynamic rendering conformance testing. 

Due to this issue we made dynamic rendering an optional extension inside Blender. The workaround when the extension is not available uses `VkRenderPass` and `VkFramebuffer`. This allowed Blender to start and use anything except EEVEE.

## Graphics-Pipeline

EEVEE has complex features like shadows & deferred BSDF rendering. However the driver isn't able to create the graphic pipelines of some features. In this case `vkCreateGraphicsPipelines` returns `VK_SUCCESS` together with a `VK_NULL_HANDLE` as graphics pipeline. In Blender graphic pipelines are created based on the drawing state and therefore placed very close to actually adding commands to the command buffer. At this stage it is very hard to recover to an error like this.

There could be more EEVEE features that are not able to work, the mentioned features are the ones we found when disabling not working parts of EEVEE, until the EEVEE render would just not be usable for users.

## Support

Due to these reasons we have added Windows proprietary driver that hasn't been validated against conformance test 1.3.2.0 to our block list. Users of those systems will automatically switch back to OpenGL that works as expected.

A consequence is that we are not able to support this platform when OpenGL will be removed in the future. The precise Blender version hasn't been picked, but most likely will be removed in 2025.

At that moment users will not be able to run Blender on Intel iGPU 10th gen or earlier on Windows. Blender will work fine on Linux. Using Windows with a dedicated GPU would also work.

## Removing the block list

For testing purposes we include the diff that can be applied to Blender main branch. When the diff is applied Blender can be started using Vulkan. However using material preview of render preview will crash due to the failing pipeline creations.


```diff
diff --git a/lib/windows_x64 b/lib/windows_x64
index 283d5458d37..e32978519cb 160000
--- a/lib/windows_x64
+++ b/lib/windows_x64
@@ -1 +1 @@
-Subproject commit 283d5458d373814824e5675d6fcd7a67d7c50cfb
+Subproject commit e32978519cb6f04496fb49f615e504bcbd540a2e
diff --git a/source/blender/gpu/vulkan/vk_backend.cc b/source/blender/gpu/vulkan/vk_backend.cc
index 61dd23bb6b0..e51a1289909 100644
--- a/source/blender/gpu/vulkan/vk_backend.cc
+++ b/source/blender/gpu/vulkan/vk_backend.cc
@@ -69,6 +69,7 @@ bool GPU_vulkan_is_supported_driver(VkPhysicalDevice vk_physical_device)
    * when actually calling a vkCmdDraw* command. According to the Vulkan specs the requirements
    * should only be met when calling a vkCmdDraw* command.
    */
+  #if 0
   if (vk_physical_device_driver_properties.driverID == VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS &&
       vk_physical_device_properties.properties.deviceType ==
           VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU &&
@@ -76,6 +77,7 @@ bool GPU_vulkan_is_supported_driver(VkPhysicalDevice vk_physical_device)
   {
     return false;
   }
+  #endif
 
   /* NVIDIA drivers below 550 don't work. When sending command to the GPU there is no reply back
    * when they are finished. Driver 550 should support GTX 700 and above GPUs.
@@ -383,6 +385,8 @@ void VKBackend::detect_workarounds(VKDevice &device)
   workarounds.dynamic_rendering_unused_attachments = !device.supports_extension(
       VK_EXT_DYNAMIC_RENDERING_UNUSED_ATTACHMENTS_EXTENSION_NAME);

+  workarounds.dynamic_rendering_unused_attachments = workarounds.dynamic_rendering = true;
+
   device.workarounds_ = workarounds;
 }


```