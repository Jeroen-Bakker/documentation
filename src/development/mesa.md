# Build Mesa

Building mesa locally can help understanding crashes, but also
some tools are disabled in the default mesa installed in ubuntu.

## Ubuntu 24.04

### Checking out the mesa repository

```
cd ~
mkdir freedesktop-git
cd freedesktop-git
git clone https://gitlab.freedesktop.org/mesa/mesa.git
```

### Ensure build dependencies
- Make sure that the source code repository has been enabled in `Software & updates`.

```bash
apt-get build-dep mesa
```

### Setup build folder

```
meson build64 --libdir lib64 --prefix $HOME/mesa -Dgallium-drivers=radeonsi,swrast,iris,zink -Dvulkan-drivers=intel,amd -Dgallium-nine=true -Dvideo-codecs=h264dec,h264enc,h265dec,h265enc,vc1dec -Dbuildtype=release
```

### Build

```
ninja -C build64 install
```

The build binaries will be located at `~/mesa` The mesa loaded can be configured to start an application using the libraries in this location.

### Run

#### Preparation

Create a script and store at `~/bin/mesa-run.sh`.
```bash
#!/bin/sh

MESA=$HOME/mesa \
LD_LIBRARY_PATH=$MESA/lib64:$MESA/lib:$LD_LIBRARY_PATH \
LIBGL_DRIVERS_PATH=$MESA/lib64/dri:$MESA/lib/dri \
VK_ICD_FILENAMES=$MESA/share/vulkan/icd.d/radeon_icd.x86_64.json:$MESA/share/vulkan/icd.d/radeon_icd.x86.json \
LIBVA_DRIVERS_PATH=$MESA/lib64/dri:$MESA/lib/dri \
VDPAU_DRIVER_PATH=$MESA/lib64/vdpau \
D3D_MODULE_PATH=$MESA/lib64/d3d/d3dadapter9.so.1:$MESA/lib/d3d/d3dadapter9.so.1 \
    exec "$@"
```

Make sure this script is executable

```bash
chmod +x ~/bin/mesa-run.sh
```

#### Running blender

```bash
~/bin/mesa-run.sh ~/blender-git/build_linux/bin/blender
```

This can be validated as the system information will report something like:

```text
Operating system: Linux-6.9.3-060903-generic-x86_64-with-glibc2.39 64 Bits, WAYLAND UI
Graphics card: AMD Radeon Graphics (radeonsi, raphael_mendocino, LLVM 17.0.6, DRM 3.57, 6.9.3-060903-generic) AMD 4.6 (Core Profile) Mesa 24.2.0-devel (git-c84d1f5571)
```