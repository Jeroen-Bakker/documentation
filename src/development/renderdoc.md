# Renderdoc

## Wayland

Renderdoc doesn't support wayland + Vulkan yet. This is still being developed and tested. The workaround for it is to force QT to use X11.

```
WAYLAND_DISPLAY= QT_QPA_PLATFORM=xcb bin/qrenderdoc
```