# Development Setup

## VSCode

Extensions:

* Microsoft
    * `C/C++`
    * `Life Share`
* Xaver Hellauer
    * `Clang-Format`
* Vadim Chiginov
    * `CodeLLDB`
* zougun
    * `Explicit Folding`
* jdinhlife
    * `GruvBox Theme`
* vscodevim
    * `VIM`
* GitKraken
    * `GitLens`
* The Rust Programming Language
    * `rust-analyzer`

Settings:

```

{
    "workbench.colorTheme": "Xcode Classic (Light)",
    "clang-format.executable": "/home/jeroen/blender-git/lib/linux_x86_64_glibc_228/llvm/bin/clang-format",
    "editor.formatOnSave": true,
    "editor.defaultFormatter": "xaver.clang-format",
    "files.autoSave": "afterDelay",
    "window.zoomLevel": 2,
    "C_Cpp.default.compilerPath": "/usr/bin/g++"
}
```

In VIM remove keybinding ctrl-P