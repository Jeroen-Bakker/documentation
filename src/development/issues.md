# Issues in Documentation

Some issues found in documentation. They needs to be fixed or require update in
scripts.

## lib2to3 required to run make format

The command `make format` uses autopep8 and that requires the python3 installation
to have lib2to3 installed. However when installing Ubuntu with minimum requirements
this library isn't installed.

```sh
sudo apt install python3-lib2to3
```

This should be added to install linux deps.
