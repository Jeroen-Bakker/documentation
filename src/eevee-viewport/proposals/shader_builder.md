# Shader Builder

Shader builder is a tool that the developers of the EEVEE/Viewport module use extensively.

Blender uses at the moment of writing around 800 static shaders. Parts of the GLSL code
is shared using libraries. When making changes to GLSL we want to ensure that the GLSL
is still valid on other platforms as well. GLSL are normally only compiled when running
Blender as it requires in OpenGL it is a driver responsibility.

We introduced Shader Builder to quickly asses that the static shaders compile as expected
during development at compile time. This already is a time saver.

## Limitations

Shader builder uses `gpu` and `ghost` libraries. These include other libraries and so on
which we don't need. To save linking time we added some stubs to functions that we isn't
used. The issue with this approach is that other developments may introduce breaking changes
when changing functions or libraries.

Currently shader builder tries to validate all possible platforms that have been enabled
in the build. This is fine during development, but due to limitations of OpenGL cannot be
enabled on the build bot as OpenGL might require an attached monitor and OpenGL drivers
installed. For Metal and Vulkan this isn't the case.

When adding new features to a GPU backend shader builder is a nice tool to use during
debugging. But as it always compile all shaders developers are adding temporarily code
to break just before the failing shader.

Some GPU features we use are not available on all platforms. Those features can generate
different shaders. Shader generation is driven by workarounds. Shader builder only validates
shader generation for the current platform. We could increase the coverage by also validating
the shaders when all workarounds are activated. Similar to Blender parameter 
`--debug-gpu-force-workarounds`.

## Propsal

* Fix the current issue, not sure why cycles is included in the dependecy chain. On Apple it requires
  python libraries.
* Separate libraries so that we don't need to rely on stubs. We can start with introducing
  a `bf_gpu_shader` that include all the necesairy bits of pieces required for shader validation
  of the gpu module. For ghost this might mean that we need to split it as well, or add a
  second library that can do all the headless initialization we need.
* Make a single run of shader builder validate a single platform. This way we can enable shader
  validation on the build bot for Metal/Vulkan. We should discuss if we want to enable shader
  builder by default as most developers don't touch glsl code and therefore only when pulling
  new code might trigger a rerun. (each run takes approx 10 seconds)
* Add option to validate static shaders that start with a certain string, eg 'eevee' would
  validate all eevee static shaders.
* Add option to validate with all workarounds enabled.