# Viewport HWRT

**STATE**: Draft

This design gives an outline of using HWRT inside the viewport.

## Benefits

**Workbench**

- Better quality workbench AO (Low complexity)
- Possible faster workbench shadows when working with huge geometries (Low complexity)

**Overlay**

- Show shadows of the active lamp.

**EEVEE**

- AO Node support (Low complexity)
- Reflection (High complexity)
- Refraction (High complexity)
- Need to research whether raytracing would benefit here: Fast GI approx?
- Scene based GI (Medium complexity)

## Reflection/Refraction

A challenge when adding a reflection/refraction is that there can only be a single hit shader. We are able to store the hit result (mesh_id, face_id/material_id, barycentric coordinates) inside a buffer and evaluate material afterwards using screen space passes.
Game engines often use a single ubershader with some scaled done textures to approximate the actual materials.

### Option: material slot selection

Option: Store a hash for up to 30 material slots selected by the user (perhaps per object) determine the area of the screen where the reflection require these material slots and perfor a screen space evaluation of the material slot.
3 materials slots as we might need to store it as a mask where 1 bit is reserved for an uber shader. 
The bitmask is used to map multiple materials into a lower resolution to reduce the amount of pixels to evaluate. Only the part where the material is used will be evaluated. The bitmask ensures that on a lower resolution the mask can be combined to store multiple materials per pixel.
Game engines typically use an ubershader with a texture array for the input textures, these textures all use the same resolution or use texture streaming techniques. In the short term we could create a texture array for all textures loaded in Blender scaled down to one resolution. The nodetree will be simplified to a single ubershader node.

### Option: ubershader

Simplified solution where only the ubershader part is used. Has lower quality as detail inside the shader are ignored.

## GPU API

- GPUAccelerationStructure: Top level acceleration structured stored inside an engine. Contains instances and orientation of GPUAccelerationStructureSurface. Can contain static and dynamic instances to improve build speeds when geometry changes.
- GPUAccelerationStructureSurface: Bottom level acceleration structure stored per 'mesh'
- We might only need ray queries. Ray queries are limited compared to RayTracingPipelines but might be enough and far easier to implement.

### Vulkan backend

**Extensions**

Although raytracing is always optional, there are minimum extensions and some optional extensions to improve the feature.
- VK_EXT_accelleration_structure (minimum requirement)
- VK_EXT_ray_query (minimum requirement)
- VK_KHR_deferred_host_operations (optional)