# 2025-03-03 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **10 March 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-03-10 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-03-10 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- Jeroen
- Miguel

## DST Global lock

Many merges happened to remove DST global locking. API cleanups needs to land next. It is still being considered to have a DST per viewport so multiple viewport can draw at the same time (eventually), but this requires more discussion as sharing GPU resources would be harder to do.
Per "engine" shader modules are now wrapped so shading compilation can be thread safe.Next limitation is that shaders can only be used by a thread at the same time. This is because the shader also carries state (like uniforms/push constants/specialization constants) and can be modified before or after binding.
- New proposal will be made to tackle the shader API. The main idea is to separate the state from the shader and when using a shader the calling code isn't modifying the shader. Only when used (or first used) it will. A proposal will be created how this will look like on data structures and usage.

## Metal

[https://projects.blender.org/blender/blender/pulls/135099] We have a fix for the Black cube on Apple/Intel/AMD. We still need feedback on testing or other hardware configurations. 

## OpenSubdiv

[https://projects.blender.org/blender/blender/issues/133716] All opensubdiv based shaders are able to compile on OpenGL, Metal & Vulkan. For now, the shaders can only be used by OpenGL due to direct GL-calls around the shaders. In the upcoming weeks(s) multiple cleanups are planned to minimize the last step of enabling opensubdiv for Metal/Vulkan.
- The complexity originates from the era where CPP couldn't be used directly in Blender code-base. Most interface code is wrapped the buffers using GLINT handles. A callback API uses these handles to be wrapped in GPU vertex buffers. Even when the buffers are never used as vertex buffers. Current solution would be to use GPU objects and keep storage buffers/vertex buffers in their expected object types.
- Noteworthy is that Blender already uses the opensubdiv API that can support Metal and Vulkan as well. This reduces the impact of the project as no new API needs to be learned/integrated.


# 2025-02-24 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **3 March 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-03-03 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-03-03 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## GPU Subdivision

- [https://projects.blender.org/blender/blender/issues/133716] Shaders are being ported to shader create info. Shaders are validated to compile in OpenGL, Vulkan & Metal and tested using render tests. The patch evaluation shader includes external GLSL from OpenSubDiv, that requires changes to shader compilation. The idea is to replace it with an include statement and programmatically include the correct code into the shader.


# 2025-02-17 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **24 February 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-02-24 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-02-24 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- Jeroen
- Miguel

## Blender 4.4

- We reached 0 high severity reports. During the weekend 4 new were reported. They seems to be easy to fix, or priority can be lowered as the issue is out of our control.

## Blender 4.5

- **Global lock**: The issue is easier than expected when using thread local storage. Design task how this will look like is in the making.
- [https://projects.blender.org/blender/blender/issues/133716]: Migrating the shaders can utilize the GPU_SHADER_CREATE_INFO define to migrate one shader at a time. We will migrate the first shader and do a review on the process. After that a bunch of shaders can be migrated by multiple developers. During development we want to include render tests. These will be created by looking at the code of each shader. These tests are essential when implementing the Metal/Vulkan part of the API.
- **Render tests**: During the meeting we discussed about having extracting test behaviors from the render test. Currently we have a python script that contains lists that generate light probes or optimize some settings. The idea that was brought up during the meeting was to use Scene/View layer properties to keep the script more manageable. 
- [https://projects.blender.org/blender/blender/pulls/133879] Overlay tests are in review.
- [https://projects.blender.org/blender/blender/pulls/133557] Feedback on the profiling uses perfetto.

## NPR

- Agreement has been made about handling discussions around the NPR topic. The needs from the NPR and module are more aligned. Follow-up meeting will be planned this week to continue design discussions.


# 2025-02-03 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **17 February 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-02-17 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-02-17 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## Module

- Went over the planning of this year. The planning was updated on https://projects.blender.org/blender/blender/wiki/Module:%20Viewport%20&%20EEVEE#projects

## Overlay

- [https://projects.blender.org/blender/blender-test-data/pulls/52] Added render tests for Overlay engine. The initial tests is generated from possible UI options using a python script. There around 2000 permutations. We still need to decide if we want to store the generated blend files on the tests/data folder or not. The decision will be made during the review as we would like to have some inputs from other as well.

## Metal

- [https://projects.blender.org/blender/blender/pulls/133830] The workaround for atomic textures could make out of bound writes. Fixing this apparently also fixed the hang on the build bot. The code will be reviewed t check all cases are covered.
- [https://projects.blender.org/blender/blender/pulls/133818] For the future we should add more Metal workarounds to `--debug-gpu-force-workarounds` as that made it easier to track this issue. Some workarounds are stored distributed in the code and requires some reorganization to make this happen.
- Workaround for raster order group does restart the existing rendering scope and keep the state of the initial rendering scope. When starting the second rendering scope it performs load operations and can clear the results of the first rendering scope, which is not correct. This might be related to support the legacy platforms (Black cube). There seems like an undefined behavior here. This needs more investigation but is considered an high prio issue.
- It is being discussed to support Apple Intel until 4.5 LTS. For 5.0 and onwards it can work, but it might break due to new developments. If this happens we should rely on community support. Main reason is that fixing driver issues on these platforms do not meet the quality/expectation Blender needs. Currently development time is burnt to discuss, investigate and fix these issues, what won't allow us to focus on adding value for other users.

> [!NOTE]
> Next week there will be no meeting. Blender 4.4 Beta will start and we will focus on fixing high prio bugs and detailing designs.

# 2025-01-27 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **3 February 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-02-03 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-02-03 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- Jeroen
- Miguel

## Module year planning

Designs have been written for upcoming module projects. They will be discussed among the module members next week. Actual designs will be published afterwards.

## GPU module 

[https://projects.blender.org/blender/blender/pulls/133557] is in review. This PR allows exporting profile data in an standard trace event file format so it can be loaded in profile tools.

## Vulkan

- [https://projects.blender.org/blender/blender/pulls/132681] New threading model has landed. Command buffer building are now done on a single thread to free up the application thread for other work. Design can be found at [https://developer.blender.org/docs/features/gpu/vulkan/render_graph/]. This also replaced the swapchain based resource management with a timeline based resource management.

**Smaller fixes**

- [blender/blender!133535] The threaded shader compiler use a BKE/GHOST function that was not thread safe, leading to reading and writing spirv files in different places. This has been solved by reading the cache folder when Blender starts and use this folder when accessing spirv files. 
- [blender/blender!133485] Using Cycles had a big performance regression. The reason was that Cycles uploaded the result to the GPU, but the Vulkan backend used a CPU roundtrip when updating the viewport. Resulting in a huge performance degradation. 
- [blender/blender!133528] Using scenes on non-rebar capable systems could lead to crashes as device only buffers where allocated with mapping capability. This combination is very limited on those systems and would crash when this memory area didn't had any space left for allocation. This was solved by giving the calling code more control over the type of memory allocation it should do. This was already the plan, but now it was required to fix an actual issue.

# 2025-01-20 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **27 January 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-01-27 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-01-27 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## Fixing bugs

Last month a large set of selection, overlay bugs were solved. There are still a few left that needs some input from Clement to see what the best way is to solve them.

There was also a discussion to guard against misuse of the draw manager API. sometimes extraction or drawing fails because the input mesh isn't consistent. Guarding against this will show the issue during development, results in better triaging and a clearer understanding what is supported and not. This requires more time to flesh out how this could be added without less runtime overhead. Asserts is one thing that comes to mind.


## Platform support

There are two known compiler issues with the new shader/CPP structure. One for Apple/AMD RX5500 [https://projects.blender.org/blender/blender/issues/132663] and the other for AMD Radeon RX530M [https://projects.blender.org/blender/blender/issues/132968], which doesn't seem to be a polaris architecture. We should investiate and check how we will deal with these platform issues.


## Metal

- Regression test failing when running on buildbot. The cause is that buildbot works on MacOS 13 which doesn't support atomic textures. It falls back to device atomics. However fixing the issues now timeouts the command buffer. Command buffer timeouts happened in the past on the buildbot but couldn't reproduce on our development systems.
- [https://projects.blender.org/blender/blender/issues/133304] Metal backend doesn't have an option to force workarounds. We should start adding them as well.



# 2024-12-09 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **20 January 2025, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2025-01-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2025-01-20 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## Discussion topic: Removal of geometry shader support

Geometry shaders have been used in Blender to support wide line rendering and to implement fallbacks & driver workarounds. Metal devices don't support geometry shaders and used a workaround that only uses vertex shaders. In Blender 4.4 this has been rewritten into a common API (called primitive expansion).

During the meeting we discussed if and how we will be supporting geometry shaders in the future. Areas that can still use geometry shaders are add-ons and workarounds.

** Add-on **

We will perform some query in github to detect if geometry shaders are used. We doubt that there are add-ons/extensions actually using it. Based on this outcome we will remove geometry shaders from the Python API or add a deprecation warning that geometry shaders will be removed in the near future.

** Internal API **

We will remove geometry shaders from our internal API. Any user code in our code base will not be allowed anymore. Internally GPU backends can still use geometry shaders to work around missing GPU features for example barycentric coordinates.

## Code quality

- Image engine has been ported to the new draw manager.
- Old draw manager and related code has been removed. Some areas did still use the old draw manager unexpectedly and were refactored to use the new draw manager.
- [https://projects.blender.org/blender/blender/issues/131339] A task has been written with the current pain points in our shading code base. It contains some solutions and feedback is asked to see what direction to take.
    - [https://projects.blender.org/blender/blender/pulls/131511] Contains a prototype for part of the proposal.
- [https://projects.blender.org/blender/blender/pulls/131580] Fix a race condition, that occurs when running GPU tests.
    - A possible related issue is that in OpenGL the first sample could use the default material shader when using parallel compilation.
- Python rendering frame boundaries to track resources fix.
- Reduced the warnings and errors when compiling shaders in CPP (validation & IDE integration).
- Regressions and memory leaks.
- Update overlay-next to use new shader create info.

## Next meeting

The next meeting is planned on 20th of January due to the holiday seasons.


# 2024-11-25 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **2 December 2024, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-12-02 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-12-02 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## Overlay

Overlay-next is now default. Overlay (Legacy) can be enabled via experimental options for the time being. It is expected to be removed within a few weeks. Issues, missing bits and pieces are now being handled.

## C++ API

[https://projects.blender.org/blender/blender/issues/130805] We discussed texture usage flags for an proposal. It is currently undecided if it is better to have a high level usage flags, or low level. Modern GPUs don't care that much of texture usage, but older hardware might still rely on it.

We should add a note that we want eventually use texture usage flags to also determine thebest place to allocate the buffers. (Rebar vs GPU vs CPU allocation)


## Vulkan

[https://projects.blender.org/blender/blender/pulls/129062] For 4.4 a workaround for devices that don't support dynamic rendering was added. This would allow us to support more devices (mostly AMD/Intel legacy platforms). However we still having issues with legacy Intel Windows drivers. Workbench is working, but the driver isn't able to handle the complexity of some EEVEE shaders. When that happens `vkCreateGraphicsPipelines` command return `VK_SUCCESS`, but with a `VK_NULL_HANDLE`. This isn't expected and not something we can easily work around.

[https://projects.blender.org/blender/blender/pulls/127768] One reason why the Vulkan backend was hidden behind developers extra option was that selection was not reliable on NVIDIA/AMD official drivers. Blender 4.3 was released and some reports where coming in for other related selection issues. The root cause of these issues was found. The cause was that reading back the depth from a depth stencil buffer was not implemented. Adding support for these data types and small code cleanups also fixed the original selection bug.


# 2024-11-18 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **25 November 2024, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-11-25 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-11-25 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- Jeroen
- Miguel

## Vulkan

- [https://projects.blender.org/blender/blender/pulls/129062]: Implemented fallback for legacy platforms using render passes.
- [https://projects.blender.org/blender/blender/pulls/130328]: Fix grid overlay artifacts
- [https://projects.blender.org/blender/blender/pulls/130258]: Related to render pass rendering. Vulkan render passes requires no empty slots between attachments. However workbench can still create framebuffers with empty slots. For now implemented a workaround by not allowing object id passes in wireframe mode. Eventually this should be replaced by a better solution (shader variation).

## Overlay next

- Studio and developers are asked to use Overlay-next.
- Vulkan compatibility is not tested yet.
- Image background images are not working.

## Quality project

[https://projects.blender.org/blender/blender/issues/130390]

During the meeting we discussed quality related changes we want to make in the upcoming 2 months. Most important is to remove the old draw manager, but some other tasks still needs to be done before we can do that. This will empower us to improve the instance performance as currently we need both draw managers to draw a screen.

- Migrate Image engine to use new draw manager
- Enabling overlay next by default
- Remove old drawing manager
- Implement performance improvements for instances.

Second topic is testing
- Render tests: Should work on all platforms
- Add render tests specific for overlay and EEVEE
- Unit test should work on all platforms

We still want to do improvements to the Vulkan backend as moving 2 months away from its development would not work. This said we will do quality based tasks within the scope of the project.
- Platform support
- Documentation
- Unit tests to test behaviors of GPU module.


# 2024-11-11 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **18 November 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-11-18 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-11-18 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## Blender 4.3 release 

- EEVEE
 - [https://projects.blender.org/blender/blender/pulls/129813] VR not working
 - [https://projects.blender.org/blender/blender/pulls/127215] Volume probe cerates bad shading when resolution is divisable by 3
 - [https://projects.blender.org/blender/blender/pulls/128960] Scenes in a reflection probe does not include baked light probe lighting
 - [https://projects.blender.org/blender/blender/pulls/129857] Add buffer workaround for stencil classification
- Metal
 - Crash when connected to an external display
 - Memory leak when rendering as texture pools are not working correctly due to internal state tracking.
 - Parts of the Metal backend is in review. We are looking into workarounds that are both Vulkan and Metal. For each workaround it is checked if we really want to support the workaround. For example vertex format conversions and scissor clearing. OpenGL supports many data type conversions, that are not natively supported in other backends.
- [https://projects.blender.org/blender/blender/issues/129889] Volume probe baking is not deterministic. Currently still in investigation.


## NPR

- Continued merging features into the NPR prototype branch
- Planned features where possible will be implemented as node groups.
- Script node is being developed. UX still has issues


## Vulkan

The Vulkan backend behind "Developer extras" option in the preferences [blender/blender#129799]. This to communicate better to the user that it is still in experimental stage.
- [https://projects.blender.org/blender/blender/pulls/129863] Fix selection issues for NVIDIA/AMD official drivers
- [https://projects.blender.org/blender/blender/pulls/129775], [https://projects.blender.org/blender/blender/pulls/130016] Fix several issues for ARM based devices.
- [https://projects.blender.org/blender/blender/pulls/129957] Fix several test cases and enabled test cases that were supported
- [https://projects.blender.org/blender/blender/pulls/129708] Fix crash when drivers don't perform any limit checks.
- [https://projects.blender.org/blender/blender/pulls/129265] Fix render artifacts when using AMD official driver


# 2024-11-04 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **11 November 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-11-11 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-11-11 time=12:00:00 timezone="Europe/Berlin"])
e
*Attendees*

- Clement
- James
- Jeroen
- Miguel

## EEVEE

- Performance EEVEE on Metal are being worked on. Was an older PR, which was spliced into multiple smaller PRs. Still requires testing before it can land.

## Metal

- Memory leaks are being investigated. Current changes had some regressions as the user count of frame buffers were done differently.
- Metal CPP: There has been a discussion about using Metal CPP. Metal CPP is a wrapper around Metal API. It has benefits as more developers are familiar with CPP, but also some draw backs. Features are not directly included in the wrapper, there is no documentation and developers needs to extract that from the Metal API documentation. It would require a cleanup and rewrite of the Metal backend as it does handle construction/destruction differently. For now this would not become a project as it is a lot of work and we don't have room in the planning for this.
- Startup times: Discussion how to handle precompilation of PSOs. For precompilation configurations that Blender is using need to be extracted and these should drive which PSOs will be compiled at build time. An idea is that we log the needed configurations and export these to the shader create info. A build step could be added that create the PSOs for these cases.

## NPR

- All the main features are implemented, but are not yet merged.
- Unclear technical parts of the design are identified and will be discussed.


# 2024-10-13 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **13 October 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-10-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-10-20 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

## EEVEE

- [https://projects.blender.org/blender/blender/commit/bc3fdc329378128dc4253681f869e01d924da213] Volume visibility check doesn't include in the shadows during the bake.
- [https://projects.blender.org/blender/blender/pulls/128877] Fix crash when using devices with low 3d texture limits. Volume probes could request memory size that is not supported by the device leading to crashes. When a volume probe texture could not be allocated a lower size will be selected. Texture is also reshaped to improve occupancy and more sizes. 
- [https://projects.blender.org/blender/blender/pulls/128525] Fix crash when using displacement mapping and bump mapping node. In this case the vertex shader was looking for functions that are only available in fragment shaders.
- Render tests are all updated. Developers should use the render tests during development.

## Metal

- Fixing issues in the Metal backend.
- Some memory leaks were detected and are being fixed. 
- Exit Blender when still compiling shaders asserts.

## Vulkan

- [https://projects.blender.org/blender/blender/pulls/128761] Research threading issues when using the vulkan backend. The vulkan backend made an incorrect assumption about the relation ship between context and threads leading to several issues. 
- Researched support for Intel iGPU on Windows. The driver on this platform doesn't support dynamic rendering unused attachments which is required, but for debugging tools compatibility marked as optional. To fix this issue we should implement a fallback for dynamic rendering, which is not targeted for the initial release.

## GPU module

- GLSL preprocessor: Goal is to improve startup times when debugging. Mostly why are we doing so many string processing when starting upcoming

## NPR prototype

- Worked on the initial release of the NPR prototype. 
- Worked on getting zones support in material nodes.



# 2024-09-30 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **7 October 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-10-07 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-10-07 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

# Metal

- Preliminary research has been done to speed up the GLSL->MSL translation. The idea is to do this at compile time. During this task it became aparent that the GLSL source could be cleaned up.
- [https://projects.blender.org/blender/blender/pulls/125657]: Metal now performs shader compilation on multiple threads. Then number of threads used is determined by the OS

# GLSL IDE support

Making GLSL compilable in a regular CPP compiler to add support for IDE functionalities like looking up symbols, jump to definition etc. In order to use the system you need to add the shader create info name you are working on to the top of the GLSL file. This will be explained in a developer doc that should still be written. The goal is to make working with GLSL easier for people who are not familiar with the code-base by adding CPP stubs and use regular CPP syntaxes like '#include' statement instead of '#pragma BLENDER_INCLUDE'.

# Vulkan

Mostly focussed on some performance improvements when using production grease pencil files.

- [https://projects.blender.org/blender/blender/pulls/127860]: GPU that blender should use for drawing can be selected. The auto detection should work as expected but there are some configurations like (multi-GPU workstations/server setups) where the auto detection doesn't allow you to select a specific GPU without tweaking driver/OS specific settings. This PR simplifies this by letting the user select the GPU to use.
- [https://projects.blender.org/blender/blender/pulls/128068]: Performance of descriptor sets. Grease pencil drawing will create a lot of similar descriptor sets, which could be a performance bottleneck on selected platforms (Intel Arc). We removed code indirection in the way how the state manager, shader interface and descriptor sets worked together, allowing better detection if the descriptor set was actually different compared to the previous one. 
- [https://projects.blender.org/blender/blender/pulls/128232]: Blender reported the internal driver version of the vulkan backend. This adds confusion when triaging as this driver is often not advertised to the user. By reporting on the marketed version of a driver this should easy the process.
- [https://projects.blender.org/blender/blender/pulls/128188]: Immediate mode used tracked resources. With the new swap chain tracked resources this isn't needed and tracked resources should be phased out. This change refactors immediate mode to use swap chain tracked resources.

# EEVEE

With the approach of Blender 4.3 Beta the focus is more on solving issues. This will be the focus for the upcoming time.


# 2024-09-16 Viewport & EEVEE Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Viewport & EEVEE module development. Any contributor (developer, UI/UX designer, writer, …) working on Viewport & EEVEE in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **23 September 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-09-23 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-09-23 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

# Metal

Vertex fetch API doesn't support index ranges that are shifted by the base index. This result in UV drawing issues but also incompatibility in other areas. This will be fixed by the Overlay engine and the primitive expansion API it is using. For the short term we will not implement a fix for 4.3 as this as it is a regression that is already out there for several years. In 4.4 it will be fixed by enabling the overlay engine.

Experimental Metal version for open subdiv. The initial implementation showed an order of magnitude slower performance as each vertex is evaluated in its own GPU submission. Currently checking if we can push the vertex loop closer to the actual submission so more work can be shared. There are still options to move to a totally different implementation where we do a batch based approach. This needs more research as Blender does more than a regular subdivision and that might be limited on user side.


# Overlay

Main development of Overlay engine has been completed. The actual stabilization/performance optimization will be planned for Blender 4.4.

# NPR

Core has been merged into the development branch (not main) A blog post is in the making.
[https://projects.blender.org/blender/blender/pulls/127591] Development continued on touch store/image sockets. Architecture will first be reviewed intern in the Module before asking wider feedback.

# OpenGL

[https://projects.blender.org/blender/blender/commit/a602e5530afd76fc3ba2b2100109d5ecf6a67ea4] Avoid race conditions and handle pending async compilations when compiling synchronously.


# Vulkan

[blender/blender#127418] Did an experiment to see how we could reduce the startup times when using Vulkan. Most time was spent on shader compilation. For OpenGL we introduced a parallel compilation feature. When implementing this in Vulkan we detected issues in the SPIR-V compiler (especially the optimizer) that blocked parallel execution. However where it was blocking didn't need to be blocked at all. This was reported and has been fixed up-stream.

The startup time was still to large (5-8 seconds) where most time was spent on front-end compilation step of shaders. Shaders that could already be cached in the pipeline can skip the front-end compilation step at all. Vulkan has an extension for it `VK_EXT_shader_module_identifier`. However this is quite complex to introduce in the current setup and it still needed a cache to store the identifiers. For now we added a disk cache for SPIR-V binaries. These binaries can work on any platform, but still needs to be compiled to specific GPUs (backend compilation step). Until now we didn't detect any performance issue with the backend compilation step.


# 2024-09-09 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **16 September 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-09-16 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-09-16 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

# EEVEE

- Messages in the viewport are now translatable
- [https://projects.blender.org/blender/blender/pulls/125005blender/blender] Batch shader compilation for image rendering has landed

# Overlay/Selection

- [https://projects.blender.org/blender/blender/issues/102179] Tasks have been updated with some missing areas, which include paint overlays & uv overlay All other overlays are in review.

# Metal

- Apple is working on OpenSubDiv support. This is an proof of concept and its goal is to figure out what needs to be done. The solution will not land directly in Blender main as there are other projects and developments that needs to be aligned.

# NPR

- NPR pass has been rewritten from a screen space pass to a geometry pass
- NPR engine can be selected as a render engine on top of anotherengine.
- During the meeting there was a discussion about how geometry attributes are currently handled. This seems to be in line with what is expected. Geometry attributes are merged before drawing and access to the attributes are indexed using a name hash which is consistent.
- All changes are in a branch [https://projects.blender.org/blender/blender/pulls/127258]. It is still early in development as it focuses on the technical decisions that needs to be made. These technical decision first needs to be agreed upon, before we can start on implementing user focused features.

# Vulkan

Preparing vulkan to be part of Blender 4.3 release. The backend will still be experimental. See [release notes](https://developer.blender.org/docs/release_notes/4.3/eevee/#vulkan-backend-experimental) for more details.

In the preferences user can select it preferred backend. When blender starts it tries to detect if that backend can be used (GPU supports our minimum feature-set). If that fails Blender will fall-back to its default backend and show a dialog with the error. In the console detailed information is logged about what feature could not be found.

There seems to be an issue with Intel iGPU support on Intel driver 30/31 which is being investigated. These driver don't support `VK_EXT_dynamic_rendering_unused_attachments` which we use to emulate subpass transitions. We might need to look for a different solution here.


# 2024-09-02 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **9 September 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-09-09 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-09-09 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel


# EEVEE

Many issues have been fixed and backported to 4.2 release.

EEVEE has some issues with running on Legacy GPUs. It is hard to reproduce on developer systems due to not having the exact same GPU/driver combination.

# Overlay-next

Project is around 80-90% finished. The complex overlays are done. Plan is to merge soon. 
Some performance regressions have been detected during development related to using a few main passes. The performance regressions imvoves workbench as well,. Some refactoring should be done to be able to do better GPU scheduling.

We prioritize features before performance. The overlay engine targets 4.4 for its initial release so there is still time to improve the performance after implementing the features.

# Metal

There are several high priority bugs for Metal when running on AMD or Intel hardware. During the meeting we discussed how we could handle those. We will be looking into the download statistics in order to make decisions.

Open Subdivision support for Metal is being developed. The current Blender part of the implementation is tight to OpenGL and isn't updated with the way when we added cross compilation to Metal. Other changes are needed on Blender side to improve OpenSubDiv integration.

# Vulkan

Last few weeks many stabilization improvements have been made to the Vulkan backend and we need to decide if the Vulkan backend will become part of the 4.3 release (still as an experimental option).

- Release notes needs to be written to explain what is expected.
- Backend selection option will be hidden behind the developer extra option.
- When Vulkan cannot be initialized Blender should switch automatically to OpenGL and inform the user.






# 2024-07-08 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 15, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-07-15 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-07-15 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

**EEVEE**

* Many bugs are being fixed in many areas
* There are still discussions on making parallel compilation default. Concerns is that enabling it on low end hardware can be much slower (3x), the technical difficulty of the solution and that it is working on all platforms. Improvements are still being made. The root cause of the slow down isn't clear at the moment. Might be related to how OS's schedule between processes, or that some shaders block the UI.
* There is a regression where rendering using parallel compilation during final image rendering, blocks the UI and adds more slowdown than expected. We need to check if we are actually disable parallel compilation or that there is an bug causing this regression.
* We didn't receive any bugs on parallel compilation on Intel based GPUs. It could be that they don't enable the feature, or that there are not enough people testing it. We will check on the hardware we have available if there are bugs to be expected.
* Normal and depth pass are not accumulated correctly
* Metal/AMD has been triaged to two bad depth texture generation. Advice is asked to AMD how to proceed.

**Vulkan**

Last weeks, the feature parity and stability of the Vulkan backend improved a lot. We are able to remove 'all' training wheels and check how the performance bottleneck shifts. The bottleneck shifted to `Descriptor sets`.

Although the descriptor set is the only solution available on all platforms, it is known to be a not optimal design and many extensions have been created to fix this. Sadly there is no extension that support all our devices. To work around this we will select the best extension for Blender and use it when the system supports it. Most likely the chosen extension will be `VK_EXT_descriptor_buffer` [[vulkan proposal](https://docs.vulkan.org/features/latest/features/proposals/VK_EXT_descriptor_buffer.html)]

There are some synchronization issues left to handle. However in the original design of the render graph we decided to not support state tracking of sub-resources, but EEVEE currently requires it. This can be implemented in two places: inside the resource tracker as a global solution, or as part of the command builder where a rendering scope can track this.


# 2024-07-01 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 8, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-07-08 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-07-08 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

**EEVEE**

- Mostly bug-fixing and platform support.
- Worked around two NVIDIA driver bugs. Hopefully would fix the shadows/light issues we have in the test cases. This will be tested.

**Metal**

- Paralize compilation has been worked on
- Prioritize to Intel/AMD issues.
  - Intel issue has been narrowed down to ray generation. When looking via a debugger to the generated rays it seems to be not what is expected.
  - AMD issue has been narrowed down to a single function in the depth of field. In the debugger this function provides correct results when looking at the work group results, But when collecting the work thread results it only gives one good result per 32 threads, leading to black pixels in the other threads.

**Vulkan**

The Vulkan backend slowly progresses. It has already proven its benefit when debugging platform specific issues in EEVEE. Some issues detected via the validation layers have been discussed with Vulkan SDK to improve the error messages. For example to use existing debug information inside the shaders to improve the messages to reduce the time when hunting down what is actually going on.

Currently the Vulkan Backend can render Workbench/Grease pencil/EEVEE scenes upto medium complexity. For higher complexity there are still issues that make Blender crash. Cycles is also supported; Cuda/Optix is still untested and most likely doesn't work due to its different memory model. A short term solution is to download the result and pass it to Vulkan in the same way HIP/OneAPI does.

The Vulkan backend still crashes when playing back animation and doing anything in a background thread (material preview, final image rendering, baking). Performance depends on the scene. The backend still has large room for improving its performance.


# 2024-06-17 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 24, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-06-24 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-06-24 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

**EEVEE**

We mainly worked on the [EEVEE documentation](https://docs.blender.org/manual/en/4.2/render/eevee/index.html) and bug fixing.
During the meeting we discussed the open high priority issues and what we expect the issues are about.
Our main concern is the Apple Metal backend support. We will discuss this internally how to deal with them.
For the Metal/Intel iGPU we will add a work around, which would make the viewport working, but has many limitations which need to be documented.


# 2024-06-10 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 17, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-06-17 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-06-17 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

**EEVEE**

- Parallel compilation has been merged. There is still an issue with specialization constants. The current API is still Blocking here as these shaders are compiled differently. After adding the parallel shader compilation the bottleneck shifted from hader compilation to texture loading. With the previous implementation the textures of the shaders that were compiled where loaded, making it just a few textures per compiled shader. Now the textures are loaded all at once when a batch of shaders have finished compiling. This shows that bigger scenes still blocks by reading the textures from disk and upload these textures to the GPU. We will look for a solution for Blender 4.3.
- EEVEE-Legacy was removed and changes to the versioning code to switch to switch automatically to EEVEE.
- Manual is being worked on. It also includes a section how to migrate existing scenes where automatic conversion doesn't work.
- Some time has been spent on identifying issues in the Metal backend. They relate to Film Accumulation in the viewport, Some errors in GPU shaders and changes to load-store/stencil export that might not be fully supported by all platforms.

# 2024-06-03 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 10, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-06-10 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-06-10 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- James
- Jeroen
- Miguel

**EEVEE**

Improve the realtime GI with a second parameter to avoid energy loss from distance reflectors and to mix it with very small thickness to avoid light leaking. 

Parallel shader compilation is still in development. Due to the complexity this needs some more time before it can land in main. Reviewing the code is currently the bottleneck, but we can still land the patch in BCon3.

**Vulkan**

Continued working on graphics pipeline creation [https://projects.blender.org/blender/blender/pulls/121787]. We currently are able to start blender and draw the full UI. We pushed the current limits to find what parts of EEVEE isn't working. Basic BSDFs, film and raytracing is working. Shadows aren't working (expect the framebuffer transition), more complex materials aren't working, Volumes haven't been tested.

The performance is where I currently expect the patch to be (close to OpenGL, but not faster). The code isn't optimized so I expect that the on par performance is still realistic.

The render graph can still crash a lot as the garbage collection is done on a global thread. This needs to move to a per thread basis. This requires some more design. 

Did do some preliminary design work to improve the shader compilation. The idea is to split the frontend and backend compilation steps and depending on the shader batch we will compile the base pipeline library for vertex/fragment stage.


# 2024-05-27 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 3, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-06-03 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-06-03 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*

- Clement
- Jeroen
- Miguel

**EEVEE**

- Many bug fixing in the area of shadows and light; most noteworthy
  - [https://projects.blender.org/blender/blender/commit/f968c99850]: Punctual shadow maps were never tagged as updated and constantly refreshed
- Adding several options that users were missing in the property panels. (Ray count + step count)
- [https://projects.blender.org/blender/blender/pulls/122232]: First patch of several to support subprocess based parallel compilation on OpenGL is in review. This will eventually reduce the compilation times. Due to limitations in drivers and how caching is implemented this PR will start new blender processes that will compile the shader, making it possible that multiple shaders can be compiled simultaneously and still be cached in the driver for the right process. A side effect is that at this moment 100MB per process is needed. Users should change the number of processes to use. The shaders itself are stored in shared memory.

**Vulkan**

- [https://projects.blender.org/blender/blender/pulls/122044] Experiment with sharing shader modules between shaders to reduce compilation and pipeline creation times. Conclusion is that sharing shader modules will not be resulting in much speedup during compilation. Reason is that the source code is different due to the defines and tracking bytecode would require parsing the spirv code. Spirv bytecode is always different and cannot be used as comparison, without decompiling and removing the meta data. In the end frontend compilation takes 35 seconds for 1400 shaders which isn't bad. Not sharing shader modules will lead to less reuse of shader pipelines. Experiment can be reviewed when this becomes an actual user facing problem.
- [https://projects.blender.org/blender/blender/pulls/121787] Work continued on dynamic rendering. Added an create info struct for graphics pipelines. This struct is large and we should find a better way to update it so we don't redo many parts of the struct. The first goal however is to make it working. Current state is that a pipeline can be created. But it resource links needs to be pushed outwards to a begin/end rendering node. Now the resources are not adapted to how they are used inside a rendering scope.

**NPR**

- An meeting was done to review the design. Next step is to create a technical prototype to flesh out some details, before the design will be updated.


# 2024-05-13 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 20, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-05-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-05-20 time=12:00:00 timezone="Europe/Berlin"])

**EEVEE**

* EEVEE-Next is enabled for new scenes in main.
* Work continued
  * Shader compilation performance. Current patch halves the compilation times, but we are not there yet and other solutions will be looked at. Most user feedback is also on this topic.
  * Sun light extraction
* [blender/blender#119734] EEVEE-Next world volume are infinite like Cycles. EEVEE-Classic world volumes end at the clip_end of the camera/viewport. This can lead to confusion as it would render different then expected. We added an operator to convert world volumes to a mesh volume.
* Support Intel iGPU has been improved. We had to work around several driver bugs.

**Vulkan**

* [blender/blender#121073] This week add support for clear attachments to the vulkan backend. Clearing attachments (compared to clearing textures) require that the GPU is in rendering mode. Previous implementation used framebuffers and render passes. The new implementation makes use of dynamic rendering (VK_KHR_dynamic_rendering) As framebuffers/renderpasses are cumbersome to setup, and also require pipelines to be rebuild when configuration changes without providing any benefit. Render passes were introduced to support tiled based rendering, but has no benefits when used in immediate mode (non arm based GPU). Dynamic rendering is widely supported and also provides additional extensions to support tiled based rendering. VK_KHR_dynamic_rendering_local_read is an upcoming extensions that will provide similar support between tiled based and immediate based rendering. At this moment it is not widely supported, but something to keep track of.

**Metal**

* Some small patches are in fight (mostly tweaks for optimizations).
* Experiments have beed done about further tweaking the shadow rendering on Metal. 

# 2024-05-06 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 13, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-05-13 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-05-13 time=12:00:00 timezone="Europe/Berlin"])

## EEVEE

* PCF shadow offset is now done automatic. We might want to add a shadow terminator bias in the future. There are some regressions when using PCF shadows on lower resolutions. Cause might be that the filtering is done on world space. Will be investigated.
* Punctual shadow map at projection discontinuity has been worked on. The fix removes the need of the custom shadow frustum. This removes padding and can increase the shadow resolution and reduce light leaking.
* World sun light extraction has being worked on as recent changes to the spherical harmonics require it. There is a working PR, but it doesn't cast shadows as that requires more of the shadow pipeline on the GPU. We expect this should be enough for the initial release.
* Improvement on shader compilation times. Some experiments have been done. 
  * NVIDIA specific extension doesn't give good results as the compilation times are all over the space.
  * Another approach is to use separate process. the compilation is much faster, but when implemented in Blender it was not that fast. Perhaps due to the overhead of Blender.
  * Another extension blocked the main thread which isn't useful for us.
  * Finally we tested a new API for compiling shaders in batches. where shaders will be stored as a handle. Shaders are compiled in the main context. The API will progress when a selection of the shaders takes to long, this way the UI is still updated. There are still some technicalities to solve.



# 2024-04-22 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **April 29, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-04-29 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-04-29 time=12:00:00 timezone="Europe/Berlin"])

## EEVEE
* The buttons in the UI have been updated. They are now more in line with Cycles and would be easier to navigate.
* Added a clamping method for direct lighting
* Code cleaning of the ray tracing pipeline
* Shadow jittering patch is still in review. It is more complicated then expected
* Landed patch for flickering shadows. Was related to calculated bounds from the draw manager.
* Fixing high priority issues.

During the meeting we discussed the next steps for shader compilation performance. We want to make the GPU backend more in control of the compilation threads. Metal can request the OS about the number of async compilation thread that the system has. And OpenGL can use a queue, schedule compilation from the main thread, and post-pone requesting linking state/results when we actually are binding the shader.

## Vulkan

* [blender/blender#120427]: The core of the render graph landed in main. The render graph is used to track resource usages and can generate needed pipeline barriers. The render graph isn't connected to anything yet. First the vulkan backend needs to be ported before we will be able to use the system. Image and buffer resources have been ported and compute shaders are the next target.

## Metal

* Build bot issue is still in research. Several fixes were done, but none fixed the buildbot issue. We think it has to do with incomplete bindings.
* Sometimes rendering in background mode results in a fully transparent render. This is being researched. 


# 2024-04-15 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **April 22, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-04-22 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-04-22 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel
* Michael
* Thomas

# EEVEE-Next

* Finished Volumetric sampling stabilization. Some improvements will be checked to reduce the convergence speed.
* Thickness approximation patch, makes lighting and ray tracing evaluation more consistent. It now works now for all BSDF. Previously the approximation was extracted from the shadow map. Currently it uses the minimum value between the shadow and shader value for compatibility reasons. UI still needs some improvements
  * Pre-computation based on a geometry nodetree setup is possible. It is slow. Issue is how to distribute the nodetree without duplicating the nodetree in many demo files. Node tree will be given to the users for testing. And later we decide how to solve the distribution.
  * Default value is based on bounding box, but value is evaluated as a sphere. Default values might not work as expected for refraction shaders.
  * This is a big change and will introduce regressions, where we would need to help users to setup the scene appropriate.
* [https://projects.blender.org/blender/blender/pulls/120616] - Light probe pass.
* [https://projects.blender.org/blender/blender/pulls/120620] - Shadow color pass is being evaluated. User feedback is needed and will be asked via the devtalk thread.
* Shadows
  * Dithering soft shadows
  * Fixes have been done to remove shadow flickering issues on NVIDIA. 
  * Solution is discussed to improve shadow updates for large shadows, or shadows are close to the camera. Idea was to reduce the shadow ray length. The issue is that this is not deterministic and should have more thought.
* Worked on solving issues that are considered regressions.
* EEVEE startup times are becoming an issue and we had some discussion in the meeting how each backend would be solving that in an ideal situation. The basic mechanisms between Vulkan and Metal are similar. But as Vulkan backend is less mature it can rely more on industry practices. Metal and OpenGL are already operational has more information about the bottlenecks. The idea is to design a common approach for shader pre-compilation (Metal + Vulkan), doing PSO caching, or quick building based on commonly parameters. Separate frontend and backend compilation more clearly in the API. Making EEVEE responsible for scheduling shader compilation.

# Metal

* AMD/Intel GPU platform support. AMD is now in decent place, Intel still has a read-modify-write issue.
* [https://projects.blender.org/blender/blender/issues/120038] Buildbot crash. The stacktrace in the report is a red herring. There are commadn buffers that are scheduled, but never executed. This will lead to a stall later on. Apple is able to reproduce it and working on a way to solve it.

# Vulkan

* [https://projects.blender.org/blender/blender/pulls/118963] The core of the render graph implementation has been added for review. First pass of the review highlighted that the implementation is fine, but would need some clarification and better naming in certain areas.


# 2024-04-08 EEVEE/Viewport Module Meeting

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **April 15, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-04-15 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-04-15 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel


## EEVEE

* Shadow optimization. LOD system for directional clipmap. Currently not used, but can be implemented for volume rendering.
* Limited number of LOD update per tile map. Currently multiple LODs can be rendered, but require geometry per LOD. When playing back animation the limit is set to 1, when rendering no limits are set. Every interaction with light and scene should be faster.
* Volume mesh rendering pipeline. Simplified the code and fixed a lot of artifacts. Volume culling is now done in 2D space in view coordinates.
* Reduced volume leaking to surface, by offsetting the volume voxel by 1.
* Dither per voxel will be improved further.
* If you don't have a world volume, the start and end of the volumes are automatically calculated. This improved the workflow when working with Volume objects.
* Translucent and refraction bsdf now uses the thickness output. This raises some questions about how to document this. A design task will be created to discuss this with developer/users.
* Experiment on dithered soft shadows dithering


## NPR

* Preliminary design on NPR engine. First feedback will be requested from module and studios who have already been working with the module on this topic. When the direction of the design is more clear other studios and community will also be asked for input. 


## Vulkan

* Several planning sessions and review sessions have been commenced. The next part of the project is focused on performance, before we implement the missing features.
  * [https://projects.blender.org/blender/blender/issues/118330] - Design on render graph
  * [https://projects.blender.org/blender/blender/issues/120174] - High level design when a render graph will be available as an API next to the current immediate and batch APIs.
* Current state is that the design has been approved. Development will restart within a few weeks.


# 2024-03-18 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 25, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-03-25 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-03-25 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel
* Michael

## EEVEE

Currently there are many PR's in review state and will be reviewed as soon as possible.

### PRs in review
* Last week viewport pixel size landed, but introduced several regressions with film overscan and border rendering. When fixing these issues also some missing parts in the border rendering where detected.
* Sphereical Harmonics Horizon scan optimization is in review.
* Volume merge voxelization performance.
* Volume rendering only used the ambient term, but now also used the directional term (so you have directional lighting from indirect lighting).
* Spherical harmonics world extraction. It now considers all the LOD0 pixels. This can later to be used for world sunlight extraction that can replace the lost lighting that is caused by the clamping of the world lighting. This will eventually create shadows from world HDRI. It is unsure if this part will be in the initial release
* Fixed discontinuities shadow rendering at cubemap face transitions of local lights. Solution hashes the face index into the sampling, to make the issue less visible.
* Shadows resolution and scale option per light.
* Disabling motion blur in viewport without playback

### PRs in development

* Dithered shadow transparency. 
* Shadow matrix offsets for light bleeding. unknown if it is really needed for the initial release. Easy to implement, but unsure which priority it has.

## Metal

* Mostly bug fixes on Metal backend
* Time was spent in researching AMD issues with EEVEE-Next on the Metal backend. Expected that most issues are GPU resource synchronization issues. But leads to very different render results.
* There are some texture format issues where AMD doesn't support read/write. we might need to use a less efficient texture format to mitigate it.




# 2024-03-11 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 18, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-03-18 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-03-18 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Miguel

## EEVEE

- [https://projects.blender.org/blender/blender/pulls/118903] Viewport pixel size is being added. Still work in progress. It allows users to render the viewport on a lower resolution what is then upscaled to the final resolution. The upscaled result is close to what is normally draw as the upscaling is happening on samples (not pixels). Currently investigating why shadows are pixelated, how screen space effects should be handled.
- Horizon scan optimiation is still in progress. Performance uplift is very noticeable. Due to the usage of spherical harmonics it might also be possible to reduce the resolution of the horizon scan without visible artifacts.
- Work continued on polishing shadows. Some experiments are done to improve the LOD selection.
- There is a platform limitation when using the Metal Backend on Intel iGPUs. In this case the film accumulation step isn't working due to unsupported image access for the Intel iGPUs.
- Upcoming weeks more attention to fix Blender 4.1 issues as well.
- We also need to start working on the release notes. At least to document behavior differences between EEVEE and EEVEE-Next. Would also help in classifying bugs and helping users.

## GPU Module

- [https://projects.blender.org/blender/blender/issues/118961] Adding support for small types (8 bit and 16 bit).


# 2024-02-26 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 4, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-03-04 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-03-04 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Michael
* Miguel

## Module

* Updated the module [roadmap](https://projects.blender.org/blender/blender/wiki/Module:%20EEVEE%20&%20Viewport#projects). Projects that won't be started in the near term are not included, as priorities will shift.

## EEVEE

* Light probe convolution was added. It now works better for refraction based materials
* Fixed issues when using reflection probes.
  * World got black when the atlas texture was resized
  * Viewport wasn't refreshed when using 1 sample.
* All Closure evaluation are agnostic by default. That means if you have a light loop over three closures it doens't assume the closure type. It is now possible to use clear coat materials without noise. It only works if you only use upto 2 reflection closures. It was a large change so testing is needed.
* Transparent shadow objects in the distance made the shadow map explode. Now the relative distance to light is used.
* [https://projects.blender.org/blender/blender/issues/118725] In order to improve blending of volume probes in a runtime free manner, we need to introduce a 1 voxel padding around the volume probe data. This offsets the actual positioning of the samples in the volume grid. This changes the sample location of the volume points and might require versioning code. This needs some discussion with artist about the consequence.
* PCF shadows still in progress
* Optimizing the film accumulation shader using specialization constants. Patch is still in review.


## Draw manager

* Meeting with Hans Goudy about Mesh batch caching optimization. We discussed the current patches and what our requirements for such project is.

# 2024-01-29 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **February 5, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-02-05 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-02-05 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Michael
* Miguel

## EEVEE

* EEVEE-Next has been post-poned to Blender 4.2 LTS. More information can be found at [https://devtalk.blender.org/t/eevee-next-release-postponed-to-blender-4-2-lts/33035]
* EEVEE-Next crash has been solved on platform requiring workarounds that are injected as a geometry shader. The generated geometry shader was overwritten by the specialization constants which lead to linking errors. The platform that we used didn't provide any error messages, what took more time to find out what was wrong in the shaders. 
* Reference images for the render tests have been generated. These images are used to validate same results across platforms. It also showed some bugs, which are being fixed. Some changes have been made to the render tests to suite our workflow
  * Render tests have been disabled for features that are not supported by EEVEE. Lightlinking, shadow catcher, light groups and others. When these features we can enable them again.
  * Generated report contained incorrect commands when generating the test report for EEVEE-Next.
* Several issues on Metal have been fixed around motion blur bug, transparent shadows, render layers.
* Reflection probes render incorrectly when the viewport is very small. This issue has been investigated, by the root cause hasn't been found yet.
* Performance improvements. Previous workaround of the read-back buffer wasn't reliable. A new implementation using permanent mapping buffers was created. 
* Scheduling draw commands for EEVEE-Next/Workbench-Next has a performance difference compared to EEVEE-Legacy. The cause was a to broad memory barrier. A patch is in review addressing this issue. 
* Fixed dithering issue in blended materials
* PCF shadow feature is still in development. Noticed an issue where 8k shadowmaps resulted in the same quality as 2k textures. This will be tackled first, to make sure that the PCF shadows don't need to be reworked in the near future.


# 2024-01-22 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **January 29, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-01-29 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-01-29 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Jeroen
* Michael
* Miguel
* Thomas

## EEVEE

This week we will decide if EEVEE still needs more time to mature and post-pone its release to Blender 4.2. The main issues that we have are performance related. 
* Shadow performance is concerning and hard to mitigate. Currently we have some ideas, but unsure if they make a difference. Ideas experimented this week include filtered importance sampling, pre-filtered shadows which is tricky as it needs to be transposed to multiple level of details (probes).
* The plan for horizon scan is to share the scan for all BSDFs. However that is more than a simple fix. A solution we are considering is to use spherical harmonics for the accumulation phase the evaluate the SH for each BSDF afterwards.
* When using Intel GPUs the material shadowing shader cannot be linked. It fails without an error. We bisect the issue back to the specialization constants change. Currently still investigating how to fix the linking error.
* Low powered GPUs are still concerning. The performance drop for those devices make them unusable. We expect that discrete have more head-room to not show the performance drop that much, compared to low powered GPUs.
* Other issues include a difference between final image rendering and viewport rendering. Might be related to reflection probes.
* There are some regressions that needs to be fixed.
* Another concern is that 4.2 is an LTS release and that might become a bottleneck when the main development branch deviates to much from the LTS branch. This might impact that some improvements can only land in main when the main issues in the LTS branch has been backported. Although this should be manageable as only high prios will be backported and most are expected right after the release.

Next to that some improvements have been made
* Intel on Metal crash has been fixed, but still has render glitches.
* Parts of the forward pipeline is skipped when they don't have any work. 
* Experiments on improving shadow rendering has been tested.

And some patches are in development.
* Profiling performance report including the GPU module.
* Read back performance issue from shadow buffer is still being worked on.
* General register spill/memory bandwidth optimizations are being performed. 
* HiZ update using texture gathering
* Thread group optimization raster order groups using explicit rasterizing order could remove atomics. Might be portable to vulkan as well.
* Multiple shader compilation threads.

# 2024-01-15 EEVEE/Viewport Module Meeting (Upcoming) #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **January 22, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-01-22 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-01-22 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Jeroen
* Miguel
* Thomas

## EEVEE

* Added support for specialization constants to the OpenGL backend. This allows shaders to be optimized based by compiling pre-configured variables as constants. [https://projects.blender.org/blender/blender/pulls/116926]
* EEVEE Next performance is getting better, but there are still some iterations to be made. Shadow rendering is one of the areas that needs more work.
* In the shadowing pipeline there is a read back to CPU, which has a performance penalty. This readback is used to inform the user that the shadow map update isn't complete yet and requires more EEVEE will perform more samples for completion. During the meeting we discussed ways how informing users can be done directly on the GPU.
* The GPU profiling patch should be reviewed and land in main to help finding areas that need to be improved. The profiling patch is more accurate than some tools provide as it measures the performance from Blender point of view. Tools sometimes only measures pipeline scheduling or closer to GPU which leads to different figures.
* Metal backend has several regressions.
  * Atomic fallback for Intel iGPUs doesn't compile. Apple has provided some feedback how to fix these issues and we will be looking into fixing them
  * Light buffers can allocate buffers with zero length, which is invalid. Could be a cause why lights aren't visible. But that needs more investigation.
* We shortly discussed what needs to be done, in case we decide to post-pone EEVEE to Blender 4.2. It seems fairly straight forward, changing UI, hide EEVEE-Next render option, switch back to EEVEE-Legacy when EEVEE-Next isn't available. 

## Workbench

* Some workbench and overlays regressions have been fixed (volume drawing, overlay drawing)
* There are some ideas about improving wireframe drawings. Unclear when that will be worked on.

# 2024-01-08 EEVEE/Viewport Module Meeting (Upcoming) #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module
development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in
Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting
can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **January 15, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2024-01-15 time=11:30:00 timezone="Europe/Berlin"] → [date=2024-01-15 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Miguel
* Michael

## EEVEE

* Specialization Constants work with Metal and uses a work around for OpenGL and Vulkan. On Metal hardware the most visible bottleneck is the film shader. There is already a patch for this.
* Specialization Constants for OpenGL and Vulkan are planned for the upcoming weeks.
* GBuffer/deferred pipeline has been optimized to limit data transfers. Other optimizations are still on going.
* GPU register pressure issues was caused by an array indirection. If you have an array of structs, it can be demoted to main memory by the driver/compiler. If the first element of the array isn't read statically it can add a lot of memory overhead and spills. Some changes have been made to fix these performance issues.
* Lighting shaders are refactored to use a stencil buffer. 
* Some bottlenecks have been detected in the shadow pipeline still. Shadow pipeline does a read back that is quite hard on the performance. The shadow pipeline also generates a lot of empty indirect calls that adds more overhead than expected We need to find out how to improve the performance here.
* We are looking into enabling render tests for EEVEE on the buildbot. Some developments have been done last month, but due to time constraints it could not finish. There is also an issue on the buildbot where older shaders are not removed and allocate a lot of disk space (also concerns Cycles). Next steps here is to ensure render tests are working and reference images can be created. We also need to check with the cycles team as it would make a change in the process of adding reference images.
* There is currently a known issue where metallic shaders + clear coat using the deferred pipeline generates noise.
* Currently still concerns if we can make it for 4.1. We want to make a final decision on 22th of January.
* When EEVEE will become stable and performant we will plan a roadmap workshop to discuss and update the future plans of EEVEE/Viewport module. 


# 2023-12-11 EEVEE/Viewport Module Meeting (Upcoming) #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module
development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in
Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting
can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **December 18, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-12-18 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-12-18 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Miguel
* Michael

## EEVEE

* [https://projects.blender.org/blender/blender/pulls/115465 Lookdev spheres have been ported to
  EEVEE-Next. The new implementation also includes shadow and light evaluation direct and baked
  light on the spheres. The spheres are positioned on the near plane of the viewport/camera.
* Several optimizations
  * Optimized the Deferred shading to use tile based approach.
  * Optimized and simplified the ray-tracing pipeline. Calulating the setup of the horizon
    scan one time and reuse it in multiple passes.
* Stabilizing and bug fixing in the horizon scan pipeline (light leaking), switching render
  engines could use incorrect shaders.
* Workaround for atomic texture operations to cover older Metal platforms.

Focus in the next period is fixing bugs, regression testing and performance. 

## Shader builder

* [https://projects.blender.org/blender/blender/pulls/115929] Shader builder is a tool used by
  EEVEE/Viewport developers to check if changes made to shaders doesn't break other shaders. It does
  this by compiling all 'static' shaders. Due to recent changes in the build environment shader
  builder didn't compile anymore on macOS and Linux. The faulty change has been identified and
  rolled back.
* [https://projects.blender.org/blender/blender/pulls/115888] Shader builder could be more useful
  when it selects other work flows as well. Especially when adding features to the GPU backend you
  would like to filter on what it exactly validates. An `--gpu-backend` and `--gpu-shader-filter`
  argument was added to support these workflow. Without these arguments developers would make local
  changes to the source code what needed to be cleaned up afterwards.
* Eventually the goal is to run shader builder on the buildbot so changes will also be validated
  on platforms where the change isn't developed on. This still requires more testing to see on which
  platforms this can be enabled and for which backends.


# 2023-12-04 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module
development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in
Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting
can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **December 11, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-12-11 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-12-11 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel
* Michael


# 2023-11-27 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module
development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in
Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting
can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **December 4, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-12-04 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-12-04 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Miguel
* Michael

## EEVEE

* [https://projects.blender.org/blender/blender/pulls/115284] World light is not included anymore
  when world uses absorbes volume. This resembles more what Cycles does, but will change the look
  of older EEVEE files. Including some of our demo files. The idea is to add a convert world volume
  to object operator to help users to convert older files. This is an user action as it changes how
  the scene is structured and should not be done automatically.
* Bump mapping implementation introduced in Blender 3.5 can lead to NVIDIA compilation issues where it
  complains to be compiling to much instructions, althought the new implementation has fewer branches.
  More research is needed in order to see what could be the work around.

## Metal

* Shader specializatoin constants and compiler constant tuning.
* Experiments for optimizing shadow maps for the dynamic shadow maps. Idea is to add a simple depth
  pass which is a transform using mesh shaders, just moving the position and most likely be optimized away.
  This is for now only used by the Metal backend due to the differences between Mesh Shader optimizations
  on other platforms.
* For larger meshes other experiments are in the making.


## OpenGL

Researched issue related to latest released Intel HD Driver. In the final released drivers there
are issues that will crash Blender 4.0 when starting. This is a Windows/Intel driver issue only and
users can work around this issue by downgrading their driver or use Linux. As we don't have access
to this platform it is unknown to us what the actual driver issue is and how much time a developer
would spent on adding a workaround. The documentation is updated and Blender will notify the user
that the platform is unsupported, without crashing.

Downgrading the driver would also support HD4400 and newer devices to be able to use Blender 4.0.
There is still a discussion going on how long we would be able to support these devices. Especially
with the changes needed for Blender 4.1. 

## Vulkan

Most time spent on researching an out of resource issue related to command submission and transient
command buffers. The root cause is unclear and also the Vulkan specs are unclear about this point.
The spec mention it as a driver responsibility so the validation layers don't report any issue.

* [https://projects.blender.org/blender/blender/pulls/115184] Show Memory Statistics in Status bar
* [https://projects.blender.org/blender/blender/pulls/114574] Enable Tests Not Relying on a Context
* [https://projects.blender.org/blender/blender/pulls/115357] Replaces the main CPU/GPU Fence with
  Timeline Semaphores. Timeline semaphores uses a more detailed way of synchronizing CPU and GPU
  events which would benefit multi threading. 
* [https://projects.blender.org/blender/blender/pulls/115343] Store Vertex, Index and Storage
  Buffers on Device Memory. 
* [https://projects.blender.org/blender/blender/pulls/115346] Initial Pipeline Cache. Only caches 
  in current process. Didn't detect any expected speed-ups when using Blender.



# 2023-11-20 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **November 27, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-11-27 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-11-27 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Miguel


**EEVEE**

* Mostly worked on solving issues in Blender 4.0.1 and Blender 4.1.
* Proposal for improving mesh data handling [https://projects.blender.org/blender/blender/issues/114112]


**Vulkan**

Some experiments and changes this week related to resource management over multiple threads. Some
resources like command pools, descriptor sets (and pools) should not be shared between
threads.

Samplers and anisotropic filtering has been added as well.

* [https://projects.blender.org/blender/blender/pulls/114786] Fix Depth Texture Data Conversion 
* [https://projects.blender.org/blender/blender/pulls/114792] Initialize Pipeline State 
* [https://projects.blender.org/blender/blender/pulls/114827] Implement Samplers 
* [https://projects.blender.org/blender/blender/pulls/114833] Make Anisotropy Sampling Optional 
* [https://projects.blender.org/blender/blender/pulls/114977] Make Command Pool, Descriptor Sets Context Specific 
* [https://projects.blender.org/blender/blender/pulls/115033] Combine Data Transfer and Compute Commands 


# 2023-11-13 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **November 20, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-11-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-11-20 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Miguel

## EEVEE

* Initial pass for the manual has been done. Shadow pipeline and light probe pipeline hasn't been documented yet and more details needs to be added.
* Several fixes surrounding UI, Normal Map, Render region and Shadowing
  
## Workbench

* Fixed issue with channel packed alpha and none alpha options in texture mode [https://projects.blender.org/blender/blender/pulls/114377].

## Vulkan

* Fixed flickering of workbench on Intel and AMD platforms. Cause was that the depth texture was requested from a texture pool. As Intel and AMD GPUs don't support 24 bit depth texture it was uses a 32 bit depth texture and overwrote the texture type. By doing this the texture pool always wanted to create a new texture in between samples causing the screen to flicker. [https://project.blender.org/blender/blender/pulls/114697]
* At the end of the vertex/geometry stage the depth position is retargetted so it emulates OpenGL depth clipping. This is done during shading patching. When using geometry stages, the `EmitVertex` has to be replaced with `gpu_EmitVertex` to perform the retargetting. [https://project.blender.org/blender/blender/pulls/114669]
* Grease pencil (v2) drawing is working [https://project.blender.org/blender/blender/pulls/114659]
* Fixed shadow rendering. There are some precision issues left. [https://project.blender.org/blender/blender/pulls/114673]
* Add support for devices that don't support `VK_FORMAT_R8G8B8_*` as vertex buffer format. [https://project.blender.org/blender/blender/pulls/114572]


# 2023-10-23 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **November 6, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-11-06 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-11-06 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

> Due to the Blender Conference the next meeting will be skipped.

## EEVEE

* Added mesh volume bounds estimation (https://projects.blender.org/blender/blender/commit/f79b86553a4).
* Material Pipeline Refactor (https://projects.blender.org/blender/blender/commit/2a725001d1e). Remove options for translarent objects. They should now be detected automatically
* EEVEE-Next: Rework the material panel (https://projects.blender.org/blender/blender/commit/7b97bc48d82).
* Bug fixes
    * NaN artifacts caused by raytracing denoising (https://projects.blender.org/blender/blender/commit/69e85382cdb).
    * Low resolution on local lights shadows (https://projects.blender.org/blender/blender/commit/439d3ae945f).
    * Broken volume shadowing from sun lights (https://projects.blender.org/blender/blender/commit/454c8fd8667).
    * Missing light specular contribution (https://projects.blender.org/blender/blender/commit/920005ddc0f).
    * SubSurfaceScattering: Invalid buffer format (https://projects.blender.org/blender/blender/commit/58319132045).
    * Allow clearing of 3D texture using the framebuffer workaround (https://projects.blender.org/blender/blender/commit/5d1489c61d5).

* Looking into regressions where dependacny update graphs aren't performed for materials
* Checking how we can convert settings from EEVEE-Legacy to EEVEE-Next automatically.
* When debugging GPU, not set variables will be set to NaN, making it easier to detect issues during development.

## Metal backend

* Adding support for existing tests cases.

## Performance

* Research has been conducted to implrove the drawing performance of many objects within the current architecture. This lead to a proposal which is currently being discussed. (https://projects.blender.org/blender/blender/issues/113771)


# 2023-10-16 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **October 23, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-10-23 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-10-23 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

## EEVEE

* EEVEE-Next was removed from experimental. Most important features are there but there are features that are broken at this moment. There will be an effort to fix those issues during BCon1.
* Support for Planar Probes has been added.
* Paralax correction for reflection probes.
* Buttons in the property panel have been rearranged. Some panels (Volume/SSS) will still be changed.
* EEVEE-Next now support Intel ARC GPUs. (https://projects.blender.org/blender/blender/pulls/113447)
* Many fixes has been done in the area of reflection probes and shader compilation.
* Thickness output on Material output is made visible in the UI.


## Workbench

* Mostly fixing Regressions. Most were related to in front depth.
* Fixing shadows on Metal.

## Vulkan

* There is a blog post on [code.blender.org](https://code.blender.org/2023/10/vulkan-project-update/) about the project status
* Stabilization and cleanups have been done.
    * Wireframe mode rendering (https://projects.blender.org/blender/blender/pulls/113141)
    * Vulkan backend supports transform feedback only as an extension and hasn't been implemented in out backend. We still compiled shaders that uses transform feedback. These shaders will not be compiled anymore during validation (https://projects.blender.org/blender/blender/pulls/113655)
    * Remove extension useful for development as it isn't available on a regular system. (https://projects.blender.org/blender/blender/pulls/113654)
    * Added workarounds for shader viewport index output and layered rendering. (https://projects.blender.org/blender/blender/pulls/113605)
 
 ## GPU
 
 * Fixed issue in python GPU module where scissor state changes didn't sent gl command. (https://projects.blender.org/blender/blender/pulls/113642)
 * Cleanup of shader dependencies. Adding reduce_min/max/add/average functions. From now on we should not use common lib shaders in EEVEE-Next. Files that start with common mustn't be used. Same refactoring should be done for other render engines as well.


# 2023-10-09 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **October 16, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-10-16 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-10-16 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel


## Blender 4.0

* Workbench next had several performance regressions. Several changes have been made to get the performance back on par with 3.6. There is a concern that there is still an overhead er object. Which should be monitored.
    * [https://projects.blender.org/blender/blender/pulls/113251]
    * [https://projects.blender.org/blender/blender/commit/c6194afc41]
* Metal workbench shadows being worked on. The shadows didn't support indirect draw in Metal. Tricky to solve and requires more investigation. 


## EEVEE-Next

* Refactor lighting evaluation to experiment with different shadow compositing. SSS Translucency Evaluation is extracted outside the light loop for better light evaluation.
* Some experiments are being done how to handle geometry thickness related to different effects.
* Development of planar reflection probes started. Still needs to be included into the raytrace pipeline.
* Test support is merged in main. Will help on finding regressions and artifacts on platforms. [https://projects.blender.org/blender/blender/pulls/112161]

## Metal

* During the meeting discussed how to handle SSBO read-back barriers. In the Metal backend a GPU_finish has been removed for performance reasons, that breaks test cases. We discussed about having a dedicated API for syncing between the host and device buffer. A PR will be sent in to discuss the API in more detail.

## Vulkan

* Initial Wayland swapchain has landed. Wayland clients don't have the concept of a window and the client itself is responsible. For OpenGL there is a library used `wl_egl` to keep track of the window state, but it is connected to the EGL library. For Vulkan there isn't a similar library provided. Currently Blender keeps track of the window size and the needed size for the swap chain. The performance isn't where we want it to be. In the future we need to address this by looking into specific vulkan extensions like `VK_EXT_swapchain_maintenance1`.
* Vulkan is enabled as eperimental options in main. This means that every alpha build of Blender 4.1 will have an command line option to start Blender using the Vulkan backend. This is highly experimental. More information can be found [here](https://devtalk.blender.org/t/blender-vulkan-status-report/25706/21)


# 2023-10-02 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **October 9, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-10-09 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-10-09 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel
* Omar


## Blender 4.0

* Fixed performance regression in workbench. Was related to pool textures and was fixed by adding a delay to the garbage collection.
* There is a performance regression when resizing the viewport. Fix is still in development. 


## EEVEE-Next

* Metal tint for EEVEE/EEVEE Next
* Shadow tracing and SSS Refactor merged in main
* Two implementation of shadow mapping. One is more performant on Apple Silicon. Both implementation can run on all hardware, but performance would be different.
* Work continues on stabilizing and performance testing


## Vulkan

* When using `GPU_PRIM_POINTS` the shader needs set `gl_PointSize` before entering the fragment stage. Some code-paths in Blender didn't use the correct shader. The Vulkan backend will assert in case it detects a shader mismatch between shader and primitive type. 
* Worked on Vulkan support when using Wayland. The challenge is that Wayland doesn't have the concept of a window and the application is mostly responsible for common window tasks like resizing. Wayland has a wrapper `wl_egl` that is used to keep track of the window size. Although EGL is a cross platform API wrapper around OpenGL and OpenGL ES and not Vulkan, after reviewing its we decided to reuse this component, as it is lightweight and provides missing pieces. [https://projects.blender.org/blender/blender/pulls/113007]
* Preparing a patch to enable Vulkan as an experimental option for Blender 4.1 alpha builds. The goal is to get feedback on its current platform support and if there are areas that we aren't aware of that isn't working. [https://projects.blender.org/blender/blender/pulls/113057]. Don't expect a usable Blender version as performance and feature parity is still ongoing.

## Compositor

* Some refactoring to speed up the performance of jump flooding. Better performance and more precision.
* Currently looking into adding full float support for the GPU compositor. For precision issues and cryptomatte. During the meeting we discussed an approach for this. The idea is to duplicate the shaders in the compositor shaders class and patch the create infos locally. As this has GPU bandwidth concerns we don't want to automatically add support for this in the GPU module.

# 2023-09-24 EEVEE/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender EEVEE/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on EEVEE/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **October 2, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-10-02 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-10-02 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

## EEVEE-next

* Fixes for irradiance grid has been made, PR should still be extracted from the development branch
* Metal shadow implementation
    * The Metal version we are targeting doesn't support texture atomics, which is required for Shadow rendering. For tile-renderer based GPU there is a new approach in development that improves performance on Apple Silicon. Intel/AMD GPU still need a work around by emulating texture atomics.
    * Metal 3.1 would support texture atomics, but would require a higher OS version. There are some options to still consider as we don't want to push users into an OS update.
    * Parameter buffer issue when GPU is flushes as to much geometry is drawn.
* SSS compatibility issue, refactored SSS pipeline to compute shaders. Needed to refactor the deferred pipeline. Replace stencil buffers with a uint buffer. The previous version updated the stencil buffer for each object, the new approach does this on pixel level. Can have some additional performance increase as only pixels are updated that needs to be updated. When using a graphics pipeline all pixels needs to be updated that the geometry touches.
* Added Screen Space indirect diffuse. So we now have a SSGI prototype even if not performant.
* Worked a bit on shadows and checked what could still be improved and what not. Added a parameter for soft shadow scaling.

State of EEVEE-Next. Plan is to merge it at the start of BCon1 of Blender 4.1. Feature wise it might happen, but there are still some bugs that would let it slip. Bugs include volume, reflection probes workflow and performance regressions and Intel ARC support.
Ideas for performance we are thinking of a fast-path that disables raytracing or other features. Other ideas is to render immediate buffers in a smaller resolution.


## Blender 4.0

* `--debug-gpu-force-workarounds` was broken and has been fixed. There are still some workarounds that should be removed as the features are OpenGL 4.3 compliant.
* Partial GPU Texture was broken for Byte textures with Non-color color spaces

## Vulkan

* Tested how the Vulkan back-end is holding up with EEVEE-Next and Cycles.
    * Basic EEVEE-Next is working. Shadows/Lights are failing.
    * It is not possible to import Cuda buffers in Vulkan. How Cycles is currently organized it should allocate the VkBuffers and import them in Cycles. 
* Implemented frame buffer blitting for depth/stencil textures. Had to work around issues where this isn't supported on AMD platforms
* Implemented a basic GPU Query pool. All geometry for the occlusion queries should happen in a single command buffer submission. Current approach is to target Overlay-next as it doesn't use occusion queries and make sure that widgets are drawn in a single command buffer submission.



# 2023-08-28 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **Septembver 4, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-09-04 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-09-04 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel
* Omar

## Eevee-next

* Development on shadow tracting has continued. Limitations were detected when using large angles. Reference game engines are fixing this by limiting the angles and cube face edges artifacts.
    * This is not usable for us as we want to mimic cycles as close as possible. Currently researching of jittering lights with large angles and mixing both solutions (traced shadows + jittered shadows) to the final result.
    * Shadow tagging should be worked on.
* Metal backend is currently broken new shadow shaders don't compile. There is work being done to improve the rendering for tile based rendering GPUs that would also solve this issue. But we should make sure that shaders would compile always in main so we can get more feedback from users.
* Refraction shaders are now better for denoising
* Reviewed the BSnF algorithms to mimic Cycles better.
* Fixes and changes
    * Landed
        * Fix for incorrect object info randomness (https://projects.blender.org/blender/blender/commit/89dc2e1649)
        * World shading compilation error (https://projects.blender.org/blender/blender/commit/f57a8f07c0)
    * In Review:
        * Fix Subsurface Scattering regression (https://projects.blender.org/blender/blender/pulls/111534)
        * Add shadow bias based on depth reconstruction precision (https://projects.blender.org/blender/blender/pulls/111478)
        * Move the transmittance LUT to the Utility Texture (https://projects.blender.org/blender/blender/pulls/111535)


## Vulkan-backend

* Most effort last week was spent on solving a state difference between GHOST (Blenders hardware and OS abstraction layer) and the Vulkan backend in the GPU module. Vulkan doesn't track states of textures and prefer fixed pipelines where texture layouts are controlled by using the pipelines in the correct order. Blender cannot work this way as the pipelines are created dynamic due to user input, add-ons with custom drawing and functional complexity requiring to many pipelines to manage. The issue we want to solve is to track the texture layout of textures in the swap chain. Swap chains are responsibility of GHOST, but the GPU module can change the layout of its textures when using it as a transfer target. The GPU module cannot correct they texture layout as it isn't aware of swap chain events. (https://projects.blender.org/blender/blender/pulls/111389) 
* We decided to align the solution close to how Metal solved this issue by using a `present` callback that the GPU context registers to the GHOST context. Initial development started with promising results, but still requires many fixes and tweaks before it will land. (https://projects.blender.org/blender/blender/pulls/111473)


## Workbench

* Workbench-next is enabled by default. (https://projects.blender.org/blender/blender/pulls/111403)
    * There is a known regression where Intel HD4000 series GPUs are unsupported. We expect that a supported GPU has at least 12 SSBO binding points. Those are needed for Eevee-next we should consider to reduce the minimum needed slots to 8 and add a message when switching to Eevee-next. 
* A work-around has been added for GPU implementations that don't support texture-views. (https://projects.blender.org/blender/blender/pulls/111402)


## Overlay

* Showing wire color option in all shading modes is in main (https://projects.blender.org/blender/blender/pulls/111502)
* Removing fresnel in edit mode user preference is ready to land in main (https://projects.blender.org/blender/blender/pulls/111494)
* Both patches still need to be ported to Overlay-next.
* Removing fresnel in object mode is still under review to make sure it works for cost common cases.


# 2023-08-21 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **August 28, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-08-28 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-08-28 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel

## Eevee-next

* New implementation of shadow rendering landed in main. The new implementation is faster and makes working with shadows usable. Check the pull request for detailed information what changed in the implementation (https://projects.blender.org/blender/blender/pulls/110979)
* Work started on improving the shadow implementation for Apple Silicon. 
    * The new implementation has some benefits when using tile rendering based GPUs.
* Work started on soft-shadows. During implementation light bleeding artifacts were detected when tracing the shadow map. Currently in investigation wht causes them.

## Vulkan-backend

* During development we detected a difference between OpenGL GLSL and Vulkan GLSL we didn't expect. When using structs as stage interfaces all attributes must share the same interpolation qualifier. Around 80 shaders in gpu, workbench, grease pencil, overlay and eevee didn't work on Vulkan as expected. After looking into different solutions we decided to not support per attribute interpolation modes for those shader. We had to patch those shaders. pyGPU wasn't effected by the changes as add-ons were not able to construct struct based stage interfaces. (https://projects.blender.org/blender/blender/pulls/111138)

## GPU Compositing

* Researching different approached for several nodes/features including Canvas transform, Pixelate and Double Edge Mask


# 2023-08-14 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **August 21, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-08-21 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-08-21 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Omar

## Eevee-next

* New solutions for shadow rendering are being researched and prototyped. When rendering a scene most time is spent on updating the shadow textures. This lead to undesired performance. Some solutions that were looked into didn't scale to all the platforms we need to support. Other solutions would reduce the shadow quality when navigating or editing in the scene, which leads to a different kind of usability issues. A possible solution have been found and is currently being implemented and tested. We are not replacing the VSM method, but optimizing the implementation to be better suitable for our use case.

## Metal-backend

* EDR support landed in main

## Vulkan-backend

* Development on the Vulkan backend has started again. After reviewing the current state the plan is to improve the command encoding. The current command encoding is deliberatly not flexible. The problem of command encoding is now better understood (with the experience we got on using Vulkan the past half year).
* The goal of command encoding is to track the commands that are sent to the GPU and where possible reorder, combine commands or add detailed memory barriers. Eventually this would lead to faster rendering, without loosing the flexibility that is required by the user interface code of Blender.

## GPU Compositor

* A question was raised if it was possible to use a software rasterizer (LLVMPIPE) to implement the CPU part of the compositor. Discussion went into the impact and potential benefits and drawbacks with this approach.
    * Current GPU module uses globally state to keep track of device features. LLVMPIPE will have a different set and this should be kept separated
    * Some features might be missing and can lead to issues later on
    * Approach could also be used for other cases to streamline CPU and GPU execution
    * Multi device in a single application can work, but gets into context issues when using the OpenGL API. Vulkan might be a better target for this. 

## Blender 4.0

* Blender BCon2 will start upcoming week. Any breaking changes will be prioritized
    * Moving some settings from cycles to DNA so we won't break the API in a later Blender version
    * Wireframe Fresnell patch code wise is fine, some changes have been asked on usability side.


# 2023-08-07 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **August 14, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-08-14 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-08-14 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Anthony
* Clement
* Jeroen
* Michael
* Omar

## Windows/ARM

Microsoft is executing a project that might eventually lead to supporting Windows/ARM as an official platform. After we bumped OpenGL version to 4.3 the developer of that project had some issues to actually support it on Microsoft/ARM devices. Mostly as the drivers only support OpenGL 4.2 with most extensions except the texture extension.

During the meeting we went into several scenarios and see what one would fit. On the short term we will add a work-around to Workbench-next so that the project can continue. Adding support for Eevee-next requires driver compatibility with OpenGL 4.3 or Vulkan 1.2.

Our advice is to focus on vulkan compatibility. Main concern is that hardware manufacturers might not implement full compatibility any time soon. Consequence is that Windows/ARM would not support all features in Blender. Most noteworthy Eevee.


## Eevee-next

Although we have worked hard and would have wanted Eevee-next to be in Blender 4.0 release it is currently not mature enough. Releasing it in Blender 4.0 would lead to undesired performance regressions, stability and workflow issues. We decided to target Blender 4.1 so we can have some extra months on polishing Eevee-next features. Eevee-next will still be available on alpha builds as experimental feature.

* Polished irradiance caching


## Metal-backend

* Mostly worked on fixes and Eevee-next compatibility.


## Viewport-compositor

* Adding support for anisotropic kuwahara filter. (https://projects.blender.org/blender/blender/pulls/110786)

# 2023-07-17 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 24, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-07-24 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-07-24 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel
* Brecht
* Francesco


## Eevee-next

* Light probes landed in main. Light probes are now updated on the fly and during image rendering are updated for each frame. Current design only supports spherical probes and most options have been removed. When they still have value they will be brought back.
* Point clouds support landed.
* SSR denoising patch added for review.

During the meeting we went over the current state of Eevee-next to find our the feasability of having this in Blender 4.0
We think that most of the features can be implemented over the next few weeks. At that time we will also communicate via
devtalk to start testing. We expect that it can lead to many bugs that we are not aware of, due to Eevee-next isn't
tested due to missing or changing features.

Currently we know of compatibility issues with Intel GPUs (both iGPU and discrete) and specific AMD platforms.

It hasn't been decided if it all fits in the schedule as due to these unknowns. When we have more insight on
stability (platform + functionality) we will make a final decision if it will be a 4.0 or 4.1 target. It is
better to have a stable system than something that will not be received well. Eevee is to important for Blender.


**Major Topics:**
- Irradiance Volumes: Much polish to be done to make it fully usable. Still needs design related to scalability.
- Reflection Probes: Working (more or less), but would need a good solution for alpha blended objects that cannot do raytrace (revert to FIS).
- Soft Shadows: A bit of an unknown as we need to trace the shadow heightfield and this has some implication with the Virtual Shadow Maps.

**Small Issues:**
- Volumetrics Shadow Support: Needs to wait for shadows to be finished as this would require shadow tracing. But can be enabled before that even.
- Object ray/bake visibility: This is needed to replace reflection probe Visibility Collection.
- Bent Normals: Ambient Occlusion needs to be integrated into Irradiance cache sampling to give correct occlusion.
- Contact Shadows: Needs to know how much distance to trace (instead of relying on user param). Otherwise, easy to port.
- Overscan: Architecture is there to support it (in viewport & render). It's just a matter to implement it.

**Compatibility support**
- Curves on Metal: This was fixed at a higher level. The PR just need a few adjustments.
- EEVEE-Next Shadows on Metal: Some out of bounds reads issues are left but otherwise good progress with workaround renders.

**UX / Polish:**
- Screen Tracing & Denoising : Still needs UI polishing.
- Material Settings: Blend modes needs to be revisited because opaque changed behavior (it can be transparent).

**Delayed features:**
- Volumetric temporal stability: Can be postponed, but would be nice target considering that EEVEE-Next is aimed to be temporally stable.
- Reflection Planes: Not planned for initial version. Might be considered a regression. But design of how to integrate them into the deferred tracing. Is the length provided in the StorageArrayBuffer specialization the initial default length, or the maximum possible? is up to discussion.



# 2023-07-10 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 17, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-07-17 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-07-17 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel
* Omar
* Thomas


## Eevee-next

* Light probes
    * Probe cubemaps are now stored in octahedral mapping space (https://projects.blender.org/blender/blender/pulls/109559)
    * Multiple probes with different resoltions are stored in a single texture (https://projects.blender.org/blender/blender/pulls/109688) 
    * Initial implementation details are described at (https://developer.blender.org/docs/eevee_and_viewport/eevee/modules/reflection_probes/#subdivisions-and-areas)
* Lookdev
    * Initial HDRI switcher (https://projects.blender.org/blender/blender/pulls/109548) landed. It has some limitations as the change of a world is 3d viewport dependant, but scene based light volume are stored in scene level. In future more developments should be done to make sure that switching HDRI would also affect diffuse light.
* Work continued on 
    * In development
        * SSR denoising. Solution is based on Eevee-rewrite branch and the Lumen presentation given by the developers last year.
        * Hair & Curves (https://projects.blender.org/blender/blender/pulls/109833)
        * Volumes (https://projects.blender.org/blender/blender/pulls/107176)
    * In Review
        * Point clouds (https://projects.blender.org/blender/blender/pulls/109832)


## Workbench-next

* Fixing a regression with transparency depth
* Fixing overlay composition with 'In Front' objects 


## GPU Compositor

* Development of the Sunray node is still being worked on.


## OpenGL 4.3

* Blender minimum OpenGL version will be bumped soon. We will display a message to the user when running on not supported platforms, but they are still being able to start Blender. When Workbench-next is made default they won't be able to start Blender anymore.

# 2023-07-03 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 10, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-07-10 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-07-10 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel

## Eevee-next

* World reflective light was reviewed and landed. https://projects.blender.org/blender/blender/pulls/108149. The world light is also added to the irradiance cache https://projects.blender.org/blender/blender/pulls/109516 allowing scenes to be lit by only the world.
* Ambient Occlusion patch has been reviewed and landed (AO node + render pass) https://projects.blender.org/blender/blender/pulls/108398
* Work continued on SSR and soft shadows and volumetrics https://projects.blender.org/blender/blender/pulls/107176


## Workbench-next

* Last patches landed. We will retest on different platform and bump opengl version to 4.3 before we make it default.




# 2023-06-26 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **July 3, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-07-03 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-07-03 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel
* Omar
* Thomas

## Eevee-next

* Irradiance cache landed. This is an initial version. Improvements to performance, workflow and compatibility are still expected.
* Implementation of Softshadows has started.
* Reflective light
    * World cubemap is still in review https://projects.blender.org/blender/blender/pulls/108149
    * Changing the HDRI in the viewport is in review, but waits for the first PR to land. https://projects.blender.org/Jeroen-Bakker/blender/pulls/5 rotation of HDRI around camera needs to be checked if this can be done for 4.0. World rotation is included in the patch
    * Adding world light to irradiance cache is in review, but waits for the first PR to land. https://projects.blender.org/Jeroen-Bakker/blender/pulls/6
    * Adding reflection probes are work in progress. https://projects.blender.org/Jeroen-Bakker/blender/pulls/7 Current development is focused on probe evaluation. It is still unclear how the baking workflow will look like. There are multiple solutions with different draw backs.


## Metal backend

* Root cause was found why Eevee crashes on some AMD platforms. The cause was that the compiler created an infinite loop when using floats as index. There is a patch that will be added for 3.6. https://projects.blender.org/blender/blender/pulls/109358
* Testing Eevee-next support. Still in early stage and nothing to report at this moment.

## Overlay-next

* Camera background is being worked on.

## Workbench-next

* Merge workbench once the missing features are in.
    * Sculpt has been done
    * Started on curve/hair support
* This will bump OpenGL support for all platforms to OpenGL 4.3. This means that Metal will also be only available on Mac platforms.


## Viewport

* Experimenting with new viewport compositor with Sergey and Brecht. Work include:
    * Adding Node previews
    * Improving performance
    * Implementing new nodes that have been added 


# 2023-06-19 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 26, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-06-26 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-06-26 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Michael
* Miguel
* Omar

## Eevee-next

* Subsurface scattering has landed in main. [https://projects.blender.org/blender/blender/pulls/107407]
* Viewport HDRI selector is being worked on. Initial version would not support blurring.
* Modified the way how textures are bound to shaders. Eevee-next used to bind internal textures and buffers to slots upto 15, leaving the lower slot numbers for user textures. This has been switched; internal textures and buffers are now occupying lower slots, and user textures the higher slots. Freeing more slots for user textures. Slot 0 and 1 are reserved still for Grease pencil [https://projects.blender.org/blender/blender/pulls/108984]
* Improved validation messages so it is easier for developers to track the reason that raises the messages.


## Overlay-next

* Porting extra passes and shapes is in progress. Contains a refactor how render passes are organized. [https://projects.blender.org/blender/blender/pulls/109059]

## Metal backend

* Memory freeing issue in Blender 3.6 has the highest priority. There has been a first round of feedback. This week BCon4 is scheduled hence this issue will have the top priority. [https://projects.blender.org/blender/blender/pulls/108940\
* Continued with the EDR/HDR change. [https://projects.blender.org/blender/blender/pulls/105662]

## Vulkan backend

* Added support for compute indirect. [https://projects.blender.org/blender/blender/pulls/108879]
* Some experiments have been done to for device selection


# 2023-06-12 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 19 22, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-06-19 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-06-19 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Jeroen
* Michael
* Miguel
* Omar

## Eevee-next

* Code review started for GI. Overall the codebase is fine, there have been some missing features detected in the Metal backend.
* We discussed about all the open points that needs to be done before Eevee-next can be enabled for Blender 4.0
    * World lighting patch is ready and needs to be approved; World lighting inside GI has been implemented, but can only be reviewed after the GI Patch has landed.
    * SSS patch is ready for review.
    * AO patch is waiting for the world lighting.
    * Hair rendering hasn't been implemented (Both Workbench-next and Eevee-next)
    * GI patch
* Other topics to be aware off
    * Refletion probes development hasn't started yet. Might land after the initial enablement.
    * There are concerns about the current platform availability
        * Intel Integrated GPUs
        * Metal backend stabilization
* More time this week will be spent on reviewing available patches.


## Overlay-next

* Everything should be in except the 'extras'. Extras is a specicial category of drawing inside the Overlay engine
* There is a design question about how to share drawing code between modules. Each object type in the overlay engine is developed as its own module. But some drawing code should be shared between onkect types.

## Metal Backend

* Mostly worked on stabilization. Fixing test cases.
* Continued with the HDR enablement patch. The setting to turn on/off HDR will be a scene setting (part of color management)

## Vulkan Backend

* Last few weeks most of the time was stepent in making sure that the Vulkan backend could be used with Workbench (legacy).
    * Normals are now calculated correctly. Issue was an initialization error in the Global state manager
    * Add support to attach cubemap sides to a framebuffer
    * Added support to attach a specific mipmap level to a framebuffer
    * Added support to update mipmaps based on the data available in mipmap level 0
    * Added support to for ImageViews separatnig usage from shaders and framebuffers
* Main issue remaining is that the depth values are stored in a different range compared to OpenGL. The depth of field currently reads incorrect data and places the focus point to a different depth. This should eventually be solved but is postponed for now to work on Workbench-next support. 


# 2023-05-22 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **June 5 22, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-06-05 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-06-05 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

## Eevee-next

* Eevee-next development are a bit behind schedule. We discussed during the meeting how to address this issue. Taking over code reviews, adding more people on the development. Jeroen and Miguel are available to help, but the topic should be clear how the solution would work in order to be effective.

## Workbech-next

* Volumes landed in main. There is an issue where transparent surfaces behind volume objects are not rendered correctly that should still be investigated.

## Overlay-next

* Development continued on Overlay-next.

## Metal Backend

* Improved depth bias to reduce tile artifacts on Apple Sylicon GPUs.
* Eevee-next requires some features in the backend
    * Currently baking irradiance caches fails on some devices. It isn't clear if the issues are related to the specific branch.
    * Image atomics are required, but not yet supported by the backend. We should add an easy solution first and after that we have time to iterate on the approach. The idea is to add a 2d buffer for the atomics.
* Apple is narrowing down the issue why AMD GPUs can have stability issues. They is an cause found, but it is still unclear how to work around it.

## Vulkan Backend

* Adding support for Workbench. Still work in progress
    * Improved stability of vulkan when descriptors where fragmented/not available.
    * Add support for texture formats that uses non-standard low precision floating points. Using a template it is possible to convert from a regular float to any low precision floats and use conversions that is appropriate for textures. Like clamping to max values, support non-signed floating points.
    * Add support for reading back depth textures and sub-areas of textures
    * Improved the surface selection to select unsigned normalized SRGB surfaces.
    * Not yet working:
        * Hair rendering (requires texel buffer support)
        * Studio lights have inverted normals.
        * Volume hasn't been tested yet.
        * Point clouds hasn't been tested yet.
        * Shadow rendering (requires stencil buffers).
        * Depth of field (requires mip mapping).

# 2023-05-15 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 22, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-05-22 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-05-22 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Omar

## Eevee

- Blender studio requested a transparency pass for Eevee. https://projects.blender.org/blender/blender/pulls/107890
    - Limitted to monochromatic opacity; colored opacity will show differently than in combined pass

## Realtime Compositor

- Continued with Tracking and color management nodes
    - https://projects.blender.org/blender/blender/commit/d91f0bf8d20972a
    - https://projects.blender.org/blender/blender/commit/6698e8f4a07fa2a
    - https://projects.blender.org/blender/blender/commit/e8c660b85538122
    - https://projects.blender.org/blender/blender/commit/88d03d15c193ab0
- https://projects.blender.org/blender/blender/commit/f9b5b0efd22291c.
- During the meeting did a quick discussion about the double edge mask node, in paint and tracking nodes that aren't easy to be converted to a GPU scalable algorithm
    - For double edge mask we need to research if an SDF approach would be possible.
    - Some nodes use an external library/code and might need to rewritten to be GPU capable.

## Eevee-next
- Fixed a regression that broke Volume rendering in EEVEE and Cycles
    - https://projects.blender.org/blender/blender/pulls/107826
- Addressed feedback for Volumes and SSS.
    - https://projects.blender.org/blender/blender/pulls/107176
    - https://projects.blender.org/blender/blender/pulls/107407
- Found and fixed a crash in EEVEE Next when the Scene doesn't have a World.
    - https://projects.blender.org/blender/blender/pulls/107891

## Overlay-next

 - Ported most shape cache functions from DRW_cache to Overlay Next.

## Vulkan backend

- Initial graphics pipeline landed in main.
    - Has been validated to work on AMD/Intel and NVIDIA GPUs on Windows and Linux systems.
    - Many advanced features have not been implemented and can crash Blender when using.
- Started on technical documentation. https://julianeisel.github.io/devdocs/eevee_and_viewport/gpu/vulkan/


# 2023-05-08 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 15, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-05-15 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-05-15 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Miguel


## Metal backend

- Metal backend support for Workbench-next and Eevee-next has worked on. Workbench-next is working, but Eevee-next need more work to get on the same level as the OpenGL backend. Currently it is possible to continue feature development using a Mac system, what was short term goal of this development.
    - Some issues found in the backend that has been reported.
    - Irradiance cache freezes computer

## Overlay/selection-next

- Removed templates from initial development. Refactored for code-clarity.
- Started porting everything from the 'extra' files. Getting familiar with the engines.

## Vulkan backend

- Device is now shared between different contexts. This improves the stability as other components (including the memory manager) is shared.
- Basic support for SRGB textures have been added. There were 2 issues with SRGB textures.
    - Not all platforms support using SRGB textures as storage buffers. Blender doesn't use these, but tagged each texture to be usable as storage buffers. Solution was to not tag SRGB textures.
    - Added data conversion between float linear host data and SRGB encoded data. SRGB encoded textures are used in Blender to save GPU bandwidth.
- GPU module is designed that resources can be bound without having a bound shader/pipeline. The color management pipeline uses this as well as Eevee-next. The state manager has been adjusted to track the resources and do a late binding when the shader pipeline is constructed.
- Swap chain has been rewritten. The previous swap chain was designed around a gaming pipeline where a single function is responsible for aquiring and presenting an swap chain image on the screen. This wasn't working as Blender window manager use both independently. The new swap chain keeps track when a new image is required to make sure that during presenting the correct image is used. This still needs some tweaks in order to land in main.
- Current state of the vulkan backend is that immediate emulation is working and gives us a technical workable, but user unusable Blender. Advanced viewport features (used by 3d viewport/image editor) isn't working and multi window isn't working. The developments has only been validated on NVIDIA. AMD open source driver has some color artifacts and is less stable. AMD closed source driver and Intel platfirn hasn't been tested at all and is expected to crash on startup.



# 2023-05-01 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 8, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-05-08 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-05-08 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Omar


## Realtime viewport

- Implemented remaining nodes
    - Denoise node (not realtime yet due to GPU<->CPU roundtrip)
    - Stabilize 2d node
    - Texture node (still has some artifacts that needs user feedback)
    - Corner pin node
    - Mask node


## Vulkan backend

- Continued with the graphics pipeline. Smaller changes already landed in main. Currently most of the immediate mode works, viewport don't work yet. (https://projects.blender.org/blender/blender/pulls/106224) Note that "work" in this sense doesn't mean that it is usable. The Vulkan backend still requires changes to become usable/stable et al in order to be usable for end-users.
    - Resources of devices are feed after the device is destroyed -> Crashes Blender at exit.
    - Vertex attribute data conversion is missing resulting some shaders not to work
    - Draw manager isn't working as it crashes when allocating global resources. This has to do with texture format support. Need to investigate as 1d texture should be supported by all platforms.
    - This list isn't complete and it only shows the issues we are currently aware of.


# 2023-04-24 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **May 1, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-05-01 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-05-01 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Jeroen
* Michael
* Miguel
* Omar

## Eevee-next

- Shaders have been patched to add Metal support to Eevee-next. Still requires other patches as well before it will work.
- Volumes now use compute shaders
- Did optimization to object bounds. In complex scenes this got good performance boost.
- Working in progress on Subsurface scattering.

## Overlay-next

- Initial patch to review the new architecture has been sent in for review. (https://projects.blender.org/blender/blender/pulls/107045)

## Metal backend

- Some discussion has been done on supporting Intel/AMD on Metal backend. There are some issues, but using OpenGL for these devices also has down-sides. For now due to the close deadline to 3.5.1 release we will categorize them as known issues.
    - Legacy Intel platforms don't start. Issue is already solved in MacOS 13.0. Still looking into the exact MacOS versions that solved the issue.
    - AMD GPU has issues when rendering the material preview. Disabling this feature is not desired as it is also used in asset creation pipeline. Downgrading to OpenGL can still be done by user in the user preferences.
- Patch for bindless attachments (needed for Eevee-next) has been send in for review.


## Vulkan backend

- Last week did a sprint to get the first pixel on screen by starting blender. This was a success and gave us feedback on the developments that still need our attention. (https://projects.blender.org/blender/blender/pulls/106224)




# 2023-04-17 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **April 24, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-04-24 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-04-24 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Brecht
* Clement
* Jeroen
* Michael
* Miguel
* Omar

## Regressions

- The fix to support Eevee on Intel HD4400-HD5500 on Windows introduced a regression that made Eevee unusable on MacOS/AMD using the OpenGL backend. This has been fixed. Thanks to the Blender community for validating the fix works on the problematic platforms. The fix has been marked to be backported to Blender 3.5.1.

## Planning

We spent time during the meeting to go over the current state of all the projects. Here is an overview.

### Eevee-next
- Indirect lighting
    - Indirect light baking working, but still needs to be added to be used during drawing/rendering.
    - Expect several weeks to polish to a MVP. After that more polishing is needed but that will be postponed after all core features have been implemented.
- Volume
    - Expected to land in the upcoming week.
    - Smaller changes are needed afterwards.
- Other
    - SSS has been ported, but still needs to be reviewed
    - SSRT and reflection probes would be prioritised after the core features are available in main.
- Polishing
    - Shadows still requires more work to be reusable. (bugs, performance). We can still delay it a bit to focus on other areas before that.
    - After the bigger changes have landed the properties that needs to be controlled by users should be clear and work on the UI/Properties can start.

### Overlay and selection

- Overlay and selection will be merged into a single engine. The selection will be done by swapping out the overlay shaders with selection shaders.
- The selection shaders won't be using GPU-queries anymore, but use atomics to match the previous behavior. 
- Initial feedback from Campbell is desired to validate the approach. After that the implementation can be extended to other selections, before users can test.
- Overlay and selection isn't a requirement for Eevee-next and could also land in a different release.

### Workbench

- Workbench next almost finished; curve rendering and tweaks are still to be done.

### Grease pencil

- Grease pencil next is handled by the GP team members.

## Eevee-next

Previous section was about the global state. This section is about the developments that have happened since the last meeting.

- Irradiance cache is now stored per object
- Volumes is working with some limitations. Volumes is implemented as a graphics shader and needs to be ported to compute shaders. 
- Eevee-next running on Metal has some unknowns. Most risk would be on platform compatibility. SSBO and bindpoint PR needs to be reviewed and feedback addressed. Platform support could be done after initial Apple Sillicon implementation.
- Challenge could be supporting Intel iGPUs and we might also bump the minimum requirement for it to match Windows/Linux bump for Blender 4.0.

## Vulkan back-end

- Initial development was done to the graphics pipeline (https://projects.blender.org/blender/blender/pulls/106224). This includes the immediate mode support. The initial development was done to find areas that require engineering. The next areas have been found so far:
    - Some vulkan commands have to run inside a `VkRenderPass`. Other commands (including data transfers) are not able to run inside a render pass. This require changes to the `VKCommandBuffer` to keep track of what is needed and perform a late render pass binding to reduce unneeded context switches.
    - A better internal API is required to support both the graphics pipeline (using `VKBatch`/`VKVertexBuffers`) and immediate mode (using `VKImmediate`/`VKBuffers`).
    - Reuse temporary buffers to reduce allocation/deallocation. As buffers are constructed with a specific memory profile these buffers also needs to be managed so in different pools. Idea is to have a `VKReusableBuffer` that will move its ownership back to the pool when it has been used. This solution is expected to work together with the submission/resource tracker that was introduced for the push constants fallback.
- Parameter has been added to GPUOffscreen to specify the needed texture usages of its internal textures. This was needed to reduce validation warnings in the Vulkan backend when used in test cases where the data was read back for validation. By default the host visibility flag wasn't set. (https://projects.blender.org/blender/blender/pulls/106899)


# 2023-04-03 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **April 10, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-04-10 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-04-10 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen

## Blender 3.5 release

### Regressions

- Issues detected when using Intel HD5500 on Windows. Current state is to reproduce it on a developer machine. Most machines we have access to are more modern or use Linux as that platform is more stable when using legacy hardware.
- Fixing hangs when rendering Cycles in Viewport on Apple Silicon


## Eevee-next

- Continued work on GI solution. 
    - EEVEE-Next Irradiance Baking: Due to time constraints it isn't possible to start implementing a solution that leverage raytracing. So the implementation will try to minimize the amount of changes to the current system while cleaning up the implementation and changing the baking algorithm (https://projects.blender.org/blender/blender/issues/105643)
    - With the rewrite of the light-cache system, it became clear that the current way of storing the light-cache per scene has its limitations. Going forward we would like to separate the actual stored data and the engine lighting representation. This mean the lighting data would be stored per object then loaded by the engine into its own data structure. (https://projects.blender.org/blender/blender/issues/106449). This is still in design phase as there are other limitations and differences compared to current implementation. This requires more investigations.
- Shadow tagging, Volume rendering. Volume rendering is more difficult than expected


## Viewport Compositing

- New Fog implementation based on Eevee Bloom.


## Metal backend

- Optimize EEVEE/SSR for Apple Silicon, 20% improvement by using packed data.
- Optimize Texture usage flags in Workbench, Eevee (still in review)


## Vulkan backend

- GPU: Renderdoc Frame Capturing (https://projects.blender.org/blender/blender/pulls/105921): For better debugging of the current Vulkan backend we needed to be able to use renderdoc with our headless draw tests. In order to get this working we use athe Renderdoc APP-API to check if Blender/Test cases are run inside renderdoc and trigger the capture where needed. This can also be used to limit the capture to specific parts of the code.
    - Some tweak still needs to be done to improve the workflow.
- Vulkan: Resource Submission Tracking (https://projects.blender.org/blender/blender/pulls/105183)
- Vulkan: Texture Data Conversions (https://projects.blender.org/blender/blender/pulls/105762)
- Vulkan: Clearing Framebuffer + Scissors (https://projects.blender.org/blender/blender/pulls/106044)


# 2023-03-20 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 27, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-03-27 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-03-27 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

## Eevee-next

- Irradiance cache: Added library to Eevee-next for Spherical harmonics.
- Deferred render support for eevee-next
- Volume support still in development.

## Blender 3.5

- Blender 3.5 Eevee regression where less image nodes were supported than in previous version.
- Grease pencil bugs
- Metal fixes
  - Principle BSDF on AMD is still in investigation. ONly on materials 

## Vulkan

- Added PR for data conversion support. We detected that some platforms don't support the data types that Blender requires. Next step is to validate this with the vendor, and design/plan workarounds when needed. (https://projects.blender.org/blender/blender/pulls/105762)



# 2023-03-13 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 20, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-03-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-03-20 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Omar
* Michael
* Miguel
* Thomas


## Planning 2023

* Last week a Clement visited Blender HQ to do several planning sessions what we will be doing in the upcoming year. A blog post will be written about this, but in short:
    * Focus on Eevee-next, porting existing code to the new code-base where possible.
    * Research how GI can be implemented in Eevee-next. There are many different algorithms that all have side effects.
    * Viewport compositor will be focusing on support for render passes (both Eevee and Cycles). Requires changes of RenderEngine API.
    * Focus on Vulkan Backend.
* Eevee-next plannig has been updated (https://projects.blender.org/blender/blender/issues/93220)
* Michael and Jason also visited Blender HQ for some technical sessions and planning for the Metal Viewport. Focus will be on adding features that Eevee-next requires (SSBO, Indirect draw/compute), performance and handling bigger scenes.


## Workbench-Next

* Rotation API has land.
* Current volume algorithm has been ported to Workbench-next.
* Design has been made how to port Subsurface scattering to Eevee-next.
* Some developments have been done to add support for volumes back to Eevee-next. This is still work in progress for the upcoming period.
* Discussion with Line-art and NPR rendering. Result of the discussion is that line-art will be using geometry nodes. A discussed GPU implementation isn't a priority and would not use much of our time.

## Viewport compositor

* Added support for bi-linear filtering.
* Added support for multi-pass images
* Refactor GPU sampler code. Still in progress (https://projects.blender.org/blender/blender/pulls/105642)


## Vulkan

* Several changes have been made to the SSBO clearing API. The previous API was designed around OpenGL capabilities. The new one on what we actually need. This allows better portability to Metal and Vulkan backends. (https://projects.blender.org/blender/blender/pulls/105521)
* Push constants have landed. (https://projects.blender.org/blender/blender/pulls/104880)
* Buffers are now always mapped to virtual memory. (https://projects.blender.org/blender/blender/pulls/105588) This is a recommendation as mapping to virtual memory as an overhead, so better to do it once and keep the reference until you don't need it anymore.


# 2023-03-06 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 13, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-03-13 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-03-13 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel

## Overlay/Selection engine

* New design has been made for the overlay and selection engine.
    * Selection will not be based on GPU queries. This would improve stability across platforms and improve performance.
    * Selection engine will be done on top of the overlay engine.
* Merged the retopology PR.


## GPU module

* Added technical documentation of the GPU module (in the header files.)


## Workbench-next

* New custom ids to improve. Workbench engine already uses this new system.
* Development of Volumes in workbench-next has started.
* Fixed coordinate system.


## Metal

* Iterate on PR/issues to get the to stable for Blender 3.5
* Synchronization issues

## Planning

* Planning for this year might focus on Eevee-next t have a MVP as soon as possible. This would mean that after the current outstanding work more development time will be spent on Eevee-next.
* Also requires designs to be more fleshed out.
* Vulkan and compositor project still needs to be discussed this week.
* Might need more effort to get more people involved in the development. 


# 2023-02-27 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **March 6, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-03-06 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-03-06 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Thomas
* Michael
* Miguel


## General improvements

* CPP rotation API (https://projects.blender.org/blender/blender/pulls/104941)
* Smaller refactorings to GPUTexture.
    * Add header documentation and reorganize sections.
    * Add texture usage flag to all texture creation.
    * Make sure all available texture format are supported.
    * Remove data_format from 3D texture creation function.
    * Remove obsolete GPU_texture_bind_ex argument set_number.


## Metal

* Resolving outstanding issues, PR review iterations
    * Barycentric data: Some tricky solution where global state is required. Metal emulates this using a struct.
    * Issue in memory manager where 1000s buffers where freed. Threading issue,
* Indirect draw call support, one liner, but requires SSBO support.


## Eevee-next

* Transparent shadows. PR is available


## Workbench-next

* Draw manager handlers without having the overhead of bounding boxes and matrices to make the material slots less resource intensive.
    * Multiple implementations have been done, resulting to a design that we want to achieve.
* Discussion with Jackque Lucke how to handle volume rendering in Workbench-next.


## Vulkan

* Added initial support for images. Found a challenge where NVIDIA drivers do not support 1D image types.
    * Idea is to have a work-around for 2D images as this can be done inside the Vulkan backend by generating different resource binding and some function overloading. Although this needs some testing. Other vulkan implementations that supports 1D textures can still use their potential optimized sillicon paths for 1D images.
* Started design for push constants
    * When push constants don't fit on the device a fallback will be created to use uniform buffer objects. In the case the fallback is needed, all push constants for that particular shader will be inside the uniform buffer objects. Shader developers are responsible to tweak the shaders to minimize the need for the fallback.
    * Minimum size for push constants are 128 bytes. Although modern drivers already support 256 bytes. See https://hackmd.io/sGtEKjGHSVKSa_tTeZXi9g for an overview of static shaders with more than 128 bytes of push constants.
    * Initial idea is to have a sequential buffer that stores all push constants for a shader that hasn't been executed yet. When execution has been detected (submit id) the sequential buffer can be reused. Push constants has an API that can benefit from sequential buffers.
* Vulkan: Query limits of physical device. (https://projects.blender.org/blender/blender/pulls/105125)
* Vulkan: Add resolving of builtins to shader interface. (https://projects.blender.org/blender/blender/pulls/105128)
* Vulkan: Initial Uniform buffers support (https://projects.blender.org/blender/blender/pulls/105129)
* Tracking resources when multiple commands are in flight (Still in developement)


# 2023-02-13 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **February 20, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-02-20 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-02-20 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Omar
* Michael
* Miguel


## Module

* Gitea migration and setting up workboards
* New issues/pull requests should be tagged with the EEVEE & viewport project.
* Documented GLSL code-style documentation.
* Including what to avoid in GLSL for MSL compatibility.

### Blender 4.0

* Prioritize *-Next projects for Blender 4.0 project. This is a lot of work and require everyone there.
* NPR project would then be a target for after 4.0 release. There is no agreement on the proposed design yet. Time can still be spent on the design and making prototypes.
* Vulkan is important for AMD and Intel and we should still focus on this project as well.
    * We might want to focus on a non-optimized first implementation.
    * Optimizations could than delayed to after Blender 4.0 targets are done.
    * Current development is used to understand the optimal requirements for the Vulkan back-end. After understanding a design and implementation can be done. An example for this would be to improve command synchronizations.

## Eevee-next

* EEVEE-Next's new Virtual Shadow Map is now in master (now called main). A [devtalk post](https://devtalk.blender.org/t/eevee-next/23570/86) explain a bit what to expect from it.
* The BLI_math_float4x4.hh removal was also finally merge with all compilation issues fixes.
* Rotation API task is created that can be picked up by a developer (also community).
* Working on transparent tagging for shadow maps. Mostly done, needs some work, but first the pull request should be reviewed. More refining can be done afterwards.
* Bump node optimizations have been commited.


## Overlay-Next

* Porting empty drawing to the Overlay-Next engine and found a ways to improve the code quality.


## Workbench-next

* Added task for mat-cap improvements. Sculpt & paint wants to update the matcaps so this should be aligned.


## Metal backend

* Python: Suppress BGL deprecation messages after 100 times. 
* Shadergraph optimization patch updated with new ideas. With parent shaders can remove safely remove stuttering. There are more stuff that can be balanced compilation times versus stutteling.
* New features will be uploaded after the bcon3 change
* Pull requests are uploaded to solve the AMD issues.

## Vulkan backend

* Vulkan: Use guardedalloc for driver allocations (https://projects.blender.org/blender/blender/pulls/104434)
* Equalize API of readback for IBO/VBO and SSBOs. (https://projects.blender.org/blender/blender/pulls/104571)
* Vulkan Compute shaders (https://projects.blender.org/blender/blender/pulls/104518)
    * Direct dispatching of compute tasks
    * Initial SSBO, VBO buffers working.
    * Current DEVICE_ONLY usage requires host visibility during test cases. My proposal would be to have a separate usage flag for host visibility as this should be treaded as an exception. This way back-end can still do further optimizations when DEVICE_ONLY is used. Metal back-end currently doesn't support DEVICE_ONLY with host visibility.

---
# 2023-02-06 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **February 13, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-02-13 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-02-13 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel



## Eevee-next

* Porting source to new matrix API. Change got reverted due to built error on windows.
* Fix remaining issues on shadow maps. It is shippable, unless we want transparency as part of the first release.
* Design of the transparent shadow maps is still in discussion. Will be worked on further.
* Crash when running Eevee in debug mode with certain node setup. Still in vestigation. Task has been added with detailed explanation.
* Detected a way to improve the bump node performance. Bump node uses the incorrect function in some cases. Also some issues detected for noise nodes as well. Approach needs feedback as there might be better approaches.  
    * Initial feedback is ok, but still need more review to see how reusable it is in other cases as the solution is compiler dependant.

## Workbench-next

* Patch for line draw issues between the objects. Two separate paths for single material passes vs multiple material passes.
* 

## Metal backend

* Perspective nodetree optimization patch. Where in complex shaders register spills work
* AMD patches have been sent for review. Requires some feedback on how to handle them more cleanly (code-wise)
    * Multiple Principled BSDF on AMD don't compile nicely. Fix is large. 
* Optimized node graph patch still needs proper review. Small changes would still stotter as the PSO needs to be validated. Still some ideas, but requires more high level changes, limited to the GPU module/draw manager.
    * If fix is quite isolated, we could work with this change for 3.5. There are some workflow issues, that could be solved afterwards.
    * Fix changes GLSL and therefore can work on any platform (OpenGL/Metal). Would also help with bump node performance regressions.


## Vulkan backend

* Integrating Vulkan Memory Allocator (VMA) into Blender. Still in progress as there are many requirements that have to be met, before the library can be used. Currently still checking how to get it running on Vulkan 1.2.
* Found a minor issue in the VMA created a patch and it has been applied upstream. The patch is also applied to Blender's local copy.
* Converted test cases to use ShaderCreateInfo. Needs some small tweaks to get working on all platforms.
* Started with SSBO, added a generic Vulkan buffer that can be used by all buffer types. Added test cases to test progress as we won't be able to start until most of the project has been finished.
* Added a patch for using guardedalloc for driver and platform memory operations. (https://developer.blender.org/D17184) Requires some feedback how we want 
* to enable it. Current proposal is to use a compilation directive.


# 2023-01-30 Eevee/Viewport Module Meeting #

**Practical Info**

This is a weekly video chat meeting for planning and discussion of Blender Eevee/viewport module development. Any contributor (developer, UI/UX designer, writer, …) working on Eevee/viewport in Blender is welcome to join.

For users and other interested parties, we ask to read the meeting notes instead so that the meeting can remain focused.

* [Google Meet](https://meet.google.com/pdh-qwfv-dwf)
* Next Meeting: **February 6, 2023, 11:30 AM to 12:00 PM Amsterdam Time** (Your local time: [date=2023-02-06 time=11:30:00 timezone="Europe/Berlin"] → [date=2023-02-06 time=12:00:00 timezone="Europe/Berlin"])

*Attendees*:

* Clement
* Jeroen
* Michael
* Miguel


## Metal backend

- Addressing issues with Metal back-end. Some patches are still in flight addressing last known issues platform specific issues (mainly Intel iGPUs).
- Looking at community feedback the back-end is stable. A close eye is kept to the bug tracker if new issues pop up.
- SSBO support is nearly done. Special binding options are still open. All Eevee-next shaders compile. Workbench shadows are still not compiling and requires more investigation. Option will be made available as a compile option, as it requires some stabilization.


## Eevee-next

- Finishing new shadowing. Added initial set of options (removing some that requires user feedback if they are really needed).
- Other shadowing algorithm for ortographic views, still has an issue. Development will pause until we have access to the main development station to make sure it works on most common platforms.


## Workbench-next

- Workbench-next is in master
- Bugfixing last issues, memory leaks with shaders and solving known issues.
    - Fix for outline is ready but requires review.


