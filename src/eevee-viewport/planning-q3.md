# EEVEE/Viewport module - Q3 planning

## Attendees

- Clement
- Jeroen
- Miguel

## Goal

Plan the priorities for the upcoming release (4.3) and quarter.
We cannot schedule a lot of things, Due to the amount of work we need to get EEVEE out of the door and the summer holiday. We would put emphasis on stabilization, fixing some really bad limitations and bugs / UX, and finishing the development of Overlay-Next.

Overlay-Next would probably be a Blender 4.4 target. Finishing the development up front would allows to land it early.

> Other modules are doing changes to Overlay which that is currently frozen. We can accept changes only if the changes will happen in both engines. This so the module can focus on realizing the mentioned goals.

## Primary targets

- Improve Loading time of textures (and non blocking)
  - Issue unveiled by the parallel compilation.
  - Mainly loading textures from disk by OIIO.
- Improve parallel compilation (non blocking for render)
  - Batch Shader Compilation improvements [blender/blender#124538]
- Shadow terminator fix.
  - Solution still unclear.
  - Scene based light bias can be a thing, but we would like to strive for an automated system like Cycles has.
- Vulkan
  - Experimental option in an official release to get feedback on the platforms it doesn't work. At that point we accept reports, but would not advice to use it due to missing features.
  - Cuda, OpenXR, GPU Selection would not work.
  - EEVEE Volume probes currently isn't working, but it is clear what is failing.
  - GPU selection requires Overlay-next.
  - GIZMOs need to use shader variation.
- Overlay Next (Probably a target for 4.4)
  - Other modules features are postponed due the its development. If changes to overlays are needed, it must be added to both overlays engines. If changes have less priority it has to go to overlay next directly.
- Bindless textures
  - Research if it is possible using the platforms we support.
  - Availability of extensions. NVIDIA reduced the platforms that support the extension.
- Metal parallel compilation

## Extended targets

- Shader To RGB deferred lighting support. (design + start implementation)
  - Is there a different way to support it from NPR project point of view?
- Refraction as transparency for Blended materials.
- Better regression tests + platform support.
- Shadow Performance? (single update)
- Sun shadow accuracy?
  - Doesn't feel as an easy task.
- Light Loop performance? (write design and consider if feasible)
  - Needs to be solved before we can think of features like light node groups.

Related to our module:
- Multi render pass support in GPU compositor
- Grease pencil and Animation module are adding new overlays.

