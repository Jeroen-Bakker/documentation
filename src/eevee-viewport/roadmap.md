# EEVEE/Viewport Module Roadmap

Draft roadmap for year 2024.

```mermaid
flowchart LR
        subgraph VulkanBackend
            VulkanRenderGraph(Vulkan render graphs)
            VulkanSpecializationConstants(Vulkan specialization constants)
            VulkanSubpasses(Vulkan subpasses/workaround)
            Vulkan(Vulkan GPU backend)
            CyclesOnVulkan(Cycles viewport on Vulkan)
            OpenXRVulkan(Vulkan OpenXR support)

            VulkanRenderGraph --> VulkanSpecializationConstants --> Vulkan
            VulkanSubpasses --> Vulkan

            Vulkan --> CyclesOnVulkan
            Vulkan --> OpenXRVulkan
        end

        subgraph DrawManager
            ArbitraryCurveRendering(Arbitrary curve rendering)
            OverlayEngine([Overlay/Selection engine])
            NonBlockingRendering(Not blocking rendering)
            MeshCache(Mesh batch caching optimization)
            MeshCacheUseMappedMemory(Mesh batch creation on mapped memory)
            MeshCacheRemoveVirtualFunctions(Reduce virtual function calls)
            MeshCacheTaskFinishBarrier(Research need for finish barrier)
            MeshCacheTaskFinishBarrier --> MeshCache
            MeshCacheRemoveVirtualFunctions --> MeshCache
            MeshCacheUseMappedMemory --> MeshCache

            OverlayEngine --> NonBlockingRendering
            OverlayEngine --> AttributeOverlay
        end
        subgraph EEVEE
            EEVEENext([EEVEE-Next])
            PanoramicCamera(Panoramic camera support)
            LightTree(Light trees)

            ShadowPerformanceLowEndHW(Performance shadow on low end hardware)

            EEVEENext --> PanoramicCamera
            EEVEENext --> LightTree
            EEVEENext --> ShadowPerformanceLowEndHW
        end
        subgraph Workbench
            ImproveVolume(Improve Volume rendering)
            ImproveMatcaps(Improve Matcaps)
        end

        subgraph MetalAnimationSpeedup
            MetalFXUpscaling(MetalFX upscaling)
            OpenSubdiv(OpenSubdic support on Metal)
        end

        subgraph NPR
            NPRTechnicalDesign(NPR engine design)
            GreasePencilv3(Grease pencil drawing)
        end

```

