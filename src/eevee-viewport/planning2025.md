# Planning Viewport & EEVEE Module 2025

| Task                                                         | Expected duration | Ticket | Cleanup | Performance | UX  | Developers | Expected target           |
| ------------------------------------------------------------ | ----------------- | -------| ------- | ----------- | --- | ---------- | ------------------------- |
| **Clement** 
| Removal of DST lock                                          | 4w                |        | +++     |             | +++ |  Miguel    | 4.5 Alpha                 |
| Add a shadow terminator bias                                 | 2w                |        |         |             | +++ |            | 4.5 Alpha (after DST lock)|
| Optimize Dithered method shaders for shader compilation time | 1w                |        |         | +           |     |            | 4.5 Alpha (after DST lock)|
| Finish shader code cleanups                                  | 1w                |#127983 | +++     |             |     |            | 4.5                       |
| Remove runtime parsing of GLSL source                        | 1w                |#129009 | +       |             |     |            | 4.5                       |
| EEVEE Reverse-Z                                              | 2w                |#108123 |         |             | ++  |            | 4.5                       |
| **Miguel**
| Material compilation cleanup                                 | 2w                |#133674 | ++      |             |     |            | 4.5                       |
| Instancing optimization                                      | 4w                |#130291 | +       | +++         |     |            | 4.5                       |
| Format aliasing for efficient texture reuse                  | 2w                |#112478 | +       | ++          |     |            | 4.5                       |
| **Jeroen**
| OpenSubdiv GPU Backend                                       | 4w                |#133716 | +++     | ++          |     |            | 4.5                       |
| Performance target                                           | 10w not fulltime  |        | +++     |             |     |            | 4.5                       |


# DRAW

## Manager
### Removal of DST lock (3-4w)                                          Cleanup+++           UX+++       (Clément) +Miguel   4.5 Alpha
#### Motivation
    - No freeze caused by material preview.
    - No UI freeze caused by EEVEE render or bake.
#### Goals
    - Remove usage of all globals inside the draw module.
    - Removing the lock (much more error prone)
    - Simple but grunt work.
    - Needs to happen in 4.5 for the Game project.
### OpenSubdiv GPU Backend #133716 (4w)                               Cleanup+++ Perf++                (Jeroen)            4.5 (create infos) and 5.0 (backend)
    - Make it backend agnostic (port to shader create info).
    - Use new OSD cross api API (get shader source).
### Instancing optimization #130291 (2w)                              Cleanup+   Perf+++               (Miguel)            4.5 ~ 5.0 (regular module work)
    - Can be split in different target
    - Long term target
### OpenSubdiv GPU Optimization (>4w)                                   Cleanup+++ Perf++                (Jeroen)            5.0?
    - Fix performance issues (compression, data duplication).
    - Follow same convention as CPU (use same vertex formats).
    - Need to have a design for the future
### HDR support (6w)                                                                         UX++        (Jeroen)            Q?
    - Swapchain support in VK, landing in Q2 #133159
    - optimize image engine
    - Tracking
    - VSE
    - HDR swapchain
### Batch Cache Refactor (ongoing)                                      Cleanup+++ Perf++                (Hans)              ?
    - Performance Benefit mostly on Unified Memory Arch (M1, VK + integrated GPUs) needs low level change in GPU module.
    - Hans is looking at it
    - Needs simplification
    - Future optimization to allow GPU extraction / compaction
### Two pass Occlusion culling (2w)                                                Perf++                ()                  Not this year (or GSOC)
    - Constant handles (from instancing opti) can help this task
    - Not really a block
    - Very scene dependant
    - Not really applicable to animation workflow

## Image Engine
### Fix Performance Issue With High Res Images (1-2w)                              Perf+++               (Jeroen)            Q?

## Overlay
### Finish shader code cleanups (<1w) #127983                         Cleanup+++                       (Clément)           4.5
### Speed-up grid drawing (2w) #130464                                           Perf+                 ()                  ?
### Improve Wireframe Z fighting + fix perf cost (1w)                              Perf+     UX+         ()                  ?

## Select
### Move mesh edit mode selection to overlays (2w)                      Cleanup+                         ()                  ?

## EEVEE #123505
### Add a shadow terminator bias (2w)                                                        UX+++       (Clément)           4.5 Alpha (after DST lock)
### Optimize Dithered method shaders for shader compilation time (1w)              Perf+                 (Clément)           4.5 Alpha
#### Motivation
The deferred shading phase out the lighting complexity from the material shader. However, we now have an issue with the complexity of the polymorphic Gbuffer code who's code can take 1/4th of common shader compilation time (and more in simpler ones).
#### Implementation
The goal is to detect part of the code that are not used upfront to disable branches that would eventually never be used.
For instance, removing the `imageStore` when we know that the Gbuffer data will fit the FB output data will already be beneficial.
Removing packing code for refraction closure will also help if it is unused.
We can also bypass all gbuffer packing for additive only surfaces.
### Implement fragment shader tangent space calculation	#97900 (1w)                Perf+++
    - Look into quality (see malt implementation)
    - Reasses prio and go into design phase (UI option? etc)
    - Quality is not as good as expected but might still be good since the high poly meshes can same more processing and have lower error.
    - Animation + High subdiv would still
### Optimize Texture Loading (4w)                                                  Perf+++               ()                  ?
    - New main bottlneck for opening files
    - Can be multithreaded
    - KTX2? (Khronos equivalent of DDS)
    - Needs design
    - Break Smaller tasks
### Persistent data (4w)                                                           Perf+++               ()                  ?
    - Low hanging fruit for animation rendering speedup
    - Keep engine in memory and receive updates
    - Needs design
    - Break Smaller tasks
    - CTRL+F12 first (not Sequential F12)
### Stochastic Light Evaluation Pipeline #133449 (4w)                 Cleanup+++ Perf+     UX++        (Clément)
### Dithered render mode transparency improvements #133449                                 UX++        (Clément)
### Grease Pencil Integration #133556 (>4w)                                                                              5.0+
    - Need design
### Stochastic Light Evaluation Pipeline #133449 (4w)                 Cleanup+++ Perf++                                  5.0
### Reverse-Z (2w)                                                                           UX++                          4.5
    - API change GPU level.
    - Isolated changes.
    - Just need to make sure all features are ported.
### Light Node Tree Support #124615 (3w)                                                   UX+++           5.0+
### Shadow Performance Improvements (4w)                                           Perf++                (Clément)          ?
    - Meshlets would make it faster
    - Update detection is slow
    - Maybe look into simpler way to render shadows
    - Vulkan has some overhead from descriptors upload (4.5 target)
    - Conclusion: Needs investigation and design
### Render Test Performance and Coverage (1w)                           Cleanup++
### Texture Streaming #123955 (lots of enthusiasm) (4w)                          Perf+++                                  ?
### High Resolution Render support #71733 (1w)                                               UX+
### Bevel Node support (2w)                                                                  UX++
### View Layer Override (<1w)                                                                UX+
### Light Group (as in cycles) (2-3w)                                                        UX++
### Anisotropic BSDF (2-3w)                                                                  UX++
### Hair BSDF (2-3w)                                                                         UX+++
### Velvet BSDF (2-3w)                                                                       UX+
### Panoramic Camera (3w)                                                                    UX++
### Oblique Camera (1w)                                                                      UX+
### Displacement with normal map #122212 (1w)                                              UX++

## NPR                                                                                                                     5.0
    - Only needed for the Cow Boy project.
    - Need to agree on design ASAP (Start of Q1).
    - Dylon wants to have design discusion after production (end april-start may)
    - Might be split into a EEVEE task and a "Compositor" task.
### EEVEE Shader to RGB replacement for deferred (3w)                                         UX+
#### Motivations
EEVEE's roadmap goes into adding more feature to the deferred render shading (Dithered) method. However the Shader To RGB evaluation is still Forward (same as Blended method).
There is a lot of existing or planned features that will not be compatible with this mode.
To this end, we need to create a system that allows some of the shader to RGB flexibility while maintaining compatibility with the stochastic sampling of closures.
### Per Object Compositor (>4w)                                                               UX+++
#### Motivations
We want to use the same workflow as GPencil object on a per object basis.

# GPU

## Common
### Format aliasing for efficient texture reuse #112478 (1w)            Cleanup+    Perf++             (Clément)        4.5
    - No actual figures how much memory it buys.
    - Needs to evaluate VK/MTL API changes/usage.
    - Can be disabled if needed (bugs, regression).
    - Could tackle API hole, being able to use texture view on TextureFromPool
### C++ API (re)design #130805 (>4w)                                    Cleanup++                      (Clément)        ?
    - Needs design approval / discussion sessions (Q1).
    - Lots of tasks/ API
    - Divide in small tasks (Q1)
    - Can be tackled in regular module work over the year(s)
### Material compilation cleanup #133674 (2w)                           Cleanup++                      (Miguel)         4.5
    - No change of API
    - Just refactoring for cleanliness
    - Low prio but should help with the canceling compilation tasks and material preview stutters
### Share GPU Textures when using the same file path. #98375 (2w)                            UX+++       (Jeroen)         ?
    - Important memory optimization for large production
    - Related to Core module
### Texture GPU memory profiling tool (2w)                                                   UX++        ()               ?
    - Profiling memory from engine should be split.
    - Low hanging fruit is to just output simple file format (html) to avoid rabbit hole of in blender UI.

## Vulkan
### Performance target (10w not fulltime)                                 Cleanup+++                     (Jeroen)         4.5 
    - Check if Hydra delegate works #133717
    - OpenSubdiv #133716
    - OpenXR #133718
### Stability                                                             Cleanup+++                     (Jeroen)         5.0
    - Ensuring everything work.
    - Bug fixing.
## Metal
### Fix hanging tests on Buildbot (2w)                                    Cleanup+++                     (James)          4.5
### Fix test discrepancies (2w)                                           Cleanup++                      (Clément, investigation by James)
    - Not isolated to Metal (opengl + Nvidia/AMD difference).
## Shader Codebase
### Codebase improvement #131339 (>4w)                                  Cleanup++(+)                   (Clément)        ?
    - Long term plan
    - Need to agree on design (Q1)
### Remove runtime parsing of GLSL source #129009 (1w)                  Cleanup+                       (Clément)        (4.5?)

# SELECTION
## Have better documentation of the select API                            Cleanup++
    - Need to discuss what are the needs of the API (what selections exists)
    - Changing API needs to have a good feature to justify the changes and the time for it.
## Add selection tests

Feature requests from studio
- Being able to cancel shader compilation in F12.
    - Being able to cancel shader and render is different task.
- Single channel texture optimization (already there; but not working everywhere, with packed files, other format than EXR etc..)
    - Brush stroke project
    - Maybe just a problem of bugfixing
    - Maybe make sure design is sound (flagging?)
    - Regression tests
- Texture GPU memory profiling tool inside blender. (Was a "Charge" request)
- Reliable transparency, Less noisy.
    - Open problem
    - Can think about some resolution scaling, might also help cycles if render agnostic.
    - Look at openGPU library.
- Shader cache issue is a huge problem for the studio (Miguel?)
- Z-fighting / Large scene (Singularity, Reverse-Z)
<!-- - Way to mix Diffuse and refraction without noise (Dog walk, has workaround, low prio) -->
