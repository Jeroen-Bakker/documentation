---
marp: true
headingDivider: 2
theme: default
paginate: true
footer: November 2024 - Blender canteen session - WBSO
---

# WBSO

**Content**
- Application
- Challenges
- Proposal

## Tax Credit for Research and Development

- Application
- Development
- Project Administration
- Submission (hour sheets)
- Company visit

## Application - When?

- Application can be done twice a year.
- Application is for a specific calendar year.
- After an architecture/initial project phase.
- Can be announced and completed within 1-2 months.

## Application - Structure

- Project (Why)
  - Component (What)
    - Technical bottleneck (How)
      - Concept solution (How)
        - Technical novelty (How)

## Example - Project

```markdown
**Vulkan**

- Improve stability and manageability of the different platforms we support.
- Availability of modern GPU features that are not available using OpenGL.
- More performance by reducing translations and restrictions between software and hardware.
```

## Example - Components

```markdown
**C1: Memory Manager**
GPUs have multiple memory areas with all their own features. The memory manager will be able to select
the best area when allocating and moves memory around when better memory areas become available.

**C2: Data converter**
Not all GPU support the same data types. When a data type is not supported it needs to be converted to
a compatible data type on the fly. Pixel layout of an image changes depending on the way the image is
used by shaders and commands. 

**C3: Shader Compiler**
Shaders are programs that run on the GPU. These programs needs to be compiled and optimized. 
Front end compilation is responsibility of the application using Vulkan.

**C4: Command scheduler**
Commands send to the GPU needs to be reordered to reduce context switches. Some commands can be bundled
into a single command to reduce the overhead on the GPU.

```
## Example - Technical bottleneck

```markdown
**TK3.1: Compilation performance**
Large scenes have many different shaders. Opening scenes can take minutes. Most time is spent
on compiling materials to shaders for realtime interaction. Compiling is a memory and compute
problem. 

**TK3.2: Optimizing shader code**
Input program is structured so it is easy to understand for developers. It is not optimized
for execution. Shader code can be optimized to by removing unnecessary code paths, constant
unrolling, branch reordering. These are all complex engineering processes and an ideal
balance needs to be found between the time spent optimizing and time reduced when invoking
the shaders

**TK3.3: Shader patching**
Not all GPU share the same features. If a GPU doesn't support a feature a workaround should
be found. These workaround can influence the actual shader program. During shader patching
the shader code is adapted to workaround missing features without the developer need to be
concerned with it when developing the shader.
```

## Example - Technical solutions

```markdown
**OR3.1.1: Threaded compilation**
Being able to compile multiple shaders in parallel.

**OR3.1.2: Cache baked pipeline**
Cache the device specific code on disk between runs of Blender.

**OR3.1.3: Cache SPIR-V binaries**
Cache the intermediate compilation result on disk. Load the compilation results from disk
when it was compiled in a previous run.

**OR3.1.4: Shader libraries**
Cache aspects of a shader in a library. Reuse parts when shaders utilize the same aspect.
Eg same geometry inputs, blend modes, framebuffer layouts.

**OR3.1.3: Shader identifiers**
Identify if a shader is already known by the GPU driver. So we can skip input assembly stages.
```


## Example - Technical novelty

```markdown
**TN3.1.1: Threaded Compilation**
OpenGL doesn't allow threaded compilation. Until now all the compilation happened on a single thread.

**TN3.1.2: Cache baked pipeline**
Downloading device specific byte-code from the device to be reused for the next time. Keeping track
of device specific byte codes. Ensuring byte-code hasn't been modified in the mean time.

**TN3.1.3: Cache SPIR-V binaries**
Fast generating identifiers. Storing identifiers with the SPIR-V binaries.

**TN3.1.4: Shader libraries**
Caching parts of shades (aspects). Combining aspects and dynamic parts into a shader.

**TN3.1.3: Shader identifiers**
Making the assembly stage optional. Ensuring the shader should be recompiled based on platform
specifics.
```

## Administration

- Project administration should be done that is suitable for the size of the company.
- Administration should track actual developments (**commits**) on a day-to-day basis for **covered activities** related to **solving technical bottlenecks** of the application.

## Activities **not** covered by WBSO

- **Project management**.
- General **preliminary research** / (technical) market research.
- Drawing up **functional requirements** (PVE) / **functional design** / **functional documentation**.
- **Data modeling**, setting up databases.
- Architectural design, unless this makes a direct demonstrable contribution to solving programming bottlenecks.
- Research into and application of **existing methods** / **techniques** / **software** (acquiring existing knowledge).

## Activities **not** covered by WBSO

- Applying / combining and implementing **existing technology**. This includes third-party libraries or technical solutions from previous projects.
- **Routine programming work** (system construction) for which **no specific programming bottleneck** needs to be solved and where there is no technical risk with regard to feasibility. This also applies to redevelopment and further development.
- Maintenance and setting up / **parameterizing software**. This also includes bug fixing, learning / training ML and AI algorithms and making software suitable for another platform (**porting*).

## Activities **not** covered by WBSO

- **Rollout** and implementation (for users)
- **Functional testing** and user acceptance testing (‘usability test’).
- Writing **user documentation**.
- Certification and **marketing**.
- Work that is physically performed **outside the European Union**.
- All other activities and programming work that do not make a direct demonstrable contribution to solving the programming bottlenecks described in the application / explanation.

## Advice for writing applications

- Number everything in the proposal and relate to this number.
  - Markup is lost
- Update projects each year.
  - Are projects that don't change actually innovative? 
- Remove parts of the proposal that are not relevant anymore.
  - Application is only for activities of a specific calendar year.
- Log these changes.

## Discovered challenges

- gitea PR workflow doesn't provide required info
  - Most development happen in local branches
  - Local branches are deleted.
  - Prototypes never land in main
  - Bigger PRs are spliced into smaller ones.
  - Tracking of activities are done on user basis.
  - Hard to link tracked activities to technical bottlenecks

## Discovered challenges

- WBSO reporting != Project development reporting
  - Daily vs weekly
  - Specific activities vs project progress
  - Current reporting isn't scalable

## Proposal Gitea - Use Meta tickets

- Setup private central gitea repository.
- Add application descriptions.
- WBSO Meta ticket
  - WBSO-year meta ticket
    - WBSO-project-year meta ticket

## Proposal Gitea - PR workflow

- Local branches, force push & squashed commits are disastrous for traceability.
- However PR's internally track changes

```shell
git checkout -b vulkan/OR3.1.1/prototype-threaded-compilation
git commit --empty-commit
git push me
```
- Create PR before starting development
- Relate PR to project meta ticket

## Proposal Gitea - Automate reporting

- Current reporting isn't scalable.
- Month report per developer per day (with prefilling the excel)
- End of year report over all projects (in case we get a review)
